package com.nyko111.IAB;

import android.util.Log;
import android.widget.Toast;

import com.nyko111.GameWorld.GameWorld;
import com.nyko111.Interfaces.IabInterface;
import com.nyko111.inappbilling.util.IabException;
import com.nyko111.inappbilling.util.IabHelper;
import com.nyko111.inappbilling.util.IabResult;
import com.nyko111.inappbilling.util.Inventory;
import com.nyko111.inappbilling.util.Purchase;
import com.nyko111.tapwizard.AndroidLauncher;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;

public class NykoBilling implements IabInterface {
	
	public ArrayList<String> purchaseSKU = new ArrayList<String>();
	
	public static Map<String, PurchaseResult> purchaseResult = new HashMap<String, PurchaseResult>();
	
	private boolean billable = false;
	
	public static IabHelper mHelper;
	
	AndroidLauncher main;
	
	public NykoBilling(final AndroidLauncher main) {
		
		this.main = main;
		
		String base64EncodedPublicKey =

                "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmwcnPtimdqVWd5qTb9biLN+ik1FUgUF/xb90S6jGGsjDfszbuwbheqClIaHM7bmXRPI5JFpwl73BDIavREjyEvBC/BVD4NIYmgLQdhebtw+WWMWv1zTBiaNVdxyKvWS1s9BwDtZHbywoRViuO8cxHJjhg+XHrf7bBoPOMnczYFU/tE6PI3V1zwkznvOrYK+V8vdwk1XDQdexSIGUc/HOLqOpqqWOZ7gx9B0jETU6eScLCSgxRVka5Meu8Ocr1nSOH2N8oiOP1Tb2FaElPJr39sJjzhRV83YdAK+jMu+52xzsgLzr+7VHuAXga7CbCKA28IOeIxQmcNRd8jDjpMffJQIDAQAB";

    	mHelper = new IabHelper(main, base64EncodedPublicKey);
    	mHelper.flagEndAsync();
    	mHelper.enableDebugLogging(true);
    	

		
	}

	public void StartIAB() {
		mHelper.startSetup(new
								   IabHelper.OnIabSetupFinishedListener() {
									   public void onIabSetupFinished(IabResult result)
									   {
										   if (mHelper == null) return;

										   if (!result.isSuccess()) {
											   Log.d(main.TAG, "In-app Billing setup failed: " +
													   result);

											   Toast.makeText(main.getApplicationContext(), "Please update your google play services!",
													   Toast.LENGTH_LONG).show();

											   billable = false;
										   } else {
											   Log.d(main.TAG, "In-app Billing is set up OK");

											   billable = true;

											   mHelper.queryInventoryAsync(mReceivedInventoryListener);

											   if (mHelper != null)
												   mHelper.flagEndAsync();




											   consumeItem();


										   }
										   //consumeItem();
									   }
								   });

	}
	
	
	public void AddPurchaseItem(String SKU, PurchaseResult PurchaseResult) {
		
		purchaseSKU.add(SKU);
		
		purchaseResult.put(SKU, PurchaseResult);
		
	}
		//if purchase is successful, checks SKU of purchase to make sure it matches then give the item and consume it
		IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener 
		= new IabHelper.OnIabPurchaseFinishedListener() {
		public void onIabPurchaseFinished(IabResult result, 
	                    Purchase purchase) 
			{
				if (mHelper == null) return;

				if (mHelper != null)
					mHelper.flagEndAsync();
				
				if (result.isFailure()) {
			      // Handle error

					System.out.println("Error in billing mPurchaseFinishedListener");

					System.out.println(result.getMessage());

			      return;
				}      
				
				if (!verifyDeveloperPayload(purchase)) {
					System.out.println("Error authenticating purchase!");
				}	
				
				
				//mHelper.consumeAsync(purchase, mConsumeFinishedListener);

				if (mHelper != null)
					mHelper.flagEndAsync();



				consumeItem();
  
		   }
		};
		
		//function that consumes item (processes purchase), making it purchaseable again
			public void consumeItem() {
				mHelper.queryInventoryAsync(mReceivedInventoryListener);
			}

	@Override
	public void restorePurchase() {

	}

	//Chekcs if item is purchased and consumes it -this consumes it allowing it to be put in game logic-
			IabHelper.QueryInventoryFinishedListener mReceivedInventoryListener 
			   = new IabHelper.QueryInventoryFinishedListener() {
				   public void onQueryInventoryFinished(IabResult result,
				      Inventory inventory) {

					  // Have we been disposed of in the meantime? If so, quit.
					  if (mHelper == null) return;
					  
					  mHelper.flagEndAsync();
					   		   
				      if (result.isFailure()) {
					  // Handle failure


						  System.out.println("Receiving inventory failed");

						  System.out.println(result.getMessage());
				      } else {
			            
				    	  /*
				             * Check for items we own. Notice that for each purchase, we check
				             * the developer payload to see if it's correct! See
				             * verifyDeveloperPayload().
				             */  
				    	  
				    	 //check debug
				    	//mHelper.consumeAsync(inventory.getPurchase(ITEM_SKU), 
						//mConsumeFinishedListener);

						  System.out.println("Receiving inventory successful");
				    	
				    	for (int i = 0; i < purchaseSKU.size(); i++) {

							System.out.println("In array:" + purchaseSKU.get(i));


							///////

							System.out.println("Purchases in inventory: " + inventory.hasPurchase(purchaseSKU.get(i).toString()));


							//////
							Purchase purchase = inventory.getPurchase(purchaseSKU.get(i));

				    		if (purchase != null)
				    			mHelper.consumeAsync(purchase, mConsumeFinishedListener);

				    	}
				    	
			            //Purchase RemoveAds = inventory.getPurchase(removeAds);
			            
			           // Purchase fivekIngame = inventory.getPurchase(Buy5000Money);
			 
				      }
			    }
			};
			
			//Gives items if sale is consumed -this is where items are given-
			IabHelper.OnConsumeFinishedListener mConsumeFinishedListener =
					  new IabHelper.OnConsumeFinishedListener() {
					   public void onConsumeFinished(Purchase purchase, 
				             IabResult result) {
						   
					   // if we were disposed of in the meantime, quit.
			           if (mHelper == null) return;

						   System.out.println("Error in billing mConsumedFinishedListener");

						   if (mHelper != null)
							   mHelper.flagEndAsync();
				            
					 if (result.isSuccess()) {

						 
						 purchaseResult.get(purchase.getSku()).CompletePurchase();
						 
						/*if (purchase.getSku().equals(Buy5000Money)) {
							GameWorld.playerInfo.Money += 5000;
						} else if (purchase.getSku().equals(removeAds)) {
								GameWorld.bShowAds = false;
							}*/
						 
					 } else {
					         // handle error
						 //GameWorld.currentState = GameState.IAB;

						 System.out.println("Error in mConsumedFinishedListener!");
					 }
				  }
				};
		
		//Buys item method
		public void buyStuff(int i) {
			
			//if (mHelper != null) mHelper.flagEndAsync();
			
			//mHelper.launchPurchaseFlow(this, ITEM_SKU, 1001, mPurchaseFinishedListener, "hghj");

			String payload = "";

			Random rand = new Random();
			
			int RequestCode = rand.nextInt(999999999);
			
			if (mHelper != null && billable) {
		       
				try {
					
					mHelper.flagEndAsync();
	        		mHelper.launchPurchaseFlow(main, purchaseSKU.get(i), RequestCode, mPurchaseFinishedListener, payload);
					
			        	/*switch (i) {
			        	case 1:
			        		mHelper.flagEndAsync();
			        		mHelper.launchPurchaseFlow(main, removeAds, RequestCode, mPurchaseFinishedListener, payload);
			        		break;
			        	case 2:
			        		mHelper.flagEndAsync();
			        		mHelper.launchPurchaseFlow(main, Buy5000Money, RequestCode, mPurchaseFinishedListener, payload);
			        		break;
			        	}*/
			        }       
			        catch(IllegalStateException ex){
			        		main.runOnUiThread(new Runnable() 
						{
					   public void run() 
						   {
						   		mHelper.flagEndAsync();
						   		Toast.makeText(main.getApplicationContext(), "Please retry in a few seconds.", Toast.LENGTH_SHORT).show();    
						   }
					});
		        	
		        }
				
		    } else {

				Toast.makeText(main.getApplicationContext(), "Please update your google play services!",
						Toast.LENGTH_LONG).show();

			}

		}
		
		
		/** Verifies the developer payload of a purchase. */
	    boolean verifyDeveloperPayload(Purchase p) {
	        String payload = p.getDeveloperPayload();

			if (mHelper != null)
				mHelper.flagEndAsync();
	        
	        /*
	         * TODO: verify that the developer payload of the purchase is correct. It will be
	         * the same one that you sent when initiating the purchase.
	         *
	         * WARNING: Locally generating a random string when starting a purchase and
	         * verifying it here might seem like a good approach, but this will fail in the
	         * case where the user purchases an item on one device and then uses your app on
	         * a different device, because on the other device you will not have access to the
	         * random string you originally generated.
	         *
	         * So a good developer payload has these characteristics:
	         *
	         * 1. If two different users purchase an item, the payload is different between them,
	         *    so that one user's purchase can't be replayed to another user.
	         *
	         * 2. The payload must be such that you can verify it even when the app wasn't the
	         *    one who initiated the purchase flow (so that items purchased by the user on
	         *    one device work on other devices owned by the user).
	         *
	         * Using your own server to store and verify developer payloads across app
	         * installations is recommended.
	         */

	        return true;
	    }
	
	public void dispose() {
		
		if (mHelper != null) 
			mHelper.dispose();

		if (mHelper != null)
			mHelper.flagEndAsync();
		
	}

}
