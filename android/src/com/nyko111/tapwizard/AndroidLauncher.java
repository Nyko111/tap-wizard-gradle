package com.nyko111.tapwizard;

import android.content.Intent;
import android.os.Bundle;
import android.os.Debug;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.chartboost.sdk.CBLocation;
import com.chartboost.sdk.Chartboost;
import com.chartboost.sdk.ChartboostDelegate;
import com.chartboost.sdk.Model.CBError.CBImpressionError;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.adcolony.sdk.*;
import com.jirbo.adcolony.AdColonyAdapter;
import com.jirbo.adcolony.AdColonyBundleBuilder;
import com.nyko111.GameWorld.GameWorld;
import com.nyko111.IAB.NykoBilling;
import com.nyko111.IAB.PurchaseResult;
import com.nyko111.Interfaces.GoogleInterface;

//import android.os.Debug;


public class AndroidLauncher extends AndroidApplication implements GoogleInterface, RewardedVideoAdListener{

	private boolean signedin = false;

	private boolean InterstitialLoaded = false;

	NykoBilling nBilling;

	View gameView;

	//Play library stuff (leaderboards)
	//private GameHelper PlayHelper;

	//Admob stuff
	private AdView adView;
	private InterstitialAd interstitialView;
	private RewardedVideoAd mRewardedVideoAd;
	private AdRequest adRequest;

	private String flurrySpace = "Interstitial";
	private String flurryPaid = "Paid Video";


	//Inapp billing stuff
	public static final String TAG = "NykoDebug";

	private RelativeLayout layout;
	// Activity shopActivity = new Activity();

	private ChartboostDelegate chartboostDelegate = new ChartboostDelegate() {
		// Declare delegate methods here, see CBSample project for examples
		// Called after a rewarded video has been viewed completely and user is eligible for reward.
		public void didCompleteRewardedVideo(String location, int reward) {




			super.didCompleteRewardedVideo(location, reward);


		}

		@Override
		public void didFailToLoadInterstitial(String location, CBImpressionError error) {


			System.out.println(error.name());

		}


		@Override
		public void didFailToLoadRewardedVideo(String location, CBImpressionError error){

			System.out.println(error.name());

		}


	};

	public AndroidLauncher() {

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		//Create the layout
		layout = new RelativeLayout(this);

		//Do stuff that initialize would have done
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);

		//Play services stuff
		//PlayHelper = new GameHelper(this, GameHelper.CLIENT_GAMES);

		//PlayHelper.setup(this);
		//PlayHelper.setMaxAutoSignInAttempts(0);

		nBilling = new NykoBilling(this);

		//Create libgdx view
		gameView = initializeForView(new TapWizard(nBilling, this));

		//Create and setup admob view
		adView = new AdView(this);

		interstitialView = new InterstitialAd(this);

		// Aarki.registerApp(this, "jsXDXkiHjAJYUoBv1k8RQ1DTDi5A");


		//Create and setup admob view

		MobileAds.initialize(getApplicationContext(), "ca-app-pub-8505588892711814~3574936680");

		adRequest = new AdRequest.Builder()
				.addNetworkExtrasBundle(AdColonyAdapter.class, AdColonyBundleBuilder.build())
				//.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
				//.addTestDevice("B669D862E7A004D0393C830E7733597E")
				.build();

		//adView.loadAd(new AdRequest());
		adView.setAdSize(AdSize.BANNER);
		adView.setAdUnitId("ca-app-pub-8505588892711814/7144939080");

		interstitialView.setAdUnitId("ca-app-pub-8505588892711814/5051669884");

		adActive(true);



		layout.addView(gameView);

		RelativeLayout.LayoutParams adParams =
				new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
						RelativeLayout.LayoutParams.WRAP_CONTENT);
		adParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		adParams.addRule(RelativeLayout.CENTER_HORIZONTAL);


		layout.addView(adView, adParams);


		setContentView(layout);

		AdColony.configure(this, "store:google", "app8789c7ea528e485aa8", "vzc9d08bb96e4e49c4b9");

		Chartboost.startWithAppId(this, "58055819f6cd452969e01d87", "62403f4c501f646ed8f6c4e707db5090d131007b");

		Chartboost.onCreate(this);
		Chartboost.setDelegate(chartboostDelegate);

		mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(this);
		mRewardedVideoAd.setRewardedVideoAdListener(this);

		//IAB Stuff
		nBilling.AddPurchaseItem("1000_starballs", new PurchaseResult() {
			@Override
			public void CompletePurchase() {

				GameWorld.playerInfo.Money += 1000;

			}
		});

		nBilling.AddPurchaseItem("5000_starballs", new PurchaseResult() {
			@Override
			public void CompletePurchase() {

				GameWorld.playerInfo.Money += 5000;

			}
		});

		nBilling.AddPurchaseItem("10000_starballs", new PurchaseResult() {
			@Override
			public void CompletePurchase() {

				GameWorld.playerInfo.Money += 10000;

			}
		});

		nBilling.AddPurchaseItem("25000_starballs", new PurchaseResult() {
			@Override
			public void CompletePurchase() {

				GameWorld.playerInfo.Money += 25000;

			}
		});

		nBilling.AddPurchaseItem("50000_starballs", new PurchaseResult() {
			@Override
			public void CompletePurchase() {

				GameWorld.playerInfo.Money += 50000;

			}
		});

		nBilling.StartIAB();

		//Keep screen on
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);


	}

	@Override
	public void onStart(){
		//PlayHelper.onStart(this);

		super.onStart();

		Chartboost.onStart(this);

		//Chartboost.cacheInterstitial(CBLocation.LOCATION_ACHIEVEMENTS);

		mRewardedVideoAd.loadAd("ca-app-pub-8505588892711814/9481869481", new AdRequest.Builder().build());

		//Chartboost.cacheRewardedVideo(CBLocation.LOCATION_GAMEOVER);

		//  mGoogleClient.connect();

	}

	@Override
	public void onStop(){
		//PlayHelper.onStop();

		super.onStop();

		Chartboost.onStop(this);

	}

	@Override
	public void onPause() {

		//adView.pause();

		GameWorld.bPaused = true;


		super.onPause();

		Chartboost.onPause(this);
	}

	@Override
	public void onResume() {

		adView.resume();

		super.onResume();

		Chartboost.onResume(this);

		GameWorld.bPaused = false;

	}

	@Override
	public void onDestroy() {

		if (adView != null)
			adView.destroy();

		adView = null;

		Chartboost.onDestroy(this);

		nBilling.dispose();

		super.onDestroy();

	}

	//Identifies if it was called as a result of inapp purchase or something else, if app request mHelper will handle it
	@Override
	protected void onActivityResult(int requestCode, int resultCode,
									Intent data)
	{
		if (nBilling.mHelper == null) return;

		if (!nBilling.mHelper.handleActivityResult(requestCode,
				resultCode, data)) {
			super.onActivityResult(requestCode, resultCode, data);
			//PlayHelper.onActivityResult(requestCode, resultCode, data);
		} else {

			nBilling.mHelper.handleActivityResult(requestCode, resultCode, data);
			Log.d(TAG, "onActivityResult handled by IABUtil.");
		}

	}

	public void adActive(final Boolean bActive) {

		runOnUiThread(new Runnable() {
			@Override
			public void run() {

				if (bActive && !adView.isActivated()) {
					//adView.setActivated(true);
					//adView.setVisibility(View.VISIBLE);

					try {
						interstitialView.loadAd(adRequest);
					} catch (NoClassDefFoundError ex) {

					}


				} else if (!bActive && adView.isActivated()){
					//adView.setActivated(false);
					//adView.setVisibility(View.GONE);
				}
			}
		});

	}

	public void Interstitial() {

		/*if (FlurryAds.isAdReady(flurrySpace))
			FlurryAds.displayAd(this, flurrySpace, adView);*/

		//if (Chartboost.hasInterstitial(CBLocation.LOCATION_ACHIEVEMENTS)) {
		//Chartboost.showInterstitial(CBLocation.LOCATION_ACHIEVEMENTS);
		//GameWorld.bInterstitialShown = true;
		//} else {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (interstitialView !=null && interstitialView.isLoaded()) {
					interstitialView.show();
					GameWorld.bInterstitialShown = true;
				}
			}

		});
		//}

	}

	public void PaidAd() {


		runOnUiThread(new Runnable() {
			@Override
			public void run() {

				if (mRewardedVideoAd != null && mRewardedVideoAd.isLoaded())
					mRewardedVideoAd.show();
				else
					mRewardedVideoAd.loadAd("ca-app-pub-8505588892711814/9481869481", new AdRequest.Builder().build());

			}

		});


	}

	public void PayAd() {

		Chartboost.getDelegate().didCompleteRewardedVideo(CBLocation.LOCATION_GAMEOVER, 100);

	}

	//Google play stuff
	public void onSignInFailed() {
		System.out.println("sign in failed");
	}

	public void onSignInSucceeded() {
		System.out.println("sign in succeeded");
	}


	@Override
	public void Login() {

		try {
			runOnUiThread(new Runnable(){

				//@Override
				public void run(){
					if (!signedin) {}
						//PlayHelper.beginUserInitiatedSignIn();
				}
			});
		} catch (final Exception ex){

		}
	}

	@Override
	public void LogOut() {
		try {
			runOnUiThread(new Runnable(){

				//@Override
				public void run(){
					if (signedin){}
						//PlayHelper.signOut();
				}
			});
		} catch (final Exception ex){

		}

	}

	@Override
	public boolean getSignedIn() {
		return false;
	}

	@Override
	public void submitScore(int score) {
		System.out.println("in submit score");

		/*if (PlayHelper.isSignedIn())
			Games.Leaderboards.submitScore(PlayHelper.getApiClient(), getString(R.string.leaderboard_main), score);
		else
			Login();*/
	}

	@Override
	public void getScores() {
		//startActivityForResult(Games.Leaderboards.getLeaderboardIntent(getString(R.string.leaderBoardID)), 105);

		//startActivityForResult(Games.Leaderboards.getLeaderboardIntent(PlayHelper.getApiClient(), getString(R.string.leaderboard_main)), 105);

	}

	@Override
	public void onRewarded(RewardItem arg0) {
		// TODO Auto-generated method stub

		//USE THIS TO GIVE PEOPLE REWARDS FROM VIDEOS
		GameWorld.playerInfo.Money += 100;

	}

	@Override
	public void onRewardedVideoAdClosed() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onRewardedVideoAdFailedToLoad(int errorCode) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onRewardedVideoAdLeftApplication() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onRewardedVideoAdLoaded() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onRewardedVideoAdOpened() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onRewardedVideoStarted() {
		// TODO Auto-generated method stub

	}

}
