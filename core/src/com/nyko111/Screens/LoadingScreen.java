package com.nyko111.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Interpolation;
import com.nyko111.Interfaces.GoogleInterface;
import com.nyko111.Interfaces.IabInterface;
import com.nyko111.tapwizard.TapWizard;
import com.nykoengine.Helpers.Assetloader;
import com.nykoengine.UserInterface.FillableBar;

import java.text.DecimalFormat;

public class LoadingScreen implements Screen {
	
    SpriteBatch batch = new SpriteBatch();
	
	IabInterface platformInterface;
	GoogleInterface googleInterface;
	GameScreen game;
	TapWizard splodygame;

	FillableBar loadingBar;

	Texture splashscreen, LoadingBarEnd, LoadingBarEndAlpha, LoadingBarEndTop, LoadingBarMid, LoadingBarMidAlpha, LoadingBarFill;

	Float percent = 0f;

	//DecimalFormat df = new DecimalFormat("###.#");

	//String sProgress = "0";

	public LoadingScreen(IabInterface aInt, GoogleInterface googleInterface, TapWizard splodygame) {
		platformInterface = aInt;
		this.googleInterface = googleInterface;
		this.splodygame = splodygame;

		if (Gdx.graphics.getHeight() > 1280) {
			splashscreen = new Texture(Gdx.files.internal("data/Large Atlas/SplashScreen_Large.png"));

			LoadingBarEnd = new Texture(Gdx.files.internal("data/Large Atlas/LoadingBar/ManaEnd.png"));

			LoadingBarEndAlpha = new Texture(Gdx.files.internal("data/Large Atlas/LoadingBar/ManaEndAlpha.png"));

			LoadingBarEndTop = new Texture(Gdx.files.internal("data/Large Atlas/LoadingBar/ManaEndTop.png"));

			LoadingBarMid = new Texture(Gdx.files.internal("data/Large Atlas/LoadingBar/ManaMid.png"));

			LoadingBarMidAlpha = new Texture(Gdx.files.internal("data/Large Atlas/LoadingBar/ManaMidAlpha.png"));

			LoadingBarFill = new Texture(Gdx.files.internal("data/Large Atlas/LoadingBar/ManaFill.png"));

		} else {

			splashscreen = new Texture(Gdx.files.internal("data/Small Atlas/SplashScreen_Small.png"));

			LoadingBarEnd = new Texture(Gdx.files.internal("data/Small Atlas/LoadingBar/ManaEnd.png"));

			LoadingBarEndAlpha = new Texture(Gdx.files.internal("data/Small Atlas/LoadingBar/ManaEndAlpha.png"));

			LoadingBarEndTop = new Texture(Gdx.files.internal("data/Small Atlas/LoadingBar/ManaEndTop.png"));

			LoadingBarMid = new Texture(Gdx.files.internal("data/Small Atlas/LoadingBar/ManaMid.png"));

			LoadingBarMidAlpha = new Texture(Gdx.files.internal("data/Small Atlas/LoadingBar/ManaMidAlpha.png"));

			LoadingBarFill = new Texture(Gdx.files.internal("data/Small Atlas/LoadingBar/ManaFill.png"));

		}

		loadingBar = new FillableBar(Assetloader.screenWidth / 4f, Assetloader.screenHeight / 4f, 100f, 11.75f, new Sprite(LoadingBarEnd), new Sprite(LoadingBarMid), new Sprite(LoadingBarEndTop), new Sprite(LoadingBarEndAlpha), new Sprite(LoadingBarMidAlpha), new Sprite(LoadingBarFill), FillableBar.BarType.HORIZONTALBAR);

		loadingBar.setBarFillOffset(Assetloader.screenWidth / 94.11f);

		//loadingBar.Reset();

		loadingBar.EmptyCompletely();

	}
	
	public LoadingScreen() {

	}


	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		 Gdx.graphics.getGL20().glClear( GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT );

		batch.begin();

		batch.draw(splashscreen, 0, 0, splashscreen.getWidth() * Assetloader.gameScaleX, splashscreen.getHeight() * Assetloader.gameScaleY);

		//Assetloader.MainFont.setColor(Color.BLACK);
		//Assetloader.MainFont.draw(batch, sProgress, Assetloader.screenWidth / 2.5f, Assetloader.screenHeight / 6);
		//Assetloader.MainFont.setColor(Color.WHITE);

		batch.end();


		percent = Interpolation.linear.apply(percent, Assetloader.manager.getProgress(), 0.1f);

		loadingBar.update(Gdx.graphics.getDeltaTime());

		//loadingBar.IncrementFill(percent * 100);

		loadingBar.Value = (percent * 100);

		loadingBar.draw(batch);

		//System.out.println("Diag: " + Assetloader.manager.getLoadedAssets());

		//System.out.println("Current Progress: " + Assetloader.manager.getProgress());

		if (Assetloader.manager.update()) {

			if (Assetloader.screenHeight > 1280) {
				//Assetloader.loadLargeTextures();

				Assetloader.createSpritesFromAtlas("MainAtlas", "data/Large Atlas/MainLarge");

				Assetloader.createSpritesFromAtlasBG("MenuBGAtlas", "data/Large Atlas/BG/MainBGLarge");

			} else {
				//Assetloader.loadSmallTextures();

				Assetloader.createSpritesFromAtlas("MainAtlas", "data/Small Atlas/MainSmall");

				Assetloader.createSpritesFromAtlasBG("MenuBGAtlas", "data/Small Atlas/BG/MainBGSmall");


			}

			Assetloader.CreateFont(Assetloader.screenWidth / 10.66f, "MainFont", Assetloader.fonts.get("Enigma2"), Color.WHITE);

			Assetloader.CreateFont(Assetloader.screenWidth / 8f, "MainWiz", Assetloader.fonts.get("TapWizFont"), Color.WHITE);
			Assetloader.CreateFont(Assetloader.screenWidth / 6.5f, "NextLevel", Assetloader.fonts.get("TapWizFont"), Color.WHITE);
			Assetloader.CreateFont(Assetloader.screenWidth / 20f, "ShopLevelFont", Assetloader.fonts.get("TapWizFont"), Color.WHITE);

			Assetloader.CreateFont(Assetloader.screenWidth / 14f,"ShopUpgradeFont", Assetloader.fonts.get("TapWizFont"), Color.WHITE);

			//Assetloader.CreateFont(Assetloader.screenWidth / 8f, "LevelButtonFont", Assetloader.fonts.get("TapWizFont"), Color.WHITE);

			Assetloader.CreateFont(Assetloader.screenWidth / 9f, "SettingsMenuFont", Assetloader.fonts.get("TapWizFont"), Color.WHITE);



			Assetloader.generateAudio();

			game = new GameScreen(platformInterface, googleInterface);

			splodygame.setScreen(game);

			LoadingBarEnd.dispose();

			LoadingBarEndAlpha.dispose();

			LoadingBarEndTop.dispose();

			LoadingBarFill.dispose();

			LoadingBarMid.dispose();

			LoadingBarMidAlpha.dispose();

			loadingBar.dispose();

			splashscreen.dispose();
			
			batch.dispose();
			
		}


		
	}


	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		if (Assetloader.screenHeight > 1280) {
			//Assetloader.loadLargeAssets();
			Assetloader.loadTextureAtlas("data/Large Atlas/MainLarge");
			Assetloader.loadTextureAtlas("data/Large Atlas/BG/MainBGLarge");
		} else {
			//Assetloader.loadSmallAssets();
			Assetloader.loadTextureAtlas("data/Small Atlas/MainSmall");
			Assetloader.loadTextureAtlas("data/Small Atlas/BG/MainBGSmall");
		}

		Assetloader.loadAudio();
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		//loadingfontgen.dispose();
		//loadingfont.dispose();
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		if (game != null) {
			game.dispose();
		}
		
	}

}
