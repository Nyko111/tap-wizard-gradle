package com.nyko111.Screens;

import com.badlogic.gdx.Screen;
import com.nyko111.GameWorld.GameRenderer;
import com.nyko111.GameWorld.GameWorld;
import com.nyko111.Interfaces.GoogleInterface;
import com.nyko111.Interfaces.IabInterface;

public class GameScreen implements Screen {

	private GameWorld world;
	private GameRenderer renderer;
	//private float runTime;
	//private float deltaTime;
	//private Stage stage, uiStage;
	
	//FPSLogger fps;
	
	
	public GameScreen(IabInterface aInt, GoogleInterface googleInterface) {        
		
		world = new GameWorld(aInt, googleInterface);
		renderer = new GameRenderer(world);
		
		//fps = new FPSLogger();
	}

	@Override
	public void render(float delta) {
		//runTime += delta;
		//deltaTime = delta;

		world.update(delta);
		renderer.render(delta);

	}

	@Override
	public void resize(int width, int height) {
		System.out.println("Risizing screen!");
		
	}

	@Override
	public void show() {
		System.out.println("Show called!");
		
	}

	@Override
	public void hide() {
		//Gdx.app.exit();
		//Assetloader.manager.clear();
		System.out.println("hide called!");
		
	}

	@Override
	public void pause() {
		System.out.println("Paused!");
		
	}

	@Override
	public void resume() {
		System.out.println("Resuming!");
		
	}

	@Override
	public void dispose() {
		
		world.dispose();
		renderer.dispose();
		
	}


}
