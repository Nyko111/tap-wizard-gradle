package com.nyko111.Menus;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.nyko111.GameWorld.GameWorld;
import com.nykoengine.Helpers.Assetloader;
import com.nykoengine.UserInterface.TextButton;

public class PausedMenu extends SettingsMenu{

	TextButton Menu;
	
	public PausedMenu(GameWorld world) {
		super(world);
		
		Menu = new TextButton(Assetloader.screenWidth / 1.7f, Assetloader.screenHeight / 3.7f, Assetloader.allSpritesMap.get("NextLevelMenuButton"), null, Assetloader.bitmapfonts.get("ShopUpgradeFont"), "Menu", Assetloader.sounds.get("buttonclick"), false);
		
		Menu.addListener(new InputListener(){
			 public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				 	Menu.Pushed();
	   
				 	
	            	
	                return true;
	            }
	            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
	            	Menu.pushed = false;
	            	
	            	//GameWorld.mainStage.spawnList.bStopSpawning = true;
	            	
	            	/*for (int i = 0; i < GameWorld.mainStage.enemyArray.size(); i++) {
	            		GameWorld.mainStage.enemyArray.remove(i);
	            	}*/
	            		
	            	GameWorld.state.changeState("LevelSelect");
	            
	            }
	        });
		
		this.addActor(Menu);
		
	}

	
}
