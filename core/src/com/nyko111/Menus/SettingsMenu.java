package com.nyko111.Menus;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.nyko111.GameWorld.GameRenderer;
import com.nyko111.GameWorld.GameWorld;
import com.nykoengine.Helpers.Assetloader;
import com.nykoengine.UserInterface.GameButton;
import com.nykoengine.UserInterface.TextButton;

public class SettingsMenu extends Stage {
	
	TextButton Return;
	
	GameButton SoundLast, SoundNext, MusicLast, MusicNext;
	
	int SoundBars = 3, MusicBars = 3;
	
	Sprite SoundBar1, SoundBar2, SoundBar3, MusicBar1, MusicBar2, MusicBar3;
	
	float PopupX, PopupY;

	GlyphLayout glyphLayout;

	GameWorld world;
	
	public SettingsMenu(GameWorld world) {

		this.world = world;

		PopupX = Assetloader.screenWidth / 12.3f;
		
		PopupY = Assetloader.screenHeight / 3.1f;
		
		Return = new TextButton(((Assetloader.screenWidth / 2f) - ((Assetloader.allSpritesMap.get("BuyButton").getWidth() * Assetloader.gameScaleX) / 2f)), Assetloader.screenHeight / 8f, Assetloader.allSpritesMap.get("BuyButton"), null, Assetloader.bitmapfonts.get("MainWiz"), "Return", Assetloader.sounds.get("buttonclick"), false);

		Return.addListener(new InputListener(){
			 public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				 	Return.Pushed();
	   
				 	
	            	
	                return true;
	            }
	            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
	            	Return.pushed = false;

					GameWorld.state.changeState("Playing");
	            	
	            	//GameWorld.state.revertToPreviousState();
	            
	            }
	        });
		
		SoundLast = new GameButton(Assetloader.screenWidth / 2.04f, Assetloader.screenHeight / 1.465f, Assetloader.allSpritesMap.get("SmallLastArrow"), null, Assetloader.sounds.get("buttonclick"), false);
		
		SoundLast.addListener(new InputListener(){
			 public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				 	SoundLast.Pushed();
	   
				 	
	            	
	                return true;
	            }
	            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
	            	SoundLast.pushed = false;
	            	
	            	if (SoundBars > 0) {
	            		SoundBars -= 1;
	            	}
	            	
	            	SetSoundBar(SoundBars);
	            
	            }
	        });
		
		
		
		MusicLast = new GameButton(Assetloader.screenWidth / 2.04f, Assetloader.screenHeight / 2f, Assetloader.allSpritesMap.get("SmallLastArrow"), null, Assetloader.sounds.get("buttonclick"), false);
		
		MusicLast.addListener(new InputListener(){
			 public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				 	MusicLast.Pushed();
	   
				 	
	            	
	                return true;
	            }
	            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
	            	MusicLast.pushed = false;
	            	
	            	if (MusicBars > 0) {
	            		MusicBars -= 1;
	            	}
	            	
	            	SetMusicBar(MusicBars);
	            
	            }
	        });
		
		SoundNext = new GameButton(Assetloader.screenWidth / 1.257f, Assetloader.screenHeight / 1.465f, Assetloader.allSpritesMap.get("SmallNextArrow"), null, Assetloader.sounds.get("buttonclick"), false);
		
		SoundNext.addListener(new InputListener(){
			 public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				 	SoundNext.Pushed();
	   
				 	
	            	
	                return true;
	            }
	            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
	            	SoundNext.pushed = false;
	            	
	            	if (SoundBars < 3) {
	            		SoundBars += 1;
	            	}
	            	
	            	SetSoundBar(SoundBars);
	            
	            }
	        });
		
		
		
		MusicNext = new GameButton(Assetloader.screenWidth / 1.257f, Assetloader.screenHeight / 2f, Assetloader.allSpritesMap.get("SmallNextArrow"), null, Assetloader.sounds.get("buttonclick"), false);
		
		MusicNext.addListener(new InputListener(){
			 public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				 	MusicNext.Pushed();
	   
				 	
	            	
	                return true;
	            }
	            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
	            	MusicNext.pushed = false;
	            	
	            	if (MusicBars < 3) {
	            		MusicBars += 1;
	            	}
	            	
	            	SetMusicBar(MusicBars);
	            
	            }
	        });
		
		SetSoundBar(3);
		SetMusicBar(3);
		
		glyphLayout = new GlyphLayout(Assetloader.bitmapfonts.get("NextLevel"), "Settings");
		
		this.addActor(Return);
		this.addActor(SoundLast);
		this.addActor(SoundNext);
		this.addActor(MusicLast);
		this.addActor(MusicNext);
		
		
	}

	public void SetSoundBar(int Bars) {
		
		switch (Bars) {
		case 0: 
			SoundBar1 = Assetloader.allSpritesMap.get("UnlitBar");
			SoundBar2 = Assetloader.allSpritesMap.get("UnlitBar");
			SoundBar3 = Assetloader.allSpritesMap.get("UnlitBar");
			world.audioManager.SoundVolume = 0;
			break;
		case 1:
			SoundBar1 = Assetloader.allSpritesMap.get("LitBar");
			SoundBar2 = Assetloader.allSpritesMap.get("UnlitBar");
			SoundBar3 = Assetloader.allSpritesMap.get("UnlitBar");
			world.audioManager.SoundVolume = 0.33f;
			break;
		case 2:
			SoundBar1 = Assetloader.allSpritesMap.get("LitBar");
			SoundBar2 = Assetloader.allSpritesMap.get("LitBar");
			SoundBar3 = Assetloader.allSpritesMap.get("UnlitBar");
			world.audioManager.SoundVolume = 0.66f;
			break;
		case 3:
			SoundBar1 = Assetloader.allSpritesMap.get("LitBar");
			SoundBar2 = Assetloader.allSpritesMap.get("LitBar");
			SoundBar3 = Assetloader.allSpritesMap.get("LitBar");
			world.audioManager.SoundVolume = 1;
			break;
		
		}
		
	}
	
	public void SetMusicBar(int Bars) {
		
		switch (Bars) {
		case 0: 
			MusicBar1 = Assetloader.allSpritesMap.get("UnlitBar");
			MusicBar2 = Assetloader.allSpritesMap.get("UnlitBar");
			MusicBar3 = Assetloader.allSpritesMap.get("UnlitBar");
			world.audioManager.SetMusicVolume(0);
			break;
		case 1:
			MusicBar1 = Assetloader.allSpritesMap.get("LitBar");
			MusicBar2 = Assetloader.allSpritesMap.get("UnlitBar");
			MusicBar3 = Assetloader.allSpritesMap.get("UnlitBar");
			world.audioManager.SetMusicVolume(0.33f);
			break;
		case 2:
			MusicBar1 = Assetloader.allSpritesMap.get("LitBar");
			MusicBar2 = Assetloader.allSpritesMap.get("LitBar");
			MusicBar3 = Assetloader.allSpritesMap.get("UnlitBar");
			world.audioManager.SetMusicVolume(0.66f);
			break;
		case 3:
			MusicBar1 = Assetloader.allSpritesMap.get("LitBar");
			MusicBar2 = Assetloader.allSpritesMap.get("LitBar");
			MusicBar3 = Assetloader.allSpritesMap.get("LitBar");
			world.audioManager.SetMusicVolume(1);
			break;
		
		}
	}
	
	@Override
	public void draw() {
		
		//draw popup
		
		GameRenderer.batch.begin();
		
		GameRenderer.batch.draw(Assetloader.allBGMap.get("ShopMenuBG"), Assetloader.screenWidth / 22.85f, Assetloader.screenHeight / 10f, Assetloader.allBGMap.get("ShopMenuBG").getWidth() * Assetloader.gameScaleX, Assetloader.allBGMap.get("ShopMenuBG").getHeight() * Assetloader.gameScaleY);

		Assetloader.bitmapfonts.get("NextLevel").draw(GameRenderer.batch, glyphLayout, (Assetloader.screenWidth / 2) - (glyphLayout.width / 2), Assetloader.screenHeight / 1.05f);
		
		GameRenderer.batch.draw(SoundBar1, Assetloader.screenWidth / 1.65f, Assetloader.screenHeight / 1.47f, SoundBar1.getWidth() * Assetloader.gameScaleX, SoundBar1.getHeight() * Assetloader.gameScaleY);
		GameRenderer.batch.draw(SoundBar2, Assetloader.screenWidth / 1.49f, Assetloader.screenHeight / 1.47f, SoundBar2.getWidth() * Assetloader.gameScaleX, SoundBar2.getHeight() * Assetloader.gameScaleY);
		GameRenderer.batch.draw(SoundBar3, Assetloader.screenWidth / 1.37f, Assetloader.screenHeight / 1.47f, SoundBar3.getWidth() * Assetloader.gameScaleX, SoundBar3.getHeight() * Assetloader.gameScaleY);
		
		GameRenderer.batch.draw(MusicBar1, Assetloader.screenWidth / 1.65f, Assetloader.screenHeight / 2f, MusicBar1.getWidth() * Assetloader.gameScaleX, MusicBar1.getHeight() * Assetloader.gameScaleY);
		GameRenderer.batch.draw(MusicBar2, Assetloader.screenWidth / 1.49f, Assetloader.screenHeight / 2f, MusicBar2.getWidth() * Assetloader.gameScaleX, MusicBar2.getHeight() * Assetloader.gameScaleY);
		GameRenderer.batch.draw(MusicBar3, Assetloader.screenWidth / 1.37f, Assetloader.screenHeight / 2f, MusicBar3.getWidth() * Assetloader.gameScaleX, MusicBar3.getHeight() * Assetloader.gameScaleY);
		
		Assetloader.bitmapfonts.get("SettingsMenuFont").draw(GameRenderer.batch, "Sound", Assetloader.screenWidth / 7.37f, Assetloader.screenHeight / 1.34f);
		
		Assetloader.bitmapfonts.get("SettingsMenuFont").draw(GameRenderer.batch, "Music", Assetloader.screenWidth / 7.37f, Assetloader.screenHeight / 1.76f);
		
		GameRenderer.batch.end();


		super.draw();
		
		
		
	}
}
