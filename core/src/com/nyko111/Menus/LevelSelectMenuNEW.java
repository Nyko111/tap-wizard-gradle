package com.nyko111.Menus;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.nyko111.GameWorld.GameWorld;
import com.nyko111.GameWorld.Level;
import com.nyko111.UserInterfaces.LevelButton;
import com.nykoengine.Helpers.Assetloader;
import com.nykoengine.UserInterface.GameButton;
import com.nykoengine.UserInterface.TextButton;

/**
 * Created by MacUser on 12/22/16.
 */
public class LevelSelectMenuNEW extends Stage {

    int LevelAmount;

    int CurrentLevelSet = 15;

    Integer LevelNumber = 0;

    int LevelButtonNumber = 1;

    public Level[] levelArray;

    public LevelButton[] levelButtons, currentLevelButtons;

    Integer TextLevelNumber = 1;

    int BossSlimes = 0, IceBossSlimes = 0, FireBossSlimes = 0;

    GameButton Next, Shop, Login, Last;

    public float TextButtonX, TextButtonY, TextButtonXorig, TextButtonYorig;

    public float TextButtonXPadding, TextButtonYPadding;

    GameWorld world;

    public LevelSelectMenuNEW(int LevelAmount, final GameWorld world) {


        //Fonts


        this.world = world;

        this.LevelAmount = LevelAmount;

        levelArray = new Level[LevelAmount];

        levelButtons = new LevelButton[LevelAmount];

        currentLevelButtons = new LevelButton[16];

        TextButtonXorig = Assetloader.screenWidth / 15.686f;

        TextButtonYorig = Assetloader.screenHeight / 1.219f;

        TextButtonX = Assetloader.screenWidth / 15.686f;

        TextButtonY = Assetloader.screenHeight / 1.219f;

        TextButtonXPadding = Assetloader.screenWidth / 4.21f;

        TextButtonYPadding = Assetloader.screenHeight / 5.12f;

        for (;LevelAmount > LevelNumber; LevelNumber++) {

            if (TextButtonX >= Assetloader.screenWidth / 1.269f) {

                TextButtonY -= TextButtonYPadding;

                TextButtonX = TextButtonXorig;

            }

            if (TextButtonY < Assetloader.screenHeight / 17) {

                TextButtonY = TextButtonYorig;

            }


            final Level level = new Level(LevelNumber);

            levelArray[LevelNumber] = level;

            final int CurrentLevel = LevelNumber;

            final LevelButton levelbutton = new LevelButton(TextButtonX, TextButtonY, Assetloader.allSpritesMap.get("LevelButton"), Assetloader.allSpritesMap.get("LevelButtonLock"), Assetloader.bitmapfonts.get("MainWiz"), TextLevelNumber.toString(), Assetloader.sounds.get("buttonclick"), false);

            levelbutton.isLocked = true;

            levelbutton.addListener(new InputListener(){
                public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                    levelbutton.Pushed();



                    return true;
                }
                public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                    levelbutton.pushed = false;

                    if (!levelbutton.isLocked) {

                        GameWorld.state.changeState("Playing");

                        world.CurrentLevel = CurrentLevel;

                        level.Start(world);

                    } else {

                        Assetloader.sounds.get("Locked").play();

                    }
                }
            });

            levelButtons[LevelButtonNumber - 1] = levelbutton;

            TextButtonX += TextButtonXPadding;

            TextLevelNumber += 1;

            LevelButtonNumber += 1;

        }

        Next = new GameButton(Assetloader.screenWidth / 1.265f, Assetloader.screenHeight / 16f, Assetloader.allSpritesMap.get("NextArrow"), null, Assetloader.sounds.get("buttonclick"), false);

        Next.addListener(new InputListener(){
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                Next.Pushed();



                return true;
            }
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                Next.pushed = false;

                NextPage();

            }
        });


        Shop = new TextButton(((Assetloader.screenWidth / 2f) - ((Assetloader.allSpritesMap.get("NextLevelMenuButton").getWidth() * Assetloader.gameScaleX) / 2f)), Assetloader.screenHeight / 8f, Assetloader.allSpritesMap.get("NextLevelMenuButton"), null, Assetloader.bitmapfonts.get("ShopUpgradeFont"), "Shop", Assetloader.sounds.get("buttonclick"), false);

        Shop.addListener(new InputListener(){
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                Shop.Pushed();



                return true;
            }
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                Shop.pushed = false;

                GameWorld.state.changeState("Shop");

                world.shopMenu.previousState = "MainMenu";
            }
        });

        Login = new TextButton(((Assetloader.screenWidth / 2f) - ((Assetloader.allSpritesMap.get("NextLevelMenuButton").getWidth() * Assetloader.gameScaleX) / 2f)), Assetloader.screenHeight / 96f, Assetloader.allSpritesMap.get("NextLevelMenuButton"), null, Assetloader.bitmapfonts.get("ShopUpgradeFont"), "Login", Assetloader.sounds.get("buttonclick"), false);

        Login.addListener(new InputListener(){
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                Login.Pushed();



                return true;
            }
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                Login.pushed = false;

                GameWorld.previousState = "LevelSelect";

                GameWorld.state.changeState("Login");

            }
        });

        /* Shop = new TextButton(((Assetloader.screenWidth / 2f) - ((Assetloader.allSpritesMap.get("EnterShopButton").getWidth() * Assetloader.gameScaleX) / 2f)), Assetloader.screenHeight / 16f, Assetloader.allSpritesMap.get("EnterShopButton"), null, GameWorld.Main, "Shop", Assetloader.sounds.get("buttonclick"), false);

        Shop.addListener(new InputListener(){
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                Shop.Pushed();



                return true;
            }
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                Shop.pushed = false;

                GameWorld.shopMenu.previousState = "LevelSelect";

                GameWorld.state.changeState("Shop");

            }
        });*/

        Last = new GameButton(Assetloader.screenWidth / 20f, Assetloader.screenHeight / 16f, Assetloader.allSpritesMap.get("LastArrow"), null, Assetloader.sounds.get("buttonclick"), false);

        Last.addListener(new InputListener(){
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                Last.Pushed();



                return true;
            }
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                Last.pushed = false;

                LastPage();

            }
        });


        PopulateButtons();


    }

    public void PopulateButtons() {

        this.clear();

        this.addActor(Next);
        this.addActor(Shop);
        this.addActor(Login);
        this.addActor(Last);

        for (int LevelSet = (CurrentLevelSet + 1) - 16, y = 0; LevelSet < (CurrentLevelSet + 1); LevelSet++, y++) {

            currentLevelButtons[y] = levelButtons[LevelSet];

            this.addActor(currentLevelButtons[y]);

        }

        /*for (int i = 0; i < pageController.pageArray.get(pageController.PageArraySelected).LevelButtons.length; i++) {

            if (pageController.pageArray.get(pageController.PageArraySelected).LevelButtons[i] != null)
                this.addActor(pageController.pageArray.get(pageController.PageArraySelected).LevelButtons[i]);

        }*/

    }

    public void NextPage() {

        if (CurrentLevelSet + 16 < LevelAmount) {
            CurrentLevelSet += 16;
        } else {

            CurrentLevelSet = LevelAmount - 1;
        }
        PopulateButtons();

    }

    public void LastPage() {

        if (CurrentLevelSet - 16 > 0) {
            CurrentLevelSet -= 16;
        } else {

            CurrentLevelSet = 16 - 1;
        }
        PopulateButtons();


    }

    @Override
    public void dispose() {


        for (int i = 0; i < levelButtons.length; i++) {

            if (levelButtons[i] != null)
                levelButtons[i].dispose();

        }

        super.dispose();


    }

}
