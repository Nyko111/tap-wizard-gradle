package com.nyko111.Menus;

import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.nyko111.GameWorld.GameRenderer;
import com.nyko111.GameWorld.GameWorld;
import com.nykoengine.Helpers.Assetloader;
import com.nykoengine.UserInterface.TextButton;

public class GameOverMenu extends Stage {

	TextButton Menu, Shop, Retry;

	GlyphLayout glyphLayout = new GlyphLayout();

	float PopupX, PopupY;

	GameWorld world;

	public GameOverMenu(final GameWorld world) {

		this.world = world;

		PopupX = ((Assetloader.screenWidth / 2f) - ((Assetloader.allBGMap.get("next level menu").getWidth()) / 2) * Assetloader.gameScaleX);

		PopupY = Assetloader.screenHeight / 6.8f;

		Menu = new TextButton(Assetloader.screenWidth / 22f, Assetloader.screenHeight / 6f, Assetloader.allSpritesMap.get("NextLevelMenuButton"), null, Assetloader.bitmapfonts.get("ShopUpgradeFont"), "Menu", Assetloader.sounds.get("buttonclick"), false);

		Menu.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				Menu.Pushed();


				return true;
			}

			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				Menu.pushed = false;

				world.state.changeState("MainMenu");

			}
		});

		Shop = new TextButton(Assetloader.screenWidth / 2.95f, Assetloader.screenHeight / 3.6f, Assetloader.allSpritesMap.get("NextLevelMenuButton"), null, Assetloader.bitmapfonts.get("ShopUpgradeFont"), "Shop", Assetloader.sounds.get("buttonclick"), false);

		Shop.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				Shop.Pushed();


				return true;
			}

			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				Shop.pushed = false;

				world.state.changeState("Shop");

				world.previousState = "GameOver";

			}
		});

		Retry = new TextButton(Assetloader.screenWidth / 1.575f, Assetloader.screenHeight / 6f, Assetloader.allSpritesMap.get("NextLevelMenuButton"), null, Assetloader.bitmapfonts.get("ShopUpgradeFont"), "Retry", Assetloader.sounds.get("buttonclick"), false);

		Retry.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				Retry.Pushed();


				return true;
			}

			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				Retry.pushed = false;

				world.levelSelectMenu.levelArray[world.CurrentLevel].Start(world);

				world.mainStage.spellArray.clear();
				world.mainStage.clear();
				world.mainStage.enemyArray.clear();
				world.particleEmitterArray.clear();
				world.mainStage.Score = 0;
				world.mainStage.manaBar.Reset();

				world.mainStage.ResetScoreMultiplier();

				world.mainStage.AddUI();

				world.state.changeState("Playing");

			}
		});

		glyphLayout.setText(Assetloader.bitmapfonts.get("NextLevel"), "Game Over!");

		this.addActor(Retry);

		this.addActor(Shop);

		this.addActor(Menu);
	}

	@Override
	public void draw() {

		//draw popup

		GameRenderer.batch.begin();
		GameRenderer.batch.draw(Assetloader.allBGMap.get("next level menu"), PopupX, PopupY, Assetloader.allBGMap.get("next level menu").getWidth() * Assetloader.gameScaleX, Assetloader.allBGMap.get("next level menu").getHeight() * Assetloader.gameScaleY);

		GameRenderer.batch.draw(Assetloader.allSpritesMap.get("BlackStar"), Assetloader.screenWidth / 6.66f, Assetloader.screenHeight / 2.5f, Assetloader.allSpritesMap.get("BlackStar").getWidth() * Assetloader.gameScaleX, Assetloader.allSpritesMap.get("BlackStar").getHeight() * Assetloader.gameScaleY);
		GameRenderer.batch.draw(Assetloader.allSpritesMap.get("BlackStar"), Assetloader.screenWidth / 2.5f, Assetloader.screenHeight / 2.15f, Assetloader.allSpritesMap.get("BlackStar").getWidth() * Assetloader.gameScaleX, Assetloader.allSpritesMap.get("BlackStar").getHeight() * Assetloader.gameScaleY);
		GameRenderer.batch.draw(Assetloader.allSpritesMap.get("BlackStar"), Assetloader.screenWidth / 1.53f, Assetloader.screenHeight / 2.5f, Assetloader.allSpritesMap.get("BlackStar").getWidth() * Assetloader.gameScaleX, Assetloader.allSpritesMap.get("BlackStar").getHeight() * Assetloader.gameScaleY);

		Assetloader.bitmapfonts.get("NextLevel").draw(GameRenderer.batch, glyphLayout, (Assetloader.screenWidth / 2) - (glyphLayout.width / 2), Assetloader.screenHeight / 1.5f);
		GameRenderer.batch.end();


		super.draw();


	}

	@Override
	public void dispose() {

		super.dispose();

	}
}