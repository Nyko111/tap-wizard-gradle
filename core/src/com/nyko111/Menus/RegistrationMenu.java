package com.nyko111.Menus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.nyko111.GameWorld.GameRenderer;
import com.nyko111.GameWorld.GameWorld;
import com.nykoengine.Helpers.Assetloader;
import com.nykoengine.UserInterface.GameButton;
import com.nykoengine.UserInterface.TextButton;

/**
 * Created by MacUser on 11/17/16.
 */
public class RegistrationMenu extends Stage {

    Boolean bRegistered = false;

    TextButton Register;

    TextField userName, passWord, reTypePassword;

    GameButton Close;

    float PopupX, PopupY;

    Color PassWordMatch = Color.WHITE;

    GlyphLayout glyphLayout;

    public Net.HttpResponseListener responseRegistrationListener;

    public RegistrationMenu() {

        responseRegistrationListener = new Net.HttpResponseListener() {
            @Override
            public void handleHttpResponse(Net.HttpResponse httpResponse) {
                Gdx.app.log("Status code ", "" + httpResponse.getStatus().getStatusCode());

                if (httpResponse.getResultAsString().equals("true")) {

                    bRegistered = true;

                    clear();

                    //add buttons
                    addActor(Close);


                }
            }
            @Override
            public void failed(Throwable t) {
                Gdx.app.log("Failed ", t.getMessage());
            }

            @Override
            public void cancelled() {
                Gdx.app.log("Cancelled ", null);
            }
        };

        PopupX = ((Assetloader.screenWidth / 2f) - ((Assetloader.allBGMap.get("RegisterBG").getWidth()) / 2) * Assetloader.gameScaleX);

        PopupY = Assetloader.screenHeight / 5.5f;


        Close = new GameButton(Assetloader.screenWidth / 1.19f, Assetloader.screenHeight / 1.37f, Assetloader.allSpritesMap.get("CloseButton"), null, Assetloader.sounds.get("buttonclick"), false);

        Close.addListener(new InputListener(){
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                Close.Pushed();



                return true;
            }
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                Close.pushed = false;

                GameWorld.state.changeState("Login");

            }
        });

        Register = new TextButton(Assetloader.screenWidth / 1.634f, Assetloader.screenHeight / 4.83f, Assetloader.allSpritesMap.get("NextLevelMenuButton"), null, Assetloader.bitmapfonts.get("ShopUpgradeFont"), "Register", Assetloader.sounds.get("buttonclick"), false);

        Register.addListener(new InputListener(){
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                Register.Pushed();



                return true;
            }
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                Register.pushed = false;

                //Dont forget username checks!

                if (passWord.getText().equals(reTypePassword.getText())) {

                    PassWordMatch = Color.WHITE;

                    GameWorld.httpContent.postMap.clear();

                    GameWorld.httpContent.AddpostMap("username", userName.getText());

                    GameWorld.httpContent.AddpostMap("password", passWord.getText());

                    GameWorld.httpContent.AddpostMap("register", "register");

                    GameWorld.httpConnection.setPostContent(GameWorld.httpContent.postMap);

                    GameWorld.httpConnection.HttpPost(responseRegistrationListener);


                } else {

                    PassWordMatch = Color.RED;

                }

            }
        });

        Drawable cursor = new Drawable() {
            @Override
            public void draw(Batch batch, float x, float y, float width, float height) {

                Assetloader.bitmapfonts.get("ShopUpgradeFont").draw(batch, "|", x, y + (Assetloader.screenHeight / 20f));

            }

            @Override
            public float getLeftWidth() {
                return 0;
            }

            @Override
            public void setLeftWidth(float leftWidth) {

            }

            @Override
            public float getRightWidth() {
                return 0;
            }

            @Override
            public void setRightWidth(float rightWidth) {

            }

            @Override
            public float getTopHeight() {
                return 0;
            }

            @Override
            public void setTopHeight(float topHeight) {

            }

            @Override
            public float getBottomHeight() {
                return 0;
            }

            @Override
            public void setBottomHeight(float bottomHeight) {

            }

            @Override
            public float getMinWidth() {
                return 0;
            }

            @Override
            public void setMinWidth(float minWidth) {

            }

            @Override
            public float getMinHeight() {
                return 0;
            }

            @Override
            public void setMinHeight(float minHeight) {

            }
        };

        TextField.TextFieldStyle textFieldStyle = new TextField.TextFieldStyle(Assetloader.bitmapfonts.get("ShopUpgradeFont"), Color.WHITE, cursor, null, null);

        userName = new TextField("", textFieldStyle);
        userName.setPosition(Assetloader.screenWidth / 5.5f, Assetloader.screenHeight / 1.55f);
        userName.setSize(Assetloader.screenWidth / 1.6f, Assetloader.screenHeight / 12.62f);

        userName.addListener(new InputListener(){

            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {

                Gdx.input.setOnscreenKeyboardVisible(true);

            }
        });

        passWord = new TextField("", textFieldStyle);
        passWord.setPosition(Assetloader.screenWidth / 5.5f, Assetloader.screenHeight / 2.03f);
        passWord.setSize(Assetloader.screenWidth / 1.6f, Assetloader.screenHeight / 12.62f);
        passWord.setPasswordCharacter('*');
        passWord.setPasswordMode(true);

        passWord.addListener(new InputListener(){

            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {

                Gdx.input.setOnscreenKeyboardVisible(true);


            }
        });

        reTypePassword = new TextField("", textFieldStyle);
        reTypePassword.setPosition(Assetloader.screenWidth / 5.5f, Assetloader.screenHeight / 2.95f);
        reTypePassword.setSize(Assetloader.screenWidth / 1.6f, Assetloader.screenHeight / 12.62f);
        reTypePassword.setPasswordCharacter('*');
        reTypePassword.setPasswordMode(true);

        reTypePassword.addListener(new InputListener(){

            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {

                Gdx.input.setOnscreenKeyboardVisible(true);


            }
        });


        this.addActor(userName);
        this.addActor(passWord);
        this.addActor(Register);
        this.addActor(reTypePassword);
        this.addActor(Close);


    }

    @Override
    public void draw() {

        GameRenderer.batch.begin();


        GameRenderer.batch.draw(Assetloader.allBGMap.get("RegisterBG"), PopupX, PopupY, Assetloader.allBGMap.get("RegisterBG").getWidth() * Assetloader.gameScaleX, Assetloader.allBGMap.get("RegisterBG").getHeight() * Assetloader.gameScaleY);


        if (!bRegistered) {

            //Username field
            Assetloader.bitmapfonts.get("ShopUpgradeFont").draw(GameRenderer.batch, "UserName", Assetloader.screenWidth / 3.21f, Assetloader.screenHeight / 1.3f);

            GameRenderer.batch.draw(Assetloader.allSpritesMap.get("TextField"), Assetloader.screenWidth / 6.64f, Assetloader.screenHeight / 1.55f, Assetloader.allSpritesMap.get("TextField").getWidth() * Assetloader.gameScaleX, Assetloader.allSpritesMap.get("TextField").getHeight() * Assetloader.gameScaleY);

            GameRenderer.batch.setColor(PassWordMatch);

            //Password field
            Assetloader.bitmapfonts.get("ShopUpgradeFont").draw(GameRenderer.batch, "Password", Assetloader.screenWidth / 3.17f, Assetloader.screenHeight / 1.62f);

            GameRenderer.batch.draw(Assetloader.allSpritesMap.get("TextField"), Assetloader.screenWidth / 6.64f, Assetloader.screenHeight / 2.03f, Assetloader.allSpritesMap.get("TextField").getWidth() * Assetloader.gameScaleX, Assetloader.allSpritesMap.get("TextField").getHeight() * Assetloader.gameScaleY);


            //reType password field
            Assetloader.bitmapfonts.get("ShopUpgradeFont").draw(GameRenderer.batch, "ReType Password", Assetloader.screenWidth / 5.34f, Assetloader.screenHeight / 2.16f);

            GameRenderer.batch.draw(Assetloader.allSpritesMap.get("TextField"), Assetloader.screenWidth / 6.64f, Assetloader.screenHeight / 2.95f, Assetloader.allSpritesMap.get("TextField").getWidth() * Assetloader.gameScaleX, Assetloader.allSpritesMap.get("TextField").getHeight() * Assetloader.gameScaleY);

            GameRenderer.batch.setColor(Color.WHITE);



        } else {

            Assetloader.bitmapfonts.get("MainWiz").draw(GameRenderer.batch, "You are", Assetloader.screenWidth / 3.6f, Assetloader.screenHeight / 1.62f);

            Assetloader.bitmapfonts.get("MainWiz").draw(GameRenderer.batch, "Registered!", Assetloader.screenWidth / 5f, Assetloader.screenHeight / 2.16f);

        }

        GameRenderer.batch.end();


        super.draw();
    }


}
