package com.nyko111.Menus;

import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.nyko111.GameWorld.GameRenderer;
import com.nyko111.GameWorld.GameWorld;
import com.nykoengine.Helpers.Assetloader;
import com.nykoengine.UserInterface.TextButton;

public class NextLevelMenu extends Stage {
	
	Sprite Star1, Star2, Star3;
	
	TextButton Menu, Shop, Next;
	
	float PopupX, PopupY;
	
	GlyphLayout glyphLayout;

	GameWorld world;
	
	public NextLevelMenu(final GameWorld world) {

		this.world = world;
		
		PopupX = ((Assetloader.screenWidth / 2f) - ((Assetloader.allBGMap.get("LoginBG").getWidth()) / 2) * Assetloader.gameScaleX);
		
		PopupY = Assetloader.screenHeight / 6.8f;
		
		Menu = new TextButton(Assetloader.screenWidth / 22f, Assetloader.screenHeight / 6f, Assetloader.allSpritesMap.get("NextLevelMenuButton"), null, Assetloader.bitmapfonts.get("ShopUpgradeFont"), "Menu", Assetloader.sounds.get("buttonclick"), false);
		
		Menu.addListener(new InputListener(){
			 public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				 	Menu.Pushed();
	   
				 	
	            	
	                return true;
	            }
	            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
	            	Menu.pushed = false;
	            	
	            	if (world.playerInfo.CompletedTutorial != 2)
	            		world.state.changeState("MainMenu");
	            
	            }
	        });
		
		Shop = new TextButton(Assetloader.screenWidth / 2.95f, Assetloader.screenHeight / 3.6f, Assetloader.allSpritesMap.get("NextLevelMenuButton"), null, Assetloader.bitmapfonts.get("ShopUpgradeFont"), "Shop", Assetloader.sounds.get("buttonclick"), false);
		
		Shop.addListener(new InputListener(){
			 public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				 	Shop.Pushed();
	   
				 	
	            	
	                return true;
	            }
	            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
	            	Shop.pushed = false;
	            	
	            	if (world.playerInfo.CompletedTutorial == 2) {
	            		
	            		world.playerInfo.CompletedTutorial += 1;
	            		
	            		world.tutorialHandler.TutorialPages[GameWorld.tutorialHandler.CurrentTutorialPage].ActionComplete();
	            	}
	            	
	            	world.state.changeState("Shop");

					world.shopMenu.previousState = "NextLevel";
	            
	            }
	        });
		
		
		Next = new TextButton(Assetloader.screenWidth / 1.575f, Assetloader.screenHeight / 6f, Assetloader.allSpritesMap.get("NextLevelMenuButton"), null, Assetloader.bitmapfonts.get("ShopUpgradeFont"), "Next", Assetloader.sounds.get("buttonclick"), false);
		
		Next.addListener(new InputListener(){
			 public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				 	Next.Pushed();
	   
				 	
	            	
	                return true;
	            }
	            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
	            	Next.pushed = false;
	            	
	            	if (world.playerInfo.CompletedTutorial != 2) {
	            		if (world.levelSelectMenu.levelArray[world.CurrentLevel + 1] != null) {
	            			world.CurrentLevel += 1;
	            			world.levelSelectMenu.levelArray[world.CurrentLevel].Start(world);
	            			System.out.println("Started level: " + world.CurrentLevel);
	            		}
	            	
	            		world.mainStage.spellArray.clear();
	    				world.mainStage.clear();
	    				world.mainStage.enemyArray.clear();
	    				world.particleEmitterArray.clear();
	    				world.mainStage.Score = 0;
	    				world.mainStage.manaBar.Reset();
	    			
	    				world.mainStage.ResetScoreMultiplier();
	    			
	    				world.mainStage.AddUI();
	            	
	    				world.state.changeState("Playing");
	            
	            	}
	            	}
	            });
		
		glyphLayout = new GlyphLayout(Assetloader.bitmapfonts.get("NextLevel"), "Victory!");
		
		this.addActor(Menu);
		
		this.addActor(Shop);
		
		this.addActor(Next);
	}

	
	public void StarScore(int StarScore) {
		
		switch (StarScore) {
		
		case 1: 
			
		Star1 = Assetloader.allSpritesMap.get("GoldStar");
		Star2 = Assetloader.allSpritesMap.get("BlackStar");
		Star3 = Assetloader.allSpritesMap.get("BlackStar");
		
		break;
		
		case 2:
			
		Star1 = Assetloader.allSpritesMap.get("GoldStar");
		Star2 = Assetloader.allSpritesMap.get("GoldStar");
		Star3 = Assetloader.allSpritesMap.get("BlackStar");
			
		break;
		
		case 3: 
			
		Star1 = Assetloader.allSpritesMap.get("GoldStar");
		Star2 = Assetloader.allSpritesMap.get("GoldStar");
		Star3 = Assetloader.allSpritesMap.get("GoldStar");
			
		break;
		
		default:
			System.out.println("Error in starscore switch");
		break;
		
		
		};
			
		
		
	}
	
	@Override
	public void draw() {
		GameRenderer.batch.begin();
		//draw popup

		GameRenderer.batch.draw(Assetloader.allBGMap.get("LoginBG"), PopupX, PopupY, Assetloader.allBGMap.get("LoginBG").getWidth() * Assetloader.gameScaleX, Assetloader.allBGMap.get("LoginBG").getHeight() * Assetloader.gameScaleY);
		
		GameRenderer.batch.draw(Star1, Assetloader.screenWidth / 6.66f, Assetloader.screenHeight / 2.5f, Star1.getWidth() * Assetloader.gameScaleX, Star1.getHeight() * Assetloader.gameScaleY);
		GameRenderer.batch.draw(Star2, Assetloader.screenWidth / 2.5f, Assetloader.screenHeight / 2.15f, Star2.getWidth() * Assetloader.gameScaleX, Star2.getHeight() * Assetloader.gameScaleY);
		GameRenderer.batch.draw(Star3, Assetloader.screenWidth / 1.53f, Assetloader.screenHeight / 2.5f, Star3.getWidth() * Assetloader.gameScaleX, Star3.getHeight() * Assetloader.gameScaleY);

		Assetloader.bitmapfonts.get("NextLevel").draw(GameRenderer.batch, glyphLayout, (Assetloader.screenWidth / 2) - (glyphLayout.width / 2), Assetloader.screenHeight / 1.5f);
		
		GameRenderer.batch.end();
		super.draw();
		
		
		
	}

	@Override
	public void dispose() {

		super.dispose();

	}
}
