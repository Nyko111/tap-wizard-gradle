package com.nyko111.Menus;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.nyko111.GameWorld.GameRenderer;
import com.nyko111.GameWorld.GameWorld;
import com.nyko111.UserInterfaces.LevelButton;
import com.nykoengine.Helpers.Assetloader;
import com.nykoengine.UserInterface.GameButton;
import com.nykoengine.UserInterface.TextButton;

public class PageController { 
	
	public enum PageType { TextButton, String, ShopButton };
		
	public class Page {
		
			public float TextButtonX, TextButtonY, StringX, StringY, GameButtonX, GameButtonY, SpriteX, SpriteY;
			
			public Vector2[] spriteXY;
			public Sprite[] spriteArray;
			
			public LevelButton[] LevelButtons;
			
			public Vector2[] stringXY;
			
			public String[] stringArray;
			
			public GameButton[] GameButtonArray;
		
			public Page(float TextButtonX, float TextButtonY, float StringX, float StringY, float GameButtonX, float GameButtonY, float SpriteX, float SpriteY) {
				
				LevelButtons = new LevelButton[16];
				
				
				stringXY = new Vector2[10];
				stringArray = new String[10];
				
				GameButtonArray = new GameButton[16];
				
				spriteXY = new Vector2[10];
				spriteArray = new Sprite[10];
			
				this.TextButtonX = TextButtonX;
				
				this.TextButtonY = TextButtonY;
				
				this.StringX = StringX;
				
				this.StringY = StringY;
				
				this.GameButtonX = TextButtonX;
				
				this.GameButtonY = TextButtonY;
				
				this.SpriteX = SpriteX;
				
				this.SpriteY = SpriteY;
				
				
			}
		
		}
	
	public List<Page> pageArray;
	
	public int TextButtonIter = 0;
	
	public int PageArraySelected = 0;
	
	public int PageArrayIter = 0;
	
	public Boolean StrArrayFull = false, SpriteArrayFull = false, TextButtonArrayFull = false;
	
	public int SpriteArrayIter = 0;
	
	public int StrArrayIter = 0;
	
	public float TextButtonXPadding, TextButtonYPadding, StringXPadding, StringYPadding, GameButtonXPadding, GameButtonYPadding, SpriteXPadding, SpriteYPadding;
		
	public PageController(float TextButtonX, float TextButtonY, float StringX, float StringY, float GameButtonX, float GameButtonY, float SpriteX, float SpriteY) {
		
		pageArray = new ArrayList<Page>();
			
		pageArray.add(new Page(TextButtonX, TextButtonY, StringX, StringY, GameButtonX, GameButtonY, SpriteX, SpriteY));
		
	}
	
	
	public void AddPage(float TextButtonX, float TextButtonY, float StringX, float StringY, float GameButtonX, float GameButtonY, float SpriteX, float SpriteY) {
		
		this.pageArray.add(new Page(TextButtonX, TextButtonY, StringX, StringY, GameButtonX, GameButtonY, SpriteX, SpriteY));
		
		SpriteArrayIter = 0;
		
		StrArrayIter = 0;
		
	}
	
	public void AddString(String str) {
		
		if (StrArrayIter <= 8) {
			StrArrayFull = false;
		}
		
		
		if (!StrArrayFull) {
			pageArray.get(PageArrayIter).stringArray[StrArrayIter] = str;
			//position
			pageArray.get(PageArrayIter).stringXY[StrArrayIter] = new Vector2(pageArray.get(PageArrayIter).StringX, pageArray.get(PageArrayIter).StringY);
			pageArray.get(PageArrayIter).StringY -= StringYPadding;
		
			StrArrayIter += 1;
		}
		
		if (StrArrayIter >= 9) {
			
			StrArrayFull = true;
			
		} 
		
	}
		

	public void DrawString() {
		
		if (pageArray.get(PageArraySelected).stringArray[0] != null) {
			
			for(int i = 0; i < pageArray.get(PageArraySelected).stringArray.length; i++) {
				
				if (pageArray.get(PageArraySelected).stringArray[i] != null) {
					
					GameRenderer.batch.begin();
					Assetloader.bitmapfonts.get("MainWiz").draw(GameRenderer.batch, pageArray.get(PageArraySelected).stringArray[i], pageArray.get(PageArraySelected).stringXY[i].x, pageArray.get(PageArraySelected).stringXY[i].y);
					GameRenderer.batch.end();
					
				} else {
				
					i = pageArray.get(PageArraySelected).stringArray.length;
			
				}
				
			}
			
		}
		
	}
	
	public void AddSprite(Sprite sprite) {
		
		if (SpriteArrayIter <= 8) {
			SpriteArrayFull = false;
		}
		
		
		if (!SpriteArrayFull) {
			pageArray.get(PageArrayIter).spriteArray[SpriteArrayIter] = sprite;
			//position
			pageArray.get(PageArrayIter).spriteXY[SpriteArrayIter] = new Vector2(pageArray.get(PageArrayIter).SpriteX, pageArray.get(PageArrayIter).SpriteY);
			
			pageArray.get(PageArrayIter).SpriteY -= SpriteYPadding;
		
			SpriteArrayIter += 1;
		}
		
		if (SpriteArrayIter >= 9) {
			
			SpriteArrayFull = true;
			
		} 
		
	}
	
	public void DrawSprite() {
		
		if (pageArray.get(PageArraySelected).spriteArray[0] != null) {
			
			for(int i = 0; i < pageArray.get(PageArraySelected).spriteArray.length; i++) {
				
				if (pageArray.get(PageArraySelected).spriteArray[i] != null) {
					
					GameRenderer.batch.begin();
					GameRenderer.batch.draw(pageArray.get(PageArraySelected).spriteArray[i], pageArray.get(PageArraySelected).spriteXY[i].x, pageArray.get(PageArraySelected).spriteXY[i].y, pageArray.get(PageArraySelected).spriteArray[i].getWidth() * Assetloader.gameScaleX, pageArray.get(PageArraySelected).spriteArray[i].getHeight() * Assetloader.gameScaleY);
					GameRenderer.batch.end();
					
				} else {
				
					i = pageArray.get(PageArraySelected).spriteArray.length;
			
				}
				
			}
			
		}
		
	}
	
	public void AddTextButton(LevelButton button) {
				
		if (SpriteArrayIter <= 14) {
			TextButtonArrayFull = false;
		}
		
		
		if (!SpriteArrayFull) {
		pageArray.get(PageArrayIter).LevelButtons[TextButtonIter] = button;
		//position
		//pageArray.get(PageArrayIter).stringXY[TextButtonIter] = new Vector2(pageArray.get(PageArrayIter).TextButtonX, pageArray.get(PageArrayIter).TextButtonY);
		
		
		
		pageArray.get(PageArrayIter).TextButtonY -= TextButtonYPadding;
		
		TextButtonIter += 1;
		}
		
		if (SpriteArrayIter >= 15) {
			
			TextButtonArrayFull = true;
			
		} 
	
		
	}
	

}
