package com.nyko111.Menus;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.nyko111.GameWorld.GameRenderer;
import com.nyko111.GameWorld.GameWorld;
import com.nykoengine.Helpers.Assetloader;
import com.nykoengine.UserInterface.GameButton;
import com.nykoengine.UserInterface.TextButton;

public class StarballsIAB extends Stage {

	TextButton Login;
	
	GameButton Return;
	
	GameButton Starballs1000;
	
	GameButton Starballs5000;
	
	GameButton Starballs10000;
	
	GameButton Starballs25000;
	
	GameButton Starballs50000;
	
	public StarballsIAB() {
		
		Return = new TextButton(Assetloader.screenWidth / 30f, 0, Assetloader.allSpritesMap.get("NextLevelMenuButton"), null, Assetloader.bitmapfonts.get("ShopUpgradeFont"), "Return", Assetloader.sounds.get("buttonclick"), false);
		
		Return.addListener(new InputListener(){
			 public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				 	Return.Pushed();
	   
				 	
	            	
	                return true;
	            }
	            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
	            	Return.pushed = false;
	            	
	            	GameWorld.state.changeState("Shop");
	            
	            }
	        });

		Login = new TextButton(Assetloader.screenWidth / 1.52f, 0, Assetloader.allSpritesMap.get("NextLevelMenuButton"), null, Assetloader.bitmapfonts.get("ShopUpgradeFont"), "Login", Assetloader.sounds.get("buttonclick"), false);

		Login.addListener(new InputListener(){
			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				Login.Pushed();



				return true;
			}
			public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
				Login.pushed = false;

				GameWorld.previousState = "IAB";

				GameWorld.state.changeState("Login");

			}
		});
		
		
		//Buy buttons
		Starballs1000 = new GameButton(Assetloader.screenWidth / 2 - (Assetloader.allSpritesMap.get("BuyButton").getWidth() * Assetloader.gameScaleX) / 2, Assetloader.screenHeight / 1.35f, Assetloader.allSpritesMap.get("BuyButton"), null, Assetloader.sounds.get("buttonclick"), false);
		
		Starballs1000.addListener(new InputListener(){
			 public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				 	Starballs1000.Pushed();
	   
				 	
	            	
	                return true;
	            }
	            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
	            	Starballs1000.pushed = false;
	            
	      
	            	GameWorld.platformInterface.buyStuff(0);
	            		
	            	//GameWorld.playerInfo.Money -= IcePrice;
	            	//GameWorld.playerInfo.IceLevel += 1;
	            	//Do stuff here for IAB
	            	
	            	
	            }
	        });
		
		Starballs5000 = new GameButton(Assetloader.screenWidth / 2 - (Assetloader.allSpritesMap.get("BuyButton").getWidth() * Assetloader.gameScaleX) / 2, Assetloader.screenHeight / 1.7f, Assetloader.allSpritesMap.get("BuyButton"), null, Assetloader.sounds.get("buttonclick"), false);
		
		Starballs5000.addListener(new InputListener(){
			 public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				 	Starballs5000.Pushed();
	   
				 	
	            	
	                return true;
	            }
	            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
	            	Starballs5000.pushed = false;
	            	
	            	//Do stuff here for IAB
					GameWorld.platformInterface.buyStuff(1);
	            
	            }
	        });
		
		Starballs10000 = new GameButton(Assetloader.screenWidth / 2 - (Assetloader.allSpritesMap.get("BuyButton").getWidth() * Assetloader.gameScaleX) / 2, Assetloader.screenHeight / 2.295f, Assetloader.allSpritesMap.get("BuyButton"), null, Assetloader.sounds.get("buttonclick"), false);
		
		Starballs10000.addListener(new InputListener(){
			 public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				 	Starballs10000.Pushed();
	   
				 	
	            	
	                return true;
	            }
	            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
	            	Starballs10000.pushed = false;
	            	
	            	//Do stuff here for IAB
					GameWorld.platformInterface.buyStuff(2);
	            
	            }
	        });
		
		Starballs25000 = new GameButton(Assetloader.screenWidth / 2 - (Assetloader.allSpritesMap.get("BuyButton").getWidth() * Assetloader.gameScaleX) / 2, Assetloader.screenHeight / 3.53f, Assetloader.allSpritesMap.get("BuyButton"), null, Assetloader.sounds.get("buttonclick"), false);
		
		Starballs25000.addListener(new InputListener(){
			 public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				 	Starballs25000.Pushed();
	   
				 	
	            	
	                return true;
	            }
	            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
	            	Starballs25000.pushed = false;
	            	
	            	//Do stuff here for IAB
					GameWorld.platformInterface.buyStuff(3);
	            
	            }
	        });
		
		Starballs50000 = new GameButton(Assetloader.screenWidth / 2 - (Assetloader.allSpritesMap.get("BuyButton").getWidth() * Assetloader.gameScaleX) / 2, Assetloader.screenHeight / 7.6f, Assetloader.allSpritesMap.get("BuyButton"), null, Assetloader.sounds.get("buttonclick"), false);
		
		Starballs50000.addListener(new InputListener(){
			 public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				 	Starballs50000.Pushed();
	   
				 	
	            	
	                return true;
	            }
	            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
	            	Starballs50000.pushed = false;
	            	
	            	//Do stuff here for IAB
					GameWorld.platformInterface.buyStuff(4);
	            
	            }
	        });
		
		
			PopulateMenu();
		
		
	}
	
public void PopulateMenu() {
		
		this.clear();
		
		this.addActor(Return);

		this.addActor(Login);
		
		this.addActor(Starballs1000);
		
		this.addActor(Starballs5000);
		
		this.addActor(Starballs10000);
		
		this.addActor(Starballs25000);
		
		this.addActor(Starballs50000);
	
		
	}

	@Override
	public void draw() {
		
GameRenderer.batch.begin();
		
		GameRenderer.batch.draw(Assetloader.allBGMap.get("ShopMenuBG"), Assetloader.screenWidth / 22.85f, Assetloader.screenHeight / 10f, Assetloader.allBGMap.get("ShopMenuBG").getWidth() * Assetloader.gameScaleX, Assetloader.allBGMap.get("ShopMenuBG").getHeight() * Assetloader.gameScaleY);
		
		//GameWorld.Main.draw(GameRenderer.batch, "SHOP", Assetloader.screenWidth / 2 - GameWorld.Main.getBounds("SHOP").width / 2, Assetloader.screenHeight / 1.05f);
		
		Assetloader.bitmapfonts.get("MainWiz").draw(GameRenderer.batch, "Buy StarBalls", Assetloader.screenWidth / 12f, Assetloader.screenHeight / 1.05f);
		
		GameRenderer.batch.end();
		
		super.draw();
		
		GameRenderer.batch.begin();
		
		//100 starball
		GameRenderer.batch.draw(Assetloader.allSpritesMap.get("StarBall"), Assetloader.screenWidth / 2.8f, Assetloader.screenHeight / 1.23f, Assetloader.allSpritesMap.get("StarBall").getWidth() * Assetloader.gameScaleX, Assetloader.allSpritesMap.get("StarBall").getHeight() * Assetloader.gameScaleY);
		Assetloader.bitmapfonts.get("ShopUpgradeFont").draw(GameRenderer.batch, "1000", Assetloader.screenWidth / 2.2f, Assetloader.screenHeight / 1.17f);
		Assetloader.bitmapfonts.get("ShopUpgradeFont").draw(GameRenderer.batch, "$0.99", Assetloader.screenWidth / 2.5f, Assetloader.screenHeight / 1.255f);
		
		//500 starball
		GameRenderer.batch.draw(Assetloader.allSpritesMap.get("StarBall"), Assetloader.screenWidth / 2.8f, Assetloader.screenHeight / 1.515f, Assetloader.allSpritesMap.get("StarBall").getWidth() * Assetloader.gameScaleX, Assetloader.allSpritesMap.get("StarBall").getHeight() * Assetloader.gameScaleY);
		Assetloader.bitmapfonts.get("ShopUpgradeFont").draw(GameRenderer.batch, "5000", Assetloader.screenWidth / 2.2f, Assetloader.screenHeight / 1.43f);
		Assetloader.bitmapfonts.get("ShopUpgradeFont").draw(GameRenderer.batch, "$4.99", Assetloader.screenWidth / 2.5f, Assetloader.screenHeight / 1.55f);
		
		//1000 starball
		GameRenderer.batch.draw(Assetloader.allSpritesMap.get("StarBall"), Assetloader.screenWidth / 2.8f, Assetloader.screenHeight / 1.965f, Assetloader.allSpritesMap.get("StarBall").getWidth() * Assetloader.gameScaleX, Assetloader.allSpritesMap.get("StarBall").getHeight() * Assetloader.gameScaleY);
		Assetloader.bitmapfonts.get("ShopUpgradeFont").draw(GameRenderer.batch, "10000", Assetloader.screenWidth / 2.2f, Assetloader.screenHeight / 1.815f);
		Assetloader.bitmapfonts.get("ShopUpgradeFont").draw(GameRenderer.batch, "$8.99", Assetloader.screenWidth / 2.5f, Assetloader.screenHeight / 2.03f);
		
		
		//2500 starball
		GameRenderer.batch.draw(Assetloader.allSpritesMap.get("StarBall"), Assetloader.screenWidth / 2.8f, Assetloader.screenHeight / 2.8f, Assetloader.allSpritesMap.get("StarBall").getWidth() * Assetloader.gameScaleX, Assetloader.allSpritesMap.get("StarBall").getHeight() * Assetloader.gameScaleY);
		Assetloader.bitmapfonts.get("ShopUpgradeFont").draw(GameRenderer.batch, "25000", Assetloader.screenWidth / 2.2f, Assetloader.screenHeight / 2.5f);
		Assetloader.bitmapfonts.get("ShopUpgradeFont").draw(GameRenderer.batch, "$14.99", Assetloader.screenWidth / 2.5f, Assetloader.screenHeight / 2.94f);
		
		//50000 starball
		GameRenderer.batch.draw(Assetloader.allSpritesMap.get("StarBall"), Assetloader.screenWidth / 2.8f, Assetloader.screenHeight / 4.88f, Assetloader.allSpritesMap.get("StarBall").getWidth() * Assetloader.gameScaleX, Assetloader.allSpritesMap.get("StarBall").getHeight() * Assetloader.gameScaleY);
		Assetloader.bitmapfonts.get("ShopUpgradeFont").draw(GameRenderer.batch, "50000", Assetloader.screenWidth / 2.2f, Assetloader.screenHeight / 4.05f);
		Assetloader.bitmapfonts.get("ShopUpgradeFont").draw(GameRenderer.batch, "$19.99", Assetloader.screenWidth / 2.5f, Assetloader.screenHeight / 5.4f);
		
		GameRenderer.batch.end();
		
		
	}
}
