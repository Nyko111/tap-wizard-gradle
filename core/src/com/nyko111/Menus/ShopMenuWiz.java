package com.nyko111.Menus;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.nyko111.GameWorld.GameRenderer;
import com.nyko111.GameWorld.GameWorld;
import com.nykoengine.Helpers.Assetloader;
import com.nykoengine.ParticleEngine.Emitter;
import com.nykoengine.UserInterface.GameButton;
import com.nykoengine.UserInterface.TextButton;

public class ShopMenuWiz extends Stage {
	
	GameButton Return, BuyStarBalls;
	
	GameButton IceBuy, FireBuy, EarthBuy, WindBuy, StarBallUpgradeBuy;
	
	String sMoney, previousState;
	
	Integer IcePrice, FirePrice, EarthPrice, WindPrice, StarBallUpgradePrice;
	
	BitmapFont  PriceFont;

	GameWorld world;
	
	//PageController pageController = new PageController(Assetloader.screenWidth / 1.55f, Assetloader.screenHeight / 1.27f, Assetloader.screenWidth / 3f, Assetloader.screenHeight / 1.18f, Assetloader.screenWidth / 15.686f, Assetloader.screenHeight / 1.219f, Assetloader.screenWidth / 8.421f, Assetloader.screenHeight / 1.32f);

	public ShopMenuWiz(final GameWorld world) {

		this.world = world;

		//PriceFont = Assetloader.CreateFont(Assetloader.screenWidth / 14f, Assetloader.fonts.get("TapWizFont"), Color.WHITE);
		
		Return = new TextButton(Assetloader.screenWidth / 30f, 0, Assetloader.allSpritesMap.get("NextLevelMenuButton"), null, Assetloader.bitmapfonts.get("ShopUpgradeFont"), "Return", Assetloader.sounds.get("buttonclick"), false);
		
		Return.addListener(new InputListener(){
			 public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				 	Return.Pushed();
	   
				 	
	            	
	                return true;
	            }
	            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
	            	Return.pushed = false;
	            	
	            	GameWorld.state.changeState(previousState);
	            
	            }
	        });
		
		BuyStarBalls = new GameButton(Assetloader.screenWidth / 1.52f, 0, Assetloader.allSpritesMap.get("NextLevelMenuButton"), null, Assetloader.sounds.get("buttonclick"), false);
		
		BuyStarBalls.addListener(new InputListener(){
			 public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				 	BuyStarBalls.Pushed();
	   
				 	
	            	
	                return true;
	            }
	            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
	            	BuyStarBalls.pushed = false;
	            	
	            	GameWorld.state.changeState("IAB");
	            
	            }
	        });
		
		//All the purchase buttons
		
		IceBuy = new GameButton(Assetloader.screenWidth / 2 - (Assetloader.allSpritesMap.get("BuyButton").getWidth() * Assetloader.gameScaleX) / 2, Assetloader.screenHeight / 1.35f, Assetloader.allSpritesMap.get("BuyButton"), null, Assetloader.sounds.get("buttonclick"), false);
		
		IceBuy.addListener(new InputListener(){
			 public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				 	IceBuy.Pushed();
	   
				 	
	            	
	                return true;
	            }
	            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
	            	IceBuy.pushed = false;
	            
	            	if (world.playerInfo.Money >= IcePrice) {
	            		
	            		world.playerInfo.Money -= IcePrice;
	            		world.playerInfo.IceLevel += 1;
	            		
	            		//Recalculate prices
	            		CalculatePrices();
	            		
	            		//Adds emitter
	            		addEmitter(new Vector2(IceBuy.getMiddlePos().x - Assetloader.screenWidth / 12f, IceBuy.getMiddlePos().y - Assetloader.screenWidth / 12f));	            		
	            	
	            	}
	            	
	            }
	        });
		
		FireBuy = new GameButton(Assetloader.screenWidth / 2 - (Assetloader.allSpritesMap.get("BuyButton").getWidth() * Assetloader.gameScaleX) / 2, Assetloader.screenHeight / 1.7f, Assetloader.allSpritesMap.get("BuyButton"), null, Assetloader.sounds.get("buttonclick"), false);
		
		FireBuy.addListener(new InputListener(){
			 public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				 	FireBuy.Pushed();
	   
				 	
	            	
	                return true;
	            }
	            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
	            	FireBuy.pushed = false;
	            	
	            	if (world.playerInfo.Money >= FirePrice) {
	            		
	            		world.playerInfo.Money -= FirePrice;
	            		world.playerInfo.FireLevel += 1;
	            		
	            		//Recalculate prices
	            		CalculatePrices();
	            		
	            		//Adds emitter
	            		addEmitter(new Vector2(FireBuy.getMiddlePos().x - Assetloader.screenWidth / 12f, FireBuy.getMiddlePos().y - Assetloader.screenWidth / 12f));

	            		
	            	}
	            
	            
	            }
	        });
		
		EarthBuy = new GameButton(Assetloader.screenWidth / 2 - (Assetloader.allSpritesMap.get("BuyButton").getWidth() * Assetloader.gameScaleX) / 2, Assetloader.screenHeight / 2.295f, Assetloader.allSpritesMap.get("BuyButton"), null, Assetloader.sounds.get("buttonclick"), false);
		
		EarthBuy.addListener(new InputListener(){
			 public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				 	EarthBuy.Pushed();
	   
				 	
	            	
	                return true;
	            }
	            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
	            	EarthBuy.pushed = false;
	            	
	            	if (world.playerInfo.Money >= EarthPrice) {
	            		
	            		world.playerInfo.Money -= EarthPrice;
	            		world.playerInfo.EarthLevel += 1;
	            		
	            		//Recalculate prices
	            		CalculatePrices();
	            		
	            		//Adds emitter
	            		addEmitter(new Vector2(EarthBuy.getMiddlePos().x - Assetloader.screenWidth / 12f, EarthBuy.getMiddlePos().y - Assetloader.screenWidth / 12f));
	            		
	            	}
	            
	            
	            }
	        });
		
		WindBuy = new GameButton(Assetloader.screenWidth / 2 - (Assetloader.allSpritesMap.get("BuyButton").getWidth() * Assetloader.gameScaleX) / 2, Assetloader.screenHeight / 3.53f, Assetloader.allSpritesMap.get("BuyButton"), null, Assetloader.sounds.get("buttonclick"), false);
		
		WindBuy.addListener(new InputListener(){
			 public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				 	WindBuy.Pushed();
	   
				 	
	            	
	                return true;
	            }
	            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
	            	WindBuy.pushed = false;
	            	
	            	if (world.playerInfo.Money >= WindPrice) {
	            		
	            		world.playerInfo.Money -= WindPrice;
	            		world.playerInfo.WindLevel += 1;
	            		
	            		//Recalculate prices
	            		CalculatePrices();
	            		
	            		//Adds emitter
	            		addEmitter(new Vector2(WindBuy.getMiddlePos().x - Assetloader.screenWidth / 12f, WindBuy.getMiddlePos().y - Assetloader.screenWidth / 12f));
	            	}
	            
	            
	            }
	        });
		
		StarBallUpgradeBuy = new GameButton(Assetloader.screenWidth / 2 - (Assetloader.allSpritesMap.get("BuyButton").getWidth() * Assetloader.gameScaleX) / 2, Assetloader.screenHeight / 7.6f, Assetloader.allSpritesMap.get("BuyButton"), null, Assetloader.sounds.get("buttonclick"), false);
		
		StarBallUpgradeBuy.addListener(new InputListener(){
			 public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				 StarBallUpgradeBuy.Pushed();
	   
				 	
	            	
	                return true;
	            }
	            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
	            	StarBallUpgradeBuy.pushed = false;
	            	
	            	if (world.playerInfo.Money >= StarBallUpgradePrice) {
	            		
	            		world.playerInfo.Money -= StarBallUpgradePrice;
	            		world.playerInfo.StarBallDropLevel += 1;
	            		//GameWorld.mainStage.wall.ReinforcedLevel += 1;
	            		
	            		//Recalculate prices
	            		CalculatePrices();

	            		//Adds emitter
	            		addEmitter(new Vector2(StarBallUpgradeBuy.getMiddlePos().x - Assetloader.screenWidth / 12f, StarBallUpgradeBuy.getMiddlePos().y - Assetloader.screenWidth / 12f));
	            		
	            	}
	            
	            
	            }
	        });
		
		//Shield buy button
		
		
		//End all purchase buttons
		
		
		PopulateMenu();
		
		CalculatePrices();
		
		
	}
	
	public void PopulateMenu() {
		
		this.clear();
		
		this.addActor(Return);
		
		this.addActor(BuyStarBalls);
		
		this.addActor(IceBuy);
		
		this.addActor(FireBuy);
		
		this.addActor(EarthBuy);
		
		this.addActor(WindBuy);
		
		this.addActor(StarBallUpgradeBuy);
	
		
	}
	
	public void CalculatePrices() {
		
		IcePrice = 20 * world.playerInfo.IceLevel;
		FirePrice = 20 * world.playerInfo.FireLevel;
		EarthPrice = 20 * world.playerInfo.EarthLevel;
		WindPrice = 20 * world.playerInfo.WindLevel;
		StarBallUpgradePrice = 40 * world.playerInfo.StarBallDropLevel;
		
	}
	
	@Override
	public void draw() {
		
		sMoney = Integer.toString(world.playerInfo.Money);
		
		GameRenderer.batch.begin();
		
		GameRenderer.batch.draw(Assetloader.allBGMap.get("ShopMenuBG"), Assetloader.screenWidth / 22.85f, Assetloader.screenHeight / 10f, Assetloader.allBGMap.get("ShopMenuBG").getWidth() * Assetloader.gameScaleX, Assetloader.allBGMap.get("ShopMenuBG").getHeight() * Assetloader.gameScaleY);
		
		//GameWorld.Main.draw(GameRenderer.batch, "SHOP", Assetloader.screenWidth / 2 - GameWorld.Main.getBounds("SHOP").width / 2, Assetloader.screenHeight / 1.05f);
		
		Assetloader.bitmapfonts.get("MainWiz").draw(GameRenderer.batch, sMoney, Assetloader.screenWidth / 2.1f, Assetloader.screenHeight / 1.05f);
		
		GameRenderer.batch.draw(Assetloader.allSpritesMap.get("StarBall"), Assetloader.screenWidth / 2.6f, Assetloader.screenHeight / 1.115f, Assetloader.allSpritesMap.get("StarBall").getWidth() * Assetloader.gameScaleX, Assetloader.allSpritesMap.get("StarBall").getHeight() * Assetloader.gameScaleY);
		
		GameRenderer.batch.end();
		
		
		//Check if there are strings in page string array
		
		
		super.draw();
		
		GameRenderer.batch.begin();
		
		GameRenderer.batch.draw(Assetloader.allSpritesMap.get("ShopIconIce"), Assetloader.screenWidth / 1.58f, Assetloader.screenHeight / 1.3f, Assetloader.allSpritesMap.get("ShopIconIce").getWidth() * Assetloader.gameScaleX, Assetloader.allSpritesMap.get("ShopIconIce").getHeight() * Assetloader.gameScaleY);
	
		//Ice fonts
			Assetloader.bitmapfonts.get("ShopLevelFont").draw(GameRenderer.batch, "LVL " + world.playerInfo.IceLevel.toString(), Assetloader.screenWidth / 1.6f, Assetloader.screenHeight / 1.29f);
		    Assetloader.bitmapfonts.get("ShopUpgradeFont").draw(GameRenderer.batch, "UPGRADE", Assetloader.screenWidth / 3.6f, Assetloader.screenHeight / 1.16f);
			Assetloader.bitmapfonts.get("ShopUpgradeFont").draw(GameRenderer.batch, IcePrice.toString(), Assetloader.screenWidth / 2.35f, Assetloader.screenHeight / 1.259f);
			GameRenderer.batch.draw(Assetloader.allSpritesMap.get("StarBall"), Assetloader.screenWidth / 3f, Assetloader.screenHeight / 1.325f, Assetloader.allSpritesMap.get("StarBall").getWidth() * Assetloader.gameScaleX, Assetloader.allSpritesMap.get("StarBall").getHeight() * Assetloader.gameScaleY);
			
		GameRenderer.batch.draw(Assetloader.allSpritesMap.get("ShopIconFire"), Assetloader.screenWidth / 1.58f, Assetloader.screenHeight / 1.61f, Assetloader.allSpritesMap.get("ShopIconFire").getWidth() * Assetloader.gameScaleX, Assetloader.allSpritesMap.get("ShopIconFire").getHeight() * Assetloader.gameScaleY);
		
		//Fire level font
			Assetloader.bitmapfonts.get("ShopLevelFont").draw(GameRenderer.batch, "LVL " + world.playerInfo.FireLevel.toString(), Assetloader.screenWidth / 1.6f, Assetloader.screenHeight / 1.61f);
			Assetloader.bitmapfonts.get("ShopUpgradeFont").draw(GameRenderer.batch, "UPGRADE", Assetloader.screenWidth / 3.6f, Assetloader.screenHeight / 1.41f);
			Assetloader.bitmapfonts.get("ShopUpgradeFont").draw(GameRenderer.batch, FirePrice.toString(), Assetloader.screenWidth / 2.35f, Assetloader.screenHeight / 1.56f);
			GameRenderer.batch.draw(Assetloader.allSpritesMap.get("StarBall"), Assetloader.screenWidth / 3f, Assetloader.screenHeight / 1.665f, Assetloader.allSpritesMap.get("StarBall").getWidth() * Assetloader.gameScaleX, Assetloader.allSpritesMap.get("StarBall").getHeight() * Assetloader.gameScaleY);
			
			
		GameRenderer.batch.draw(Assetloader.allSpritesMap.get("ShopIconEarth"), Assetloader.screenWidth / 1.58f, Assetloader.screenHeight / 2.13f, Assetloader.allSpritesMap.get("ShopIconEarth").getWidth() * Assetloader.gameScaleX, Assetloader.allSpritesMap.get("ShopIconEarth").getHeight() * Assetloader.gameScaleY);
		
		//Earth level font
			Assetloader.bitmapfonts.get("ShopLevelFont").draw(GameRenderer.batch, "LVL " + world.playerInfo.EarthLevel.toString(), Assetloader.screenWidth / 1.6f, Assetloader.screenHeight / 2.13f);
			Assetloader.bitmapfonts.get("ShopUpgradeFont").draw(GameRenderer.batch, "UPGRADE", Assetloader.screenWidth / 3.6f, Assetloader.screenHeight / 1.795f);
			Assetloader.bitmapfonts.get("ShopUpgradeFont").draw(GameRenderer.batch, EarthPrice.toString(), Assetloader.screenWidth / 2.35f, Assetloader.screenHeight / 2.05f);
			GameRenderer.batch.draw(Assetloader.allSpritesMap.get("StarBall"), Assetloader.screenWidth / 3f, Assetloader.screenHeight / 2.23f, Assetloader.allSpritesMap.get("StarBall").getWidth() * Assetloader.gameScaleX, Assetloader.allSpritesMap.get("StarBall").getHeight() * Assetloader.gameScaleY);
			
		GameRenderer.batch.draw(Assetloader.allSpritesMap.get("ShopIconWind"), Assetloader.screenWidth / 1.58f, Assetloader.screenHeight / 3.125f, Assetloader.allSpritesMap.get("ShopIconWind").getWidth() * Assetloader.gameScaleX, Assetloader.allSpritesMap.get("ShopIconWind").getHeight() * Assetloader.gameScaleY);
		
		//Wind level font
			Assetloader.bitmapfonts.get("ShopLevelFont").draw(GameRenderer.batch, "LVL " + world.playerInfo.WindLevel.toString(), Assetloader.screenWidth / 1.6f, Assetloader.screenHeight / 3.15f);
			Assetloader.bitmapfonts.get("ShopUpgradeFont").draw(GameRenderer.batch, "UPGRADE", Assetloader.screenWidth / 3.6f, Assetloader.screenHeight / 2.47f);
			Assetloader.bitmapfonts.get("ShopUpgradeFont").draw(GameRenderer.batch, WindPrice.toString(), Assetloader.screenWidth / 2.35f, Assetloader.screenHeight / 2.995f);
			GameRenderer.batch.draw(Assetloader.allSpritesMap.get("StarBall"), Assetloader.screenWidth / 3f, Assetloader.screenHeight / 3.4f, Assetloader.allSpritesMap.get("StarBall").getWidth() * Assetloader.gameScaleX, Assetloader.allSpritesMap.get("StarBall").getHeight() * Assetloader.gameScaleY);
			
			
		GameRenderer.batch.draw(Assetloader.allSpritesMap.get("StarBall"), Assetloader.screenWidth / 1.75f, Assetloader.screenHeight / 4.9f, (Assetloader.allSpritesMap.get("StarBall").getWidth() * Assetloader.gameScaleX) / 1.3f, (Assetloader.allSpritesMap.get("StarBall").getHeight() * Assetloader.gameScaleY) / 1.3f);
		
		//Wall level font

			Assetloader.bitmapfonts.get("ShopLevelFont").draw(GameRenderer.batch, "Drop", Assetloader.screenWidth / 1.55f, Assetloader.screenHeight / 4.25f);

			Assetloader.bitmapfonts.get("ShopLevelFont").draw(GameRenderer.batch, "LVL " + world.playerInfo.StarBallDropLevel.toString(), Assetloader.screenWidth / 1.6f, Assetloader.screenHeight / 6f);

			Assetloader.bitmapfonts.get("ShopUpgradeFont").draw(GameRenderer.batch, "UPGRADE", Assetloader.screenWidth / 4.2f, Assetloader.screenHeight / 3.98f);

			Assetloader.bitmapfonts.get("ShopUpgradeFont").draw(GameRenderer.batch, StarBallUpgradePrice.toString(), Assetloader.screenWidth / 2.35f, Assetloader.screenHeight / 5.45f);
			
			GameRenderer.batch.draw(Assetloader.allSpritesMap.get("StarBall"), Assetloader.screenWidth / 3f, Assetloader.screenHeight / 7.1f, (Assetloader.allSpritesMap.get("StarBall").getWidth() * Assetloader.gameScaleX), (Assetloader.allSpritesMap.get("StarBall").getHeight() * Assetloader.gameScaleY));


		Assetloader.bitmapfonts.get("ShopLevelFont").draw(GameRenderer.batch, "Buy", Assetloader.screenWidth / 1.28f, Assetloader.screenHeight / 12f);

		Assetloader.bitmapfonts.get("ShopLevelFont").draw(GameRenderer.batch, "StarBalls", Assetloader.screenWidth / 1.41f, Assetloader.screenHeight / 20f);

		GameRenderer.batch.end();
		
		
		
		
	}
	
	public void addEmitter(Vector2 Position) {
		
		Emitter emitter = new Emitter(Assetloader.allSpritesMap.get("FireParticleAlpha"), null, Position, new Vector2(200,200), 0, .5f, .25f, 8, 1);
		
		emitter.setRandDirections(true, true);
		
		GameWorld.particleEmitterArray.add(emitter);
	}
	
	@Override
	public void dispose() {
		
		super.dispose();
		
	}
	
}
