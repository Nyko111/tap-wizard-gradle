package com.nyko111.Menus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.nyko111.GameWorld.GameRenderer;
import com.nyko111.GameWorld.GameWorld;
import com.nykoengine.Helpers.Assetloader;
import com.nykoengine.UserInterface.GameButton;
import com.nykoengine.UserInterface.TextButton;


/**
 * Created by MacUser on 11/17/16.
 */
public class SignInMenu extends Stage {

    public String userID;

    boolean bSignedin = false;

    TextButton Register, Login, Save, Load;

    TextField userName, passWord;

    GameButton Close;

    float PopupX, PopupY;

    GlyphLayout glyphLayout;

    GameWorld world;

    public Net.HttpResponseListener responseLoginListener, responseSaveGameListener;

    public SignInMenu(final GameWorld world) {

        this.world = world;

        responseLoginListener = new Net.HttpResponseListener() {
            @Override
            public void handleHttpResponse(Net.HttpResponse httpResponse) {
                Gdx.app.log("Status code ", "" + httpResponse.getStatus().getStatusCode());

                String httpstring = httpResponse.getResultAsString();

                String splitstring[] = httpstring.split("\n");

                System.out.println("This is 0: " + httpstring);
                //System.out.println("This is 1: " + splitstring[1]);


                if (splitstring[0].contains("login: ")) {

                    if (splitstring[0].contains("true")) {

                        bSignedin = true;

                        GameWorld.httpConnection.setURL("http://tallraccoongames.com/TapWizardSaves.php");

                        clear();

                        //add buttons
                        addActor(Close);
                        addActor(Save);
                        addActor(Load);


                    } else {

                        //Handle if not logged in

                    }

                } else {

                    //handle error

                }

                if (splitstring[1].contains("userid")) {

                    userID = splitstring[1].replaceAll("userid: ", "");


                } else {

                    //handle error

                }

                /*if (httpResponse.getResultAsString().equals("true")) {

                    bSignedin = true;

                    GameWorld.signInMenu.clear();

                    //add buttons
                    GameWorld.signInMenu.addActor(Close);

                    System.out.println("ITS TRUE!!!~");

                }*/

            }
            @Override
            public void failed(Throwable t) {
                Gdx.app.log("Failed ", t.getMessage());
            }

            @Override
            public void cancelled() {
                Gdx.app.log("Cancelled ", null);
            }
        };

        responseSaveGameListener = new Net.HttpResponseListener() {
            @Override
            public void handleHttpResponse(Net.HttpResponse httpResponse) {
                Gdx.app.log("Status code ", "" + httpResponse.getStatus().getStatusCode());


                String httpstring = httpResponse.getResultAsString();

                String splitstring[] = httpstring.split("\n");

                if (splitstring[0].contains("money: ")) {

                    splitstring[0] = splitstring[0].replaceAll("money: ", "");

                    world.playerInfo.Money = Integer.parseInt(splitstring[0]);

                }

                if (splitstring[1].contains("icelevel: ")) {

                    splitstring[1] = splitstring[1].replaceAll("icelevel: ", "");

                    world.playerInfo.IceLevel = Integer.parseInt(splitstring[1]);

                }

                if (splitstring[2].contains("firelevel: ")) {

                    splitstring[2] = splitstring[2].replaceAll("firelevel: ", "");

                    world.playerInfo.FireLevel = Integer.parseInt(splitstring[2]);

                }

                if (splitstring[3].contains("earthlevel: ")) {

                    splitstring[3] = splitstring[3].replaceAll("earthlevel: ", "");

                    world.playerInfo.EarthLevel = Integer.parseInt(splitstring[3]);

                }

                if (splitstring[4].contains("windlevel: ")) {

                    splitstring[4] = splitstring[4].replaceAll("windlevel: ", "");

                    world.playerInfo.WindLevel = Integer.parseInt(splitstring[4]);

                }

                if (splitstring[5].contains("starballdroplevel: ")) {

                    splitstring[5] = splitstring[5].replaceAll("starballdroplevel: ", "");

                    world.playerInfo.StarBallDropLevel = Integer.parseInt(splitstring[5]);

                }

                if (splitstring[6].contains("unlockedlevels: ")) {

                    splitstring[6] = splitstring[6].replaceAll("unlockedlevels: ", "");

                    world.UnlockedLevels = Integer.parseInt(splitstring[6]);


                }

                if (splitstring[7].contains("showads: ")) {

                    splitstring[7] = splitstring[7].replaceAll("showads: ", "");

                    world.bShowAds = Boolean.parseBoolean(splitstring[7]); //Integer.parseInt(splitstring[7]);

                }

                System.out.println(httpstring);


            }
            @Override
            public void failed(Throwable t) {
                Gdx.app.log("Failed ", t.getMessage());
            }

            @Override
            public void cancelled() {
                Gdx.app.log("Cancelled ", null);
            }
        };

        PopupX = ((Assetloader.screenWidth / 2f) - ((Assetloader.allBGMap.get("LoginBG").getWidth()) / 2) * Assetloader.gameScaleX);

        PopupY = Assetloader.screenHeight / 4.42f;


        Close = new GameButton(Assetloader.screenWidth / 1.19f, Assetloader.screenHeight / 1.46f, Assetloader.allSpritesMap.get("CloseButton"), null, Assetloader.sounds.get("buttonclick"), false);

        Close.addListener(new InputListener(){
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                Close.Pushed();



                return true;
            }
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                Close.pushed = false;

                GameWorld.state.changeState(GameWorld.previousState);

            }
        });

        Register = new TextButton(Assetloader.screenWidth / 16f, Assetloader.screenHeight / 3.975f, Assetloader.allSpritesMap.get("NextLevelMenuButton"), null, Assetloader.bitmapfonts.get("ShopUpgradeFont"), "Register", Assetloader.sounds.get("buttonclick"), false);

        Register.addListener(new InputListener(){
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                Register.Pushed();



                return true;
            }
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                Register.pushed = false;

                GameWorld.state.changeState("Registration");

            }
        });

        Login = new TextButton(Assetloader.screenWidth / 1.634f, Assetloader.screenHeight / 3.975f, Assetloader.allSpritesMap.get("NextLevelMenuButton"), null, Assetloader.bitmapfonts.get("ShopUpgradeFont"), "Login", Assetloader.sounds.get("buttonclick"), false);

        Login.addListener(new InputListener(){
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                Login.Pushed();



                return true;
            }
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                Login.pushed = false;

                GameWorld.httpContent.postMap.clear();

                GameWorld.httpContent.AddpostMap("username", userName.getText());

                GameWorld.httpContent.AddpostMap("password", passWord.getText());

                GameWorld.httpContent.AddpostMap("register", "login");

                GameWorld.httpConnection.setPostContent(GameWorld.httpContent.postMap);

                GameWorld.httpConnection.HttpPost(responseLoginListener);

                //GameWorld.state.changeState(GameWorldState.REGISTRATIONMENU);

            }
        });

        Load = new TextButton(Assetloader.screenWidth / 1.634f, Assetloader.screenHeight / 3.975f, Assetloader.allSpritesMap.get("NextLevelMenuButton"), null, Assetloader.bitmapfonts.get("ShopUpgradeFont"), "Load", Assetloader.sounds.get("buttonclick"), false);

        Load.addListener(new InputListener(){
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                Load.Pushed();



                return true;
            }
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                Load.pushed = false;

                GameWorld.httpContent.postMap.clear();

                GameWorld.httpContent.AddpostMap("userid", userID);

                GameWorld.httpContent.AddpostMap("load", "load");

                /*
                //Money
                GameWorld.httpContent.AddpostMap("money", String.valueOf(GameWorld.playerInfo.Money));

                //Ice Level
                GameWorld.httpContent.AddpostMap("icelevel", String.valueOf(GameWorld.playerInfo.IceLevel));

                //Fire level
                GameWorld.httpContent.AddpostMap("firelevel", String.valueOf(GameWorld.playerInfo.FireLevel));

                //Earth level
                GameWorld.httpContent.AddpostMap("earthlevel", String.valueOf(GameWorld.playerInfo.EarthLevel));

                //Wind level
                GameWorld.httpContent.AddpostMap("windlevel", String.valueOf(GameWorld.playerInfo.WindLevel));

                //starball drop level
                GameWorld.httpContent.AddpostMap("starballdroplevel", String.valueOf(GameWorld.playerInfo.StarBallDropLevel));

                //unlocked levels
                GameWorld.httpContent.AddpostMap("unlockedlevels", String.valueOf(GameWorld.UnlockedLevels));
            */

                GameWorld.httpConnection.setPostContent(GameWorld.httpContent.postMap);

                GameWorld.httpConnection.HttpPost(responseSaveGameListener);


            }
        });

        Save = new TextButton(Assetloader.screenWidth / 16f, Assetloader.screenHeight / 3.975f, Assetloader.allSpritesMap.get("NextLevelMenuButton"), null, Assetloader.bitmapfonts.get("ShopUpgradeFont"), "Save", Assetloader.sounds.get("buttonclick"), false);

        Save.addListener(new InputListener(){
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                Save.Pushed();



                return true;
            }
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                Save.pushed = false;

                GameWorld.httpContent.postMap.clear();

                GameWorld.httpContent.AddpostMap("userid", userID);

                GameWorld.httpContent.AddpostMap("load", "save");

                //Money
                GameWorld.httpContent.AddpostMap("money", String.valueOf(world.playerInfo.Money));

                //Ice Level
                GameWorld.httpContent.AddpostMap("icelevel", String.valueOf(world.playerInfo.IceLevel));

                //Fire level
                GameWorld.httpContent.AddpostMap("firelevel", String.valueOf(world.playerInfo.FireLevel));

                //Earth level
                GameWorld.httpContent.AddpostMap("earthlevel", String.valueOf(world.playerInfo.EarthLevel));

                //Wind level
                GameWorld.httpContent.AddpostMap("windlevel", String.valueOf(world.playerInfo.WindLevel));

                //starball drop level
                GameWorld.httpContent.AddpostMap("starballdroplevel", String.valueOf(world.playerInfo.StarBallDropLevel));

                //unlocked levels
                GameWorld.httpContent.AddpostMap("unlockedlevels", String.valueOf(world.UnlockedLevels));

                //showads
                GameWorld.httpContent.AddpostMap("showads", String.valueOf(world.bShowAds));

                GameWorld.httpConnection.setPostContent(GameWorld.httpContent.postMap);

                GameWorld.httpConnection.HttpPost(responseSaveGameListener);

                //GameWorld.state.changeState(GameWorldState.REGISTRATIONMENU);

            }
        });

        Drawable cursor = new Drawable() {
            @Override
            public void draw(Batch batch, float x, float y, float width, float height) {

                Assetloader.bitmapfonts.get("ShopUpgradeFont").draw(batch, "|", x, y + (Assetloader.screenHeight / 20f));

            }

            @Override
            public float getLeftWidth() {
                return 0;
            }

            @Override
            public void setLeftWidth(float leftWidth) {

            }

            @Override
            public float getRightWidth() {
                return 0;
            }

            @Override
            public void setRightWidth(float rightWidth) {

            }

            @Override
            public float getTopHeight() {
                return 0;
            }

            @Override
            public void setTopHeight(float topHeight) {

            }

            @Override
            public float getBottomHeight() {
                return 0;
            }

            @Override
            public void setBottomHeight(float bottomHeight) {

            }

            @Override
            public float getMinWidth() {
                return 0;
            }

            @Override
            public void setMinWidth(float minWidth) {

            }

            @Override
            public float getMinHeight() {
                return 0;
            }

            @Override
            public void setMinHeight(float minHeight) {

            }
        };

        final TextField.TextFieldStyle textFieldStyle = new TextField.TextFieldStyle(Assetloader.bitmapfonts.get("ShopUpgradeFont"), Color.WHITE, cursor, null, null);

        userName = new TextField("", textFieldStyle);
        userName.setPosition(Assetloader.screenWidth / 5.5f, Assetloader.screenHeight / 1.75f);
        userName.setSize(Assetloader.screenWidth / 1.6f, Assetloader.screenHeight / 12.62f);

        userName.addListener(new InputListener(){

            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {

                Gdx.input.setOnscreenKeyboardVisible(true);

            }
        });

        passWord = new TextField("", textFieldStyle);
        passWord.setPosition(Assetloader.screenWidth / 5.5f, Assetloader.screenHeight / 2.515f);
        passWord.setSize(Assetloader.screenWidth / 1.6f, Assetloader.screenHeight / 12.62f);
        passWord.setPasswordCharacter('*');
        passWord.setPasswordMode(true);

        passWord.addListener(new InputListener(){

            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {

                Gdx.input.setOnscreenKeyboardVisible(true);

            }
        });


        this.addActor(userName);
        this.addActor(passWord);
        this.addActor(Register);
        this.addActor(Login);
        this.addActor(Close);


    }

    @Override
    public void draw() {

        GameRenderer.batch.begin();

        //Popup BG
        GameRenderer.batch.draw(Assetloader.allBGMap.get("LoginBG"), PopupX, PopupY, Assetloader.allBGMap.get("LoginBG").getWidth() * Assetloader.gameScaleX, Assetloader.allBGMap.get("LoginBG").getHeight() * Assetloader.gameScaleY);


        if (!bSignedin) {
            //Username field
            Assetloader.bitmapfonts.get("ShopUpgradeFont").draw(GameRenderer.batch, "UserName", Assetloader.screenWidth / 3.21f, Assetloader.screenHeight / 1.43f);

            GameRenderer.batch.draw(Assetloader.allSpritesMap.get("TextField"), Assetloader.screenWidth / 6.64f, Assetloader.screenHeight / 1.755f, Assetloader.allSpritesMap.get("TextField").getWidth() * Assetloader.gameScaleX, Assetloader.allSpritesMap.get("TextField").getHeight() * Assetloader.gameScaleY);

            //Password field
            Assetloader.bitmapfonts.get("ShopUpgradeFont").draw(GameRenderer.batch, "Password", Assetloader.screenWidth / 3.17f, Assetloader.screenHeight / 1.9f);

            GameRenderer.batch.draw(Assetloader.allSpritesMap.get("TextField"), Assetloader.screenWidth / 6.64f, Assetloader.screenHeight / 2.515f, Assetloader.allSpritesMap.get("TextField").getWidth() * Assetloader.gameScaleX, Assetloader.allSpritesMap.get("TextField").getHeight() * Assetloader.gameScaleY);

        } else {

            Assetloader.bitmapfonts.get("MainWiz").draw(GameRenderer.batch, "You are", Assetloader.screenWidth / 3.6f, Assetloader.screenHeight / 1.62f);

            Assetloader.bitmapfonts.get("MainWiz").draw(GameRenderer.batch, "Signed in!", Assetloader.screenWidth / 5f, Assetloader.screenHeight / 2f);


        }



        GameRenderer.batch.end();



        super.draw();
    }


}
