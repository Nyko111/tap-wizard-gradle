package com.nyko111.Menus;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.nyko111.GameWorld.GameWorld;
import com.nykoengine.UserInterface.GameButton;

public class MainMenu extends Stage {
	
	public MainMenu(final GameWorld world) {


		//PlayButton = new GameButton(Assetloader.screenWidth / 2 - (Assetloader.allSpritesMap.get("PlayButton").getWidth() * Assetloader.gameScaleX) / 2, Assetloader.screenHeight / 6f, Assetloader.allSpritesMap.get("PlayButton"), null, Assetloader.sounds.get("buttonclick"), false);
		
		/*PlayButton.addListener(new InputListener(){
			 public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				 	PlayButton.Pushed();
	   
				 	
	            	
	                return true;
	            }
	            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
	            	PlayButton.pushed = false;
	            	
	            	GameWorld.state.changeState(GameWorldState.LEVELSELECT);
	            
	            }
	        });
		
		ExitButton = new GameButton(Assetloader.screenWidth / 2 - (Assetloader.allSpritesMap.get("ExitButton").getWidth() * Assetloader.gameScaleX) / 2, Assetloader.screenHeight / 20f, Assetloader.allSpritesMap.get("ExitButton"), null, Assetloader.sounds.get("buttonclick"), false);
		
		ExitButton.addListener(new InputListener(){
			 public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				 	ExitButton.Pushed();
	   
				 	
	            	
	                return true;
	            }
	            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
	            	ExitButton.pushed = false;
	            	
	            	GameWorld.state.changeState(GameWorldState.EXITING);
	            
	            }
	        });
		
		OptionButton = new GameButton(Assetloader.screenWidth / 1.25f, Assetloader.screenHeight / 13.47f, Assetloader.allSpritesMap.get("OptionsButton"), null, Assetloader.sounds.get("buttonclick"), false);
		
		OptionButton.addListener(new InputListener(){
			 public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				 	OptionButton.Pushed();
	   
				 	
	            	
	                return true;
	            }
	            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
	            	OptionButton.pushed = false;
	            	
	            	GameWorld.state.changeState(GameWorldState.SETTINGSMENU);
	            
	            }
	        });*/
			
			this.addListener(new InputListener(){
			 public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
	   
	                return true;
	            }
	            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
	            	
	            	world.state.changeState("LevelSelect");
	            
	            }
	        });
		
	}
	
	@Override
	public void draw() {
		
		super.draw();
		
	}

}
