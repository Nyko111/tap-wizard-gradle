package com.nyko111.Menus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.nyko111.GameWorld.GameRenderer;
import com.nyko111.GameWorld.GameWorld;
import com.nykoengine.Helpers.Assetloader;
import com.nykoengine.UserInterface.GameButton;
import com.nykoengine.UserInterface.TextButton;


/**
 * Created by MacUser on 11/17/16.
 */
public class RewardedVideoMenu extends Stage {

    public String previousState;

    TextButton Watch;

    GameButton Close;

    float PopupX, PopupY;

    GlyphLayout glyphLayout;

    public RewardedVideoMenu() {

        PopupX = ((Assetloader.screenWidth / 2f) - ((Assetloader.allBGMap.get("LoginBG").getWidth()) / 2) * Assetloader.gameScaleX);

        PopupY = Assetloader.screenHeight / 4.42f;


        Close = new GameButton(Assetloader.screenWidth / 1.19f, Assetloader.screenHeight / 1.46f, Assetloader.allSpritesMap.get("CloseButton"), null, Assetloader.sounds.get("buttonclick"), false);

        Close.addListener(new InputListener(){
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                Close.Pushed();



                return true;
            }
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                Close.pushed = false;

                GameWorld.state.changeState(previousState);

            }
        });

        Watch = new TextButton(Assetloader.screenWidth / 2.95f, Assetloader.screenHeight / 3.975f, Assetloader.allSpritesMap.get("NextLevelMenuButton"), null, Assetloader.bitmapfonts.get("ShopUpgradeFont"), "Register", Assetloader.sounds.get("buttonclick"), false);

        Watch.addListener(new InputListener(){
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                Watch.Pushed();



                return true;
            }
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                Watch.pushed = false;

                GameWorld.googleInterface.PaidAd();

            }
        });


        this.addActor(Watch);
        this.addActor(Close);


    }

    @Override
    public void draw() {

        GameRenderer.batch.begin();

        //Popup BG
        GameRenderer.batch.draw(Assetloader.allBGMap.get("LoginBG"), PopupX, PopupY, Assetloader.allBGMap.get("LoginBG").getWidth() * Assetloader.gameScaleX, Assetloader.allBGMap.get("LoginBG").getHeight() * Assetloader.gameScaleY);

        Assetloader.bitmapfonts.get("ShopUpgradeFont").draw(GameRenderer.batch, "Watch a video", Assetloader.screenWidth / 3.21f, Assetloader.screenHeight / 1.43f);

        Assetloader.bitmapfonts.get("ShopUpgradeFont").draw(GameRenderer.batch, "and earn", Assetloader.screenWidth / 3.21f, Assetloader.screenHeight / 1.43f);


        GameRenderer.batch.draw(Assetloader.allSpritesMap.get("StarBall"), Assetloader.screenWidth / 2.6f, Assetloader.screenHeight / 1.115f, Assetloader.allSpritesMap.get("StarBall").getWidth() * Assetloader.gameScaleX, Assetloader.allSpritesMap.get("StarBall").getHeight() * Assetloader.gameScaleY);


        Assetloader.bitmapfonts.get("ShopUpgradeFont").draw(GameRenderer.batch, "100", Assetloader.screenWidth / 3.21f, Assetloader.screenHeight / 1.43f);



        GameRenderer.batch.end();



        super.draw();
    }


}
