package com.nyko111.GameWorld;


import java.util.ArrayList;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.nyko111.Actors.Enemy;
import com.nyko111.Actors.ManaBall;
import com.nyko111.Actors.SpawnList;
import com.nyko111.Actors.StarBall;
import com.nyko111.Actors.Wall;
import com.nyko111.Actors.Wizard;
import com.nyko111.Spells.EarthSpell;
import com.nyko111.Spells.ProjectileSpell;
import com.nyko111.Spells.SpellBase;
import com.nyko111.Spells.WindSpell;
import com.nykoengine.Animation.AnimNyko;
import com.nykoengine.Animation.ScreenShake;
import com.nykoengine.Audio.AudioManager;
import com.nykoengine.Detection.SpawnPoint;
import com.nykoengine.Helpers.Assetloader;
import com.nykoengine.Helpers.SpawnLogic;
import com.nykoengine.ParticleEngine.Emitter;
import com.nykoengine.UserInterface.FillableBar;
import com.nykoengine.UserInterface.GameButton;
import com.nykoengine.UserInterface.FillableBar.BarType;

public class MainStage extends Stage implements InputProcessor {
	
	Integer randomSpriteNumber;
	
	boolean Dispose = false;
	
	public Wizard wizard;
	
	public Wall wall;
	
	public int MaxEnemies = 0;
	
	public int ScoreMultiplier = 1;
	
	public int Score = 0;
	
	public int TopScore = 0;
	
	//private GameWorld world;
	
	//public DialogueBox dialogueBox;
	
	public ArrayList<SpellBase> spellArray = new ArrayList<SpellBase>();
	
	public ArrayList<StarBall> starBallArray = new ArrayList<StarBall>();
	
	public ArrayList<ManaBall> manaBallArray = new ArrayList<ManaBall>();
	
	public Emitter spellButtonEmitter;
	
	public SpellSelector spellSelector;
	
	public GameButton[] selectedSpells = new GameButton[4];
	
	public ArrayList<Enemy> enemyArray = new ArrayList<Enemy>();
	
	GameButton Pause;
	
	//public AnimNyko enemyanimation;
	
	Boolean bIsLoaded = false;
	
	public SpawnLogic enemySpawn;
	
	public SpawnList spawnList;
	
	boolean SpellCoolDown = false;
	
	float coolDownTimeLeft = .5f;
	
	ScheduledThreadPoolExecutor collisionTimer = new ScheduledThreadPoolExecutor(2);
	
	public FillableBar manaBar;
	
	float rainbowX, rainbowY;
	
	public static ScreenShake screenShake;

	GameWorld world;

	public MainStage(final GameWorld world) {
		// TODO Auto-generated constructor stub

		this.world = world;

		//stage = new Stage(world.screenWidth, world.gameHeight, true);

		//TEST DEBUG SHIT TAPWIZ
		
		//dialogueBox = new DialogueBox(Assetloader.allSpritesMap.get("dialouge"), 100f, 100f, 0f, 0f, new Vector2(Assetloader.allSpritesMap.get("dialouge").getWidth(), Assetloader.allSpritesMap.get("dialouge").getHeight()), GameWorld.Main, "Test, how are you? testing12 123 testing derp 12");
		
		//
		
		manaBar = new FillableBar(Assetloader.screenWidth / 1.175f, Assetloader.screenHeight / 4f, 100f, 11.75f, Assetloader.allSpritesMap.get("ManaEnd"), Assetloader.allSpritesMap.get("ManaMid"), Assetloader.allSpritesMap.get("ManaEndTop"), Assetloader.allSpritesMap.get("ManaEndAlpha"), Assetloader.allSpritesMap.get("ManaMidAlpha"),  Assetloader.allSpritesMap.get("ManaFill"), BarType.VERTICALBAR);
		
		//manaBar.setWallWidth(18 * Assetloader.gameScaleY);
		
		manaBar.setBarFillOffset(Assetloader.screenHeight / 150.58f);

		manaBar.Reset();
		
		spellButtonEmitter = new Emitter(Assetloader.allSpritesMap.get("FireParticleAlpha"), null, new Vector2(Assetloader.screenWidth / 6.1f, Assetloader.screenHeight / 16f), new Vector2(Assetloader.screenWidth / 8, Assetloader.screenWidth / 8), 0, 1.5f, -1f, 3, 750);
		
		wizard = new Wizard();
	
		Pause = new GameButton(Assetloader.screenWidth / 64f, Assetloader.screenHeight / 1.108f, Assetloader.allSpritesMap.get("PauseButton"), null, Assetloader.sounds.get("buttonclick"), false);
		
		Pause.addListener(new InputListener(){
			 public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				 	Pause.Pushed();
	   
				 	
	            	
	                return true;
	            }
	            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
	            	Pause.pushed = false;
	            	
	            	world.state.changeState("Paused");
	            
	            }
	        });
		
		spawnList = new SpawnList(this, world);
				
		wall = new Wall(((Assetloader.screenWidth / 2f) - ((Assetloader.allSpritesMap.get("wall").getWidth()) / 2) * Assetloader.gameScaleX), 0, Assetloader.allSpritesMap.get("wall"));
		
		spellSelector = new SpellSelector(world);
		
		selectedSpells = spellSelector.Spells();
		
		//Spell button particle emitter
		
		//END DEBUG TAPWIZ
		
		Vector2[] positions = new Vector2[3];
		
		SpawnPoint[] spawnPoints = new SpawnPoint[3];
		
		Vector2 pos1 = new Vector2(), pos2 = new Vector2(), pos3 = new Vector2();
		
		pos1.x = Assetloader.screenWidth / 9.2f;
		pos1.y = Assetloader.screenHeight / 1.16f;
		
		pos2.x = Assetloader.screenWidth / 2.68f;
		pos2.y = Assetloader.screenHeight / 1.16f;
		
		pos3.x = Assetloader.screenWidth / 1.57f;
		pos3.y = Assetloader.screenHeight / 1.16f;
		
		//pos5.x = Assetloader.screenWidth / 1.23f;
		//pos5.y = Assetloader.screenHeight / 1.16f;
		
		spawnPoints[0] = new SpawnPoint(pos1.x, pos1.y, (Assetloader.allSpritesMap.get("slime_walk01").getWidth() * Assetloader.gameScaleX) * 0.8f, (Assetloader.allSpritesMap.get("slime_walk01").getHeight() * Assetloader.gameScaleY) * 0.8f);
		spawnPoints[1] = new SpawnPoint(pos2.x, pos2.y, (Assetloader.allSpritesMap.get("slime_walk01").getWidth() * Assetloader.gameScaleX) * 0.8f, (Assetloader.allSpritesMap.get("slime_walk01").getHeight() * Assetloader.gameScaleY) * 0.8f);
		spawnPoints[2] = new SpawnPoint(pos3.x, pos3.y, (Assetloader.allSpritesMap.get("slime_walk01").getWidth() * Assetloader.gameScaleX) * 0.8f, (Assetloader.allSpritesMap.get("slime_walk01").getHeight() * Assetloader.gameScaleY) * 0.8f);
		
		
		//positions[4] = pos5;
		
		enemySpawn = new SpawnLogic(spawnPoints, spawnList, 5);
		
		collisionTimer.scheduleAtFixedRate( new Runnable() {
			@Override
			public void run() {
				
				enemySpawn.SpawnPointCollision(null);
				
				for (int i = 0; i < enemyArray.size(); i++) {
					
					enemySpawn.SpawnPointCollision(enemyArray.get(i).hitbox.getBounds());
					
					if (enemyArray.get(i) != null) {
						
						for (int y = i; y < enemyArray.size(); y++)  {
							
							if (enemyArray.get(y) != null && enemyArray.get(i) != enemyArray.get(y)) {
								
								if (Intersector.overlaps(enemyArray.get(i).hitbox.getBounds(), enemyArray.get(y).hitbox.getBounds())) {
								
									enemyArray.get(y).Collide(enemyArray.get(i).WalkSpeed - 10f);
									
									//enemyArray.get(y).Collide(enemyArray.get(i).WalkSpeed);
									//enemyArray.get(i).Collide(enemyArray.get(y).WalkSpeed);
									
								} else if (enemyArray.get(y) != null){
									
									enemyArray.get(y).bIsColliding = false;
									enemyArray.get(i).bIsColliding = false;
									
								}
							} 
							
						} 
					}
				}
				
			}
		}, 0, 250, TimeUnit.MILLISECONDS);
		
		//this.addActor(ShopMenuButton);
		//button2 = new GameButton(0, 0, world, Assetloader.button1, Assetloader.button1);
		//button2.setTouchable(Touchable.enabled);
		
		//End of level rainbow
		
		rainbowX = ((Assetloader.screenWidth / 2) - (((Assetloader.allBGMap.get("rainbow").getWidth() / 2)) * Assetloader.gameScaleX));
		rainbowY = Assetloader.screenHeight / 1.8f;
		
		screenShake = new ScreenShake(this.getCamera().viewportWidth / 2, this.getCamera().viewportHeight / 2, 1);
		
	}
		
	@Override
	public void act(float deltaTime) {

			super.act(deltaTime);
			
			//Disable spawn if max enemies are on screen
			if (enemyArray.size() >= MaxEnemies) {
				
				enemySpawn.bSpawnable = false;
				
			} else {
				
				enemySpawn.bSpawnable = true;
				
			}
			
			//SpellCooldown timer
			if (SpellCoolDown) {
				
				coolDownTimeLeft -= deltaTime;
				
				if (coolDownTimeLeft <= 0) {
					
					SpellCoolDown = false;
				}
				
			}
		
			//Shakes screen if screneshake has time left on it
			screenShake.shakeScreen(GameRenderer.cam, GameRenderer.batch);
			
			
			wizard.update(deltaTime);
			
			manaBar.update(deltaTime);
			
			manaBar.IncrementFill(deltaTime * 5);
			
			for (int i = 0; enemyArray.size() > i; i++) {
			
				if (enemyArray.get(i) != null) {
				
					if (enemyArray.get(i).isDead) {
					
						enemyArray.remove(i);
					}
				}
			
			}
			
			
			for (int i = 0; i < starBallArray.size(); i++) {
				
				starBallArray.get(i).update(deltaTime);
				
				if (!starBallArray.get(i).Moving) { 
					
					starBallArray.remove(i);
					
				}
				
			}
			
			for (int i = 0; i < manaBallArray.size(); i++) {
				
				manaBallArray.get(i).update(deltaTime);
				
				if (!manaBallArray.get(i).Moving) { 
					
					manaBallArray.remove(i);
					
				}
				
			}
	
			for (int i = 0; spellArray.size() > i; i++) {
			
				//chcek if spells are used
				
				if (spellArray.get(i) != null) {
					
				if (spellArray.get(i).spellUsed /*|| spellArray.get(i).Position.y > Assetloader.screenHeight*/) {
			
					spellArray.remove(i);
				
					}
				}
			}

	}
	
	@Override
	public void draw() {
		
		super.draw();
		GameRenderer.batch.begin();
		
		GameRenderer.batch.draw(Assetloader.allBGMap.get("MainBG-Tree"), 0, 0, Assetloader.allBGMap.get("MainBG-Tree").getWidth() * Assetloader.gameScaleX, Assetloader.allBGMap.get("MainBG-Tree").getHeight() * Assetloader.gameScaleY);
		
		Pause.draw(GameRenderer.batch, 1);
		
		wall.draw(GameRenderer.batch, 1f);
		
		for (int i = 0; i < starBallArray.size(); i++) {
			
			starBallArray.get(i).draw(GameRenderer.batch, 1f);
			
		}
		
		for (int i = 0; i < manaBallArray.size(); i++) {
			
			manaBallArray.get(i).draw(GameRenderer.batch, 1f);
			
		}
		
		wizard.draw(GameRenderer.batch);
		
		for (int i = 0; i < 4; i++) {
			
			selectedSpells[i].draw(GameRenderer.batch, 1);
		}
		
		if (spawnList.DoneSpawning() && enemyArray.isEmpty()) {
			
			GameRenderer.batch.draw(Assetloader.allBGMap.get("rainbow"), rainbowX, rainbowY, Assetloader.allBGMap.get("rainbow").getWidth() * Assetloader.gameScaleX, Assetloader.allBGMap.get("rainbow").getHeight() * Assetloader.gameScaleY);
		
		}
	
		//dialogueBox.draw(GameRenderer.batch);
		GameRenderer.batch.end();
		
	}
	
	
	public void AddUI() {
		
		//this.addActor(Shield);
		//this.addActor(Freeze);
		//this.addActor(Explosion);
		this.addActor(Pause);
		
		this.addActor(selectedSpells[0]);
		this.addActor(selectedSpells[1]);
		this.addActor(selectedSpells[2]);
		this.addActor(selectedSpells[3]);
		
	}
	
	public void clear() {
		
		bIsLoaded = false;
		
		
		super.clear();
		
	}
	
	public void addWindSpell(Vector2 Target) {
			
		//1
	 	spellArray.add(new WindSpell(0, Assetloader.allSpritesMap.get("wind_magic"), null, Target.cpy(), spellSelector.selectedSpell, spellSelector.selectedStatusEffect, spellSelector.damagedColor, enemyArray, world));
		//spellArray.get(spellArray.size() - 1).setParticleSprites(Assetloader.allSpritesMap.get("fire_particle"), Assetloader.allSpritesMap.get("FireParticleAlpha"));
	 	this.addActor(spellArray.get(spellArray.size() - 1));
	 	
	 	Assetloader.sounds.get("WindSpell").play(AudioManager.SoundVolume);
			
	}
		
	public void addFireSpell(Vector2 Target) {
			
	 	//spellArray.add(new FireSpell(4.98f + (0.02f * world.playerInfo.FireLevel), Assetloader.allSpritesMap.get("firemagic"), Assetloader.allSpritesMap.get("firemagic alpha"), Target, spellSelector.selectedSpell, spellSelector.selectedStatusEffect, spellSelector.damagedColor, enemyArray));
		
		spellArray.add(new ProjectileSpell(4.5f + (0.5f * world.playerInfo.FireLevel), Assetloader.allSpritesMap.get("firemagic"), Assetloader.allSpritesMap.get("firemagic alpha"), Target, spellSelector.selectedSpell, spellSelector.selectedStatusEffect, spellSelector.damagedColor, enemyArray, world));
		
		spellArray.get(spellArray.size() - 1).setParticleSprites(Assetloader.allSpritesMap.get("fire_particle"), Assetloader.allSpritesMap.get("FireParticleAlpha"));
	 	this.addActor(spellArray.get(spellArray.size() - 1));
		
		Assetloader.sounds.get("FireSpell").play(AudioManager.SoundVolume);
				
	}
	
	public void addEarthSpell(Vector2 Target) {
	
		spellArray.add(new EarthSpell(7.8f + (0.2f * world.playerInfo.EarthLevel), Assetloader.allSpritesMap.get("EarthSpell"), Assetloader.allSpritesMap.get("earthmagic shadow"), Target, spellSelector.selectedSpell, spellSelector.selectedStatusEffect, spellSelector.damagedColor, world));
		spellArray.get(spellArray.size() - 1).setParticleSprites(Assetloader.allSpritesMap.get("EarthParticle"), null);
		this.addActor(spellArray.get(spellArray.size() - 1));
		
		
	}
	
	public void addIceSpell(Vector2 Target) {
		
		Vector2 Target2 = Target.cpy();
			
		Target2.x += Assetloader.screenWidth / 6f;
			
		Vector2 Target3 = Target.cpy();
			
		Target3.x -= Assetloader.screenWidth / 6f;
			
		//Left ice part
		
		randomSpriteNumber = Assetloader.rand.nextInt(3) + 1;
		
		spellArray.add(new ProjectileSpell(1.5f + ((0.5f * world.playerInfo.IceLevel)), Assetloader.allSpritesMap.get("IceSpell" + randomSpriteNumber), null, Target2.cpy(), spellSelector.selectedSpell, spellSelector.selectedStatusEffect, spellSelector.damagedColor, enemyArray, world));
		spellArray.get(spellArray.size() - 1).setParticleSprites(Assetloader.allSpritesMap.get("IceParticle"), null);
		this.addActor(spellArray.get(spellArray.size() - 1));
		
		//Main ice part
		spellArray.add(new ProjectileSpell(1.5f + ((0.5f * world.playerInfo.IceLevel)), Assetloader.allSpritesMap.get("IceSpellMain"), null, Target.cpy(), spellSelector.selectedSpell, spellSelector.selectedStatusEffect, spellSelector.damagedColor, enemyArray, world));
		spellArray.get(spellArray.size() - 1).setParticleSprites(Assetloader.allSpritesMap.get("IceParticle"), null);
		this.addActor(spellArray.get(spellArray.size() - 1));
 		
 		//Right ice part
 		
 		randomSpriteNumber = Assetloader.rand.nextInt(4) + 1;
 		
 		spellArray.add(new ProjectileSpell(1.5f + ((0.5f * world.playerInfo.IceLevel)), Assetloader.allSpritesMap.get("IceSpell" + randomSpriteNumber), null, Target3.cpy(), spellSelector.selectedSpell, spellSelector.selectedStatusEffect, spellSelector.damagedColor, enemyArray, world));
		spellArray.get(spellArray.size() - 1).setParticleSprites(Assetloader.allSpritesMap.get("IceParticle"), null);
		this.addActor(spellArray.get(spellArray.size() - 1));
 		
 		Assetloader.sounds.get("IceSpell").play(AudioManager.SoundVolume);
	
		
	}
	
	public void ResetScoreMultiplier() {
		
		ScoreMultiplier = 1;
		
	}
	
	public void ScoreMultiplier() {
		
		Score += ScoreMultiplier;
		
		ScoreMultiplier = ScoreMultiplier * 2;
		
	}
	
	@Override
	 public boolean touchDown(int screenX, int screenY, int pointer, int button) {
       
			super.touchDown(screenX, screenY, pointer, button);
		
			if (!SpellCoolDown && manaBar.Value >= spellSelector.ManaCost && (GameWorld.state.getCurrentState().equals(GameWorld.state.StateMap.get("Playing")))) {
			
					if (screenY < Assetloader.screenHeight / 1.3f) {
						
						
						//Reduce mana in mana pool
						manaBar.Value -= spellSelector.ManaCost;
						
						wizard.currentAnim = Wizard.SpellThrowAnim;
			
						Vector2 TargetPos = new Vector2();

						System.out.println("Tap X: " + Gdx.input.getX());


						if (Gdx.input.getX() < Gdx.graphics.getWidth() / 2.35f) {
							TargetPos.x = Gdx.input.getX() / 2f;
						} else if (Gdx.input.getX() > Gdx.graphics.getWidth() / 1.73f) {

								TargetPos.x = Gdx.input.getX();

						} else {

							TargetPos.x = Assetloader.screenWidth / 2f;

							System.out.println("Middle shot");

						}

						TargetPos.y =  Gdx.graphics.getHeight()- Gdx.input.getY();//Assetloader.screenHeight;
            
					if (spellSelector.selectedSpell == spellSelector.selectedSpell.Earth) {
            	
						addEarthSpell(TargetPos);
            	
					} else if (spellSelector.selectedSpell == spellSelector.selectedSpell.Fire) {
            	
						addFireSpell(TargetPos);
            	
            	
					} else if (spellSelector.selectedSpell == spellSelector.selectedSpell.Water) {
            	
						TargetPos.y =  Gdx.graphics.getHeight() / 2f;
						
						addIceSpell(TargetPos);
            	
					} else if (spellSelector.selectedSpell == spellSelector.selectedSpell.Air) {
		            	
								//TargetPos.x = screenX;
						
						TargetPos.y =  Gdx.graphics.getHeight() / 2f;
								
						addWindSpell(TargetPos.cpy());
						
								
		            	
					}
           
				
					SpellCoolDown = true;
					
					coolDownTimeLeft = .5f;
					
				}
					
            
			} else if (!SpellCoolDown && (GameWorld.state.getCurrentState().equals(world.state.StateMap.get("Playing")))) {
				
				Assetloader.sounds.get("NoMana").play(world.audioManager.SoundVolume);
				
			}
			
        return true;
    }

	public void Reset() {

		manaBar.Reset();
		clear();
		enemyArray.clear();
		GameWorld.particleEmitterArray.clear();

		Score = 0;
		ResetScoreMultiplier();

	}
	
		public void dispose() {
			
			if (!Dispose) {

				spawnList = null;

				manaBar.dispose();
				
				wizard.dispose();
				
				spellButtonEmitter.dispose();
				
				enemyArray.clear();
				
				spellArray.clear();
				
				manaBallArray.clear();
				
				starBallArray.clear();
				
				collisionTimer.shutdown();
				
				spellSelector.dispose();
				
				enemySpawn.dispose();
				
				super.dispose();
				
				Dispose = true;
			}
		}
}

	

