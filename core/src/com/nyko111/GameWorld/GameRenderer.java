package com.nyko111.GameWorld;


import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ai.Agent;
import com.badlogic.gdx.ai.fsm.DefaultStateMachine;
import com.badlogic.gdx.ai.fsm.StateMachine;
import com.badlogic.gdx.ai.msg.MessageDispatcher;
import com.badlogic.gdx.ai.msg.Telegram;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.nykoengine.NykoFSM.GameState;
import com.nykoengine.Helpers.Assetloader;
import com.nykoengine.ParticleEngine.Emitter;

public class GameRenderer {
	
	boolean Dispose = false;
	
	//static boolean shakeReverse = true;
	
	//SpriteBatch is called to batch up all the sprites to display
	public static SpriteBatch batch;
	
	public GameWorld world;
	
	static public OrthographicCamera cam;
	
	//public Stage stage;
	
	//Time between frames
	public float deltaTime;
	
	//private float gameHeight;
	
	//public static StateMachine<GameRenderer> state;
	
	public static Sprite CurrentBG = Assetloader.allBGMap.get("titlemenu");
	
	//float fontx, fonty;
	
	public static float InitCamX, InitCamY;
	
	public GameRenderer(GameWorld world) {
		this.world = world;

		//this.gameHeight = gameHeight;
		//this.uiStage = world.getuiStage().uiStage;
		
		//stage.setViewport(screenWidth, gameHeight, true);
		cam = new OrthographicCamera();
		cam.setToOrtho(false, Assetloader.screenWidth, Assetloader.screenHeight);
		cam.update();
		
		InitCamX = cam.position.x;
		InitCamY = cam.position.y;
		//input = new InputHandler(world);
		batch = new SpriteBatch();
		batch.setProjectionMatrix(cam.combined);
		
		//this.stage = world.getLevel();
		
		//state = new DefaultStateMachine<GameRenderer>(this, GameRendererState.MAINMENU);
		
		
		//this makes the stage shake with everything else
		////world.mainStage.getViewport().setCamera(GameRenderer.cam);

		
	}
	
	public void render(float deltaTime) {
		//Render everything to screen
		
		Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
		Gdx.graphics.getGL20().glClear( GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT );
		
		batch.begin();
		batch.draw(CurrentBG, 0, 0, CurrentBG.getWidth() * Assetloader.gameScaleX, CurrentBG.getHeight() * Assetloader.gameScaleY);
		
		batch.end();
		
		((GameState)(world.state.getCurrentState())).Render(world, this);
		
		batch.begin();
		for (int i = 0; world.particleEmitterArray.size() > i; i++) {
			
			if (world.particleEmitterArray.get(i) != null)
				world.particleEmitterArray.get(i).draw(GameRenderer.batch, 1f);
			
		}
		batch.end();
		
		cam.update();
		//REENTER
		/*if (world.confirmPopup.bActiveConfirmPopup) {
			confirmPopupDraw();
		}*/
		
		
		
	}
	
	public static void resetCamera() {
		
		//fix cam after shaking it so everythings back to 0
		
		cam.position.x = InitCamX;
		
		cam.position.y = InitCamY;
		
		cam.update();
		
		batch.setProjectionMatrix(GameRenderer.cam.combined);
		
	}
	
	
	static public void confirmPopupDraw() {
		
		//GameWorld.confirmPopup.draw(batch);
		
	}
	
	public void dispose() {
		
		if (!Dispose) {
			
			batch.dispose();
			
			Dispose = true;
			
		}
		
	}

}
