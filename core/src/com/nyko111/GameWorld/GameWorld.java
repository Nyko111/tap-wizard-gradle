package com.nyko111.GameWorld;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.nyko111.Helping.SaveLoader;
import com.nyko111.Interfaces.GoogleInterface;
import com.nyko111.Interfaces.IabInterface;
import com.nyko111.Menus.GameOverMenu;
import com.nyko111.Menus.LevelSelectMenuNEW;
import com.nyko111.Menus.MainMenu;
import com.nyko111.Menus.NextLevelMenu;
import com.nyko111.Menus.PausedMenu;
import com.nyko111.Menus.RegistrationMenu;
import com.nyko111.Menus.SettingsMenu;
import com.nyko111.Menus.ShopMenuWiz;
import com.nyko111.Menus.SignInMenu;
import com.nyko111.Menus.StarballsIAB;
import com.nyko111.TutorialPages.TutorialEarthSpell;
import com.nyko111.TutorialPages.TutorialFireSpell;
import com.nyko111.TutorialPages.TutorialIceSpell;
import com.nyko111.TutorialPages.TutorialManaBar;
import com.nyko111.TutorialPages.TutorialWindSpell;
import com.nykoengine.NykoFSM.FSM;
import com.nyko111.TutorialPages.TutorialChooseSpell;
import com.nyko111.TutorialPages.TutorialShop;
import com.nyko111.TutorialPages.TutorialSlimeTypes;
import com.nyko111.TutorialPages.TutorialTouchAnywhere;
import com.nykoengine.Audio.AudioManager;
import com.nykoengine.Helpers.Assetloader;
import com.nykoengine.Network.HttpConnection;
import com.nykoengine.Network.HttpContent;
import com.nykoengine.ParticleEngine.Emitter;
import com.nykoengine.TutorialEngine.TutorialHandler;
import com.nykoengine.TutorialEngine.TutorialPageBase;
import com.nykoengine.UserInterface.ConfirmPopup;

import java.util.ArrayList;
import java.util.Timer;

import GameStates.ExitingState;
import GameStates.GameOverState;
import GameStates.IABState;
import GameStates.LevelSelectState;
import GameStates.LoginState;
import GameStates.MainMenuState;
import GameStates.NextLevelState;
import GameStates.PausedState;
import GameStates.PlayingState;
import GameStates.RegistrationState;
import GameStates.SettingsState;
import GameStates.ShopState;
import GameStates.TutorialState;


public class GameWorld {

	//This is where all the game logic goes

	public boolean bShowAds = true;
	
	boolean Dispose = false;
	
	public int UnlockedLevels = 1;
	
	public int CurrentLevel = 0;
	
	//Holds all the info about player, purchased goods, levels, money, etc
	public static PlayerInfo playerInfo;


	public AudioManager audioManager = new AudioManager();
	
	//All menu's
	public MainMenu mainMenu;
	
	public LevelSelectMenuNEW levelSelectMenu;
	
	public NextLevelMenu nextLevelMenu;
	
	public GameOverMenu gameOverMenu;
	
	public ShopMenuWiz shopMenu;
	
	public StarballsIAB IAB;

	public SettingsMenu settingsMenu;
	
	public PausedMenu pausedMenu;

	public SignInMenu signInMenu;

	public RegistrationMenu registrationMenu;
	
	//MainStage is the main game stage (where the player or characters are aswell as the GUI
	public MainStage mainStage;

	//Handles music. Obviously.
	
	public static ArrayList<Emitter> particleEmitterArray = new ArrayList<Emitter>();
	
	//This interface allows you to communicate with the android launcher without it, communication is impossible and IAB will not work
	public static IabInterface platformInterface;
	
	//bBack boolean used to detect if back button on android is pressed, if it is, then goes back.
	//Using booleans it prevents it from just skipping back multiple pages
	public Boolean bBack = false;
	
	//Detect if screen is touched
	public Boolean bTouched = false;
	
	//If game is paused
	public static boolean bPaused = false;
	
	//Menu's, these hold gamebuttons and logic for them.
	
	//GameState Machine
	public static FSM state;
	
	//Delta time between frames
	public static float deltaTime;
	
	public boolean bIsLoaded = false;
	
	//Currency
	
	public static int TestItem = 0;
	
	public SaveLoader saveLoader;
	
	//public static ItemHandler itemHandler = new ItemHandler();
	
	//Current time
	public long time;
	public long seconds;
	public long minutes;
	public long minutesSinceLastPlayed;
	
	public static boolean bInterstitialShown = false;
	
	public Timer InterstitialTimer;
	
	//public static BitmapFont Main, NextLevel, LevelFont, UpgradeFont;
	
	//Time until nextlevel menu
	public float NextLevelSwitchTimer = 1f;
	
	//First tutorial (an array that holds the tutorial pages to display)
	public static TutorialPageBase[] firstTutorial;

	public static TutorialPageBase[] secondTutorial;
	
	public static TutorialPageBase[] thirdTutorial;
	//Tutorial handler
	static public TutorialHandler tutorialHandler;
	
	//Ad/play score interface
	public static GoogleInterface googleInterface;

	//HttpShit
	public static HttpConnection httpConnection;

	public static HttpContent httpContent;


	public static String previousState;
	
	
	public GameWorld(IabInterface aInt, GoogleInterface googleInterface) {
		
		this.googleInterface = googleInterface;

		/*if (Assetloader.screenHeight > 1280) {
			//Assetloader.loadLargeTextures();
			Assetloader.loadTextureAtlas("data/Large Atlas/BG/MainBGLarge");
		} else {
			//Assetloader.loadSmallTextures();

			Assetloader.loadTextureAtlas("data/Small Atlas/BG/MainBGSmall");


		}*/

		/*
		Main = Assetloader.CreateFont(Assetloader.screenWidth / 8f, "MainWiz", Assetloader.fonts.get("TapWizFont"), Color.WHITE);
		NextLevel = Assetloader.CreateFont(Assetloader.screenWidth / 6.5f, "NextLevel", Assetloader.fonts.get("TapWizFont"), Color.WHITE);
		LevelFont = Assetloader.CreateFont(Assetloader.screenWidth / 20f, "ShopLevelFont", Assetloader.fonts.get("TapWizFont"), Color.WHITE);

		UpgradeFont = Assetloader.CreateFont(Assetloader.screenWidth / 14f,"ShopUpgradeFont", Assetloader.fonts.get("TapWizFont"), Color.WHITE);
		*/

		this.platformInterface = aInt;


		InterstitialTimer = new Timer();
		
		playerInfo = new PlayerInfo();
		
		//Initialize time
		time = System.nanoTime();
		seconds = time / 1000000000;
		minutes = seconds / 60;

		//confirmPopup = new ConfirmPopup();

		//Instantiate menus
		mainMenu = new MainMenu(this);

		//mainStage = new MainStage();

		//shopMenu = new ShopMenuWiz();

		/*levelSelectMenu = new LevelSelectMenuNEW(96);

		nextLevelMenu = new NextLevelMenu();

		gameOverMenu = new GameOverMenu();

		IAB = new StarballsIAB();

		settingsMenu = new SettingsMenu();

		pausedMenu = new PausedMenu();

		signInMenu = new SignInMenu();

		registrationMenu = new RegistrationMenu();*/


		//Add categories to shopmenu
		//shopmenu.addCategory(shopmenu.shopCategories);
		
		//Add shop buttons to First category of shopmenu

		state = new FSM(this);

		state.AddState("Exiting", new ExitingState());

		state.AddState("GameOver", new GameOverState());

		state.AddState("IAB", new IABState());

		state.AddState("LevelSelect", new LevelSelectState());

		state.AddState("Login", new LoginState());

		state.AddState("MainMenu", new MainMenuState());

		state.AddState("NextLevel", new NextLevelState());

		state.AddState("Paused", new PausedState());

		state.AddState("Playing", new PlayingState());

		state.AddState("Registration", new RegistrationState());

		state.AddState("Settings", new SettingsState());

		state.AddState("Shop", new ShopState());

		state.AddState("Tutorial", new TutorialState());

		state.changeState("MainMenu");

		saveLoader = new SaveLoader(this);

		//Load player info
		saveLoader.LoadPlayerInfo(playerInfo);

		//saveLoader.Load(shopmenu);
		
		//shopmenu.shopCategories[0][0].Purchased = true;

		//HttpShit

		httpConnection = new HttpConnection("http://tallraccoongames.com/login.php");

		//httpSaveGameConnection = new HttpConnection("http://192.168.1.208/TapWizardSaves.php");

		httpContent = new HttpContent();
		
		//Start threads
		//musichandler.start(

		//Make first level tutorial
		firstTutorial = new TutorialPageBase[7];

		firstTutorial[0] = new TutorialWindSpell();

		firstTutorial[1] = new TutorialFireSpell();

		firstTutorial[2] = new TutorialEarthSpell();

		firstTutorial[3] = new TutorialIceSpell();

		firstTutorial[4] = new TutorialManaBar();

		firstTutorial[5] = new TutorialChooseSpell();

		firstTutorial[6] = new TutorialTouchAnywhere();

		//Make second level tutorial
		secondTutorial = new TutorialPageBase[1];

		secondTutorial[0] = new TutorialSlimeTypes();

		//Make third tutorial
		thirdTutorial = new TutorialPageBase[1];

		thirdTutorial[0] = new TutorialShop();

		//Make tutorial handler
		tutorialHandler = new TutorialHandler();


	}

	public void update(float deltaTime) {
		//update world stuff logic (objects, maps, etc)

		/*if (!Assetloader.bIsLoaded) {

			if (Assetloader.manager.update()) {

				if (Assetloader.screenHeight > 1280) {
					//Assetloader.loadLargeTextures();

					Assetloader.createSpritesFromAtlasBG("MainBGAtlas", "data/Large Atlas/BG/MainBGLarge");

				} else {
					//Assetloader.loadSmallTextures();


					Assetloader.createSpritesFromAtlasBG("MainBGAtlas", "data/Small Atlas/BG/MainBGSmall");


				}

				//Make first level tutorial
				firstTutorial = new TutorialPageBase[7];

				firstTutorial[0] = new TutorialWindSpell();

				firstTutorial[1] = new TutorialFireSpell();

				firstTutorial[2] = new TutorialEarthSpell();

				firstTutorial[3] = new TutorialIceSpell();

				firstTutorial[4] = new TutorialManaBar();

				firstTutorial[5] = new TutorialChooseSpell();

				firstTutorial[6] = new TutorialTouchAnywhere();

				//Make second level tutorial
				secondTutorial = new TutorialPageBase[1];

				secondTutorial[0] = new TutorialSlimeTypes();

				//Make third tutorial
				thirdTutorial = new TutorialPageBase[1];

				thirdTutorial[0] = new TutorialShop();

				//Make tutorial handler
				tutorialHandler = new TutorialHandler();


				Assetloader.bIsLoaded = true;

			}

		}*/
		
 		this.deltaTime = deltaTime;
 		
 		audioManager.Update();
 		
 		//Update time while app is running
 		time = System.nanoTime();
		seconds = time / 1000000000;
		minutes = seconds / 60;
 		
 		state.Update(this);
 		
 		for (int i = 0; particleEmitterArray.size() > i; i++) {
			
			if (particleEmitterArray.get(i) != null) {
				if (particleEmitterArray.get(i).isDone()) {
					particleEmitterArray.remove(i);
				} else {
					particleEmitterArray.get(i).update(deltaTime);
				}
			}
			}
		
	} 
	
	public MainStage getLevel() {
		return mainStage;
	}

	public GameWorld getWorld() {

		return this;

	}
	
	public void dispose() {
		
		if (!Dispose) {

			if (audioManager != null)
			audioManager.dispose();
			
			if (mainStage != null)
				mainStage.dispose();


			tutorialHandler.dispose();

			firstTutorial = null;

			secondTutorial = null;

			thirdTutorial = null;

			state = null;

			httpConnection = null;

			httpContent = null;

			if (mainMenu != null)
				mainMenu.dispose();

			if (levelSelectMenu != null)
				levelSelectMenu.dispose();

			if (nextLevelMenu != null)
				nextLevelMenu.dispose();

			if (gameOverMenu != null)
				gameOverMenu.dispose();

			if (shopMenu != null)
				shopMenu.dispose();

			if (IAB != null)
				IAB.dispose();

			if (settingsMenu != null)
				settingsMenu.dispose();

			if (pausedMenu != null)
				pausedMenu.dispose();

			if (signInMenu != null)
				signInMenu.dispose();

			if (registrationMenu != null)
				registrationMenu.dispose();

			
			for (int i = 0; i < particleEmitterArray.size(); i++) {
				
				if (particleEmitterArray.get(i) != null) {
					
					particleEmitterArray.get(i).dispose();
					
				}
				
			}
			
			Dispose = true;
			
		}
		
	}

}
