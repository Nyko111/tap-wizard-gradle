package com.nyko111.GameWorld;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.nyko111.Spells.SpellBase.SpellType;
import com.nyko111.StatusEffects.StatusEffect;
import com.nykoengine.Helpers.Assetloader;
import com.nykoengine.UserInterface.GameButton;

public class SpellSelector { 
	
	MainStage mainStage;

	GameButton AirSpell, FireSpell, EarthSpell, WaterSpell; 
	
	float ManaCost = 6;
	
	final GameButton[] selectedSpells = new GameButton[4];
	
	public SpellType selectedSpell = SpellType.Fire;
	
	public StatusEffect selectedStatusEffect = null;
	
	public Color damagedColor = Color.RED; 
	
	Vector2[] selectedSpellsPos;

	GameWorld world;
	
	//public Sprite selectedSpellSprite = Assetloader.allSpritesMap.get("IceSpell");
	
	
	public SpellSelector(final GameWorld world) {

		this.world = world;
		
		selectedSpellsPos = new Vector2[4];
		
		for (int i = 0; selectedSpellsPos.length > i; i++) {
			
			selectedSpellsPos[i] = new Vector2();
			
			selectedSpellsPos[i].y = Assetloader.screenHeight / 19f;			
		
		}
		
		selectedSpellsPos[0].x = 0f;//Assetloader.screenWidth / 8f;
		selectedSpellsPos[0].y = 0f;
		
		selectedSpellsPos[1].x = Assetloader.screenWidth / 6.1f;
		selectedSpellsPos[1].y = Assetloader.screenHeight / 16f;
		
		selectedSpellsPos[2].x = Assetloader.screenWidth / 1.54f;
		selectedSpellsPos[2].y = Assetloader.screenHeight / 16f;
		
		selectedSpellsPos[3].x = Assetloader.screenWidth / 1.25f;
		selectedSpellsPos[3].y = 0;
		
		AirSpell = new GameButton(selectedSpellsPos[0].x, selectedSpellsPos[0].y, Assetloader.allSpritesMap.get("AirButton"), Assetloader.allSpritesMap.get("AirButtonPressed"), Assetloader.sounds.get("buttonclick"), true);
		
		AirSpell.addListener(new InputListener(){
			 public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				 	
				 	//change all other buttons to sprite 1 (so only this one shows lit up sprite)
				 	for (int i = 0; selectedSpells.length > i; i++) {
				 		selectedSpells[i].Toggle = false;
				 
				 	}
				 	
				 	selectedSpells[0].Pushed();
				 	
				 	ManaCost = 4;
				 	selectedSpell = SpellType.Air;
				 	//selectedStatusEffect = null;
				 	//selectedSpellSprite = Assetloader.allSpritesMap.get("AirSpell");
				 	damagedColor = Color.BLUE;
				 	
				 	world.mainStage.spellButtonEmitter.setPosition(AirSpell.getX(), AirSpell.getY());
				 	world.mainStage.spellButtonEmitter.addParticle();
				 	world.mainStage.spellButtonEmitter.addParticle();
				 	world.mainStage.spellButtonEmitter.addParticle();
	            	
	                return true;
	            }
	            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
	            	selectedSpells[0].pushed = false;
	            	
	            	//AirSpell.Toggle();
	            
	            }
	        });
		
			FireSpell = new GameButton(selectedSpellsPos[1].x, selectedSpellsPos[1].y, Assetloader.allSpritesMap.get("FireButton"), Assetloader.allSpritesMap.get("FireButtonPressed"), Assetloader.sounds.get("buttonclick"), true);
			
			FireSpell.addListener(new InputListener(){
				 public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
					
					 	//change all other buttons to sprite 1 (so only this one shows lit up sprite)
					 	for (int i = 0; selectedSpells.length > i; i++) {
					 		selectedSpells[i].Toggle = false;
					 
					 	}
					 
					 	selectedSpells[1].Pushed();
		   
					 	ManaCost = 6;
					 	
					 	selectedSpell = SpellType.Fire;
					 	//selectedStatusEffect = null;
					 	//selectedSpellSprite = Assetloader.allSpritesMap.get("FireSpell");
					 	damagedColor = Color.RED;
					 	
					 	world.mainStage.spellButtonEmitter.setPosition(FireSpell.getX(), FireSpell.getY());
					 	world.mainStage.spellButtonEmitter.addParticle();
					 	world.mainStage.spellButtonEmitter.addParticle();
					 	world.mainStage.spellButtonEmitter.addParticle();
		            	
		                return true;
		            }
		            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
		            	selectedSpells[1].pushed = false;
		            	
		            	//FireSpell.Toggle();
		            
		            }
		        });
	
			
			EarthSpell = new GameButton(selectedSpellsPos[2].x, selectedSpellsPos[2].y, Assetloader.allSpritesMap.get("EarthButton"), Assetloader.allSpritesMap.get("EarthButtonPressed"), Assetloader.sounds.get("buttonclick"), true);
			
			EarthSpell.addListener(new InputListener(){
				 public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
					 	
					 	//change all other buttons to sprite 1 (so only this one shows lit up sprite)
					 	for (int i = 0; selectedSpells.length > i; i++) {
					 		selectedSpells[i].Toggle = false;
					 
					 	}
					 
					 	selectedSpells[2].Pushed();
					 	
					 	ManaCost = 20;
		   
					 	selectedSpell = SpellType.Earth;
					 	
					 	//selectedStatusEffect = new ChangeColorEffect();
					 	//selectedStatusEffect = null;
					 	//selectedSpellSprite = Assetloader.allSpritesMap.get("EarthSpell");
					 	damagedColor = Color.GREEN;
					 	
					 	world.mainStage.spellButtonEmitter.setPosition(EarthSpell.getX(), EarthSpell.getY());
					 	world.mainStage.spellButtonEmitter.addParticle();
					 	world.mainStage.spellButtonEmitter.addParticle();
					 	world.mainStage.spellButtonEmitter.addParticle();
		            	
		                return true;
		            }
		            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
		            	selectedSpells[2].pushed = false;
		            	
		            	
		            
		            }
		        });
			
			WaterSpell = new GameButton(selectedSpellsPos[3].x, selectedSpellsPos[3].y, Assetloader.allSpritesMap.get("IceButton"), Assetloader.allSpritesMap.get("IceButtonPressed"), Assetloader.sounds.get("buttonclick"), true);
			
			WaterSpell.addListener(new InputListener(){
				 public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
						
					 	//change all other buttons to sprite 1 (so only this one shows lit up sprite)
						for (int i = 0; selectedSpells.length > i; i++) {
					 		selectedSpells[i].Toggle = false;
					 
					 	}
					 	
						selectedSpells[3].Pushed();
		   
						ManaCost = 8;
							
					 	selectedSpell = SpellType.Water;
					 	//selectedStatusEffect = new WaterEffect();
					 	//statusEffect = StatusEffect.Water;
					 	//selectedSpellSprite = Assetloader.allSpritesMap.get("IceSpell");
					 	damagedColor = Color.CYAN;
					 	
					 	world.mainStage.spellButtonEmitter.setPosition(WaterSpell.getX(), WaterSpell.getY());
					 	world.mainStage.spellButtonEmitter.addParticle();
					 	world.mainStage.spellButtonEmitter.addParticle();
					 	world.mainStage.spellButtonEmitter.addParticle();
					 	
					 	//projectileEffect = new SplashDamage();
		            	
		                return true;
		            }
		            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
		            	selectedSpells[3].pushed = false;
		            	
		            	
		            
		            }
		        });
			
			//DebugTemp
			selectedSpells[0] = AirSpell;
			selectedSpells[1] = FireSpell;
			selectedSpells[2] = EarthSpell;
			selectedSpells[3] = WaterSpell;
			
			//selectedSpells[1].Pushed();
			//selectedSpells[1].pushed = false;
			
			//world.mainStage.spellButtonEmitter.setPosition(FireSpell.getX(), FireSpell.getY());
	}
	
	public GameButton[] Spells() {
		
		return selectedSpells;
		
	}
	
	public void dispose() {
		
		selectedSpells[0].dispose();
		selectedSpells[1].dispose();
		selectedSpells[2].dispose();
		selectedSpells[3].dispose();
	}
	
	
}

	



