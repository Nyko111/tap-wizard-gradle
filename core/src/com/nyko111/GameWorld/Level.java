package com.nyko111.GameWorld;

import com.nykoengine.Helpers.Assetloader;

public class Level {

	int Slimes, IceSlimes, FireSlimes;
	
	int BossSlimes, IceBossSlimes, FireBossSlimes;
	
	int MaxScore;
	
	public Boolean isLocked = true;
	
	public Level(int LevelNumber) {

		Slimes = 0;

		IceSlimes = 0;

		FireSlimes = 0;

		int SlimeAmount = 3 + (int)(LevelNumber * 1.3f);

		for (int i = 0; i < SlimeAmount; i++) {

			switch (Assetloader.rand.nextInt(3)) {
				case 1:
					Slimes += 1;
					break;
				case 2:
					IceSlimes += 1;
					break;
				case 3:
					FireSlimes += 1;
					break;
				default:
					Slimes += 1;
					break;

			}



		}
		
		//Boss Slimes
		if (LevelNumber % 10 == 0 && LevelNumber != 0) {

			switch (Assetloader.rand.nextInt(3)) {
				case 1:
					BossSlimes = 1;
					break;
				case 2:
					IceBossSlimes = 1;
					break;
				case 3:
					FireBossSlimes = 1;
					break;
				default:
					BossSlimes = 1;
					break;

			}

		} else {

			BossSlimes = 0;
			IceBossSlimes = 0;
			FireBossSlimes = 0;
		}
		
		CalculateScore(Slimes + IceSlimes + FireSlimes + BossSlimes + IceBossSlimes + FireBossSlimes);
		
	}
	
	public void CalculateScore(int EnemyCount) {
		
		int Multiplyer = 1;
		
		for (int i = 0; i < EnemyCount; i++) {
			
			MaxScore += Multiplyer;
			
			Multiplyer = Multiplyer * 2;
			
		}
		
	}
	
	public void Start(GameWorld world) {
		
		if (world.CurrentLevel < 10) {
			
			world.mainStage.MaxEnemies = 5;
			
		} else if (world.CurrentLevel < 30) {
			
			world.mainStage.MaxEnemies = 7;
			
		} else if (world.CurrentLevel >= 30) {
			
			world.mainStage.MaxEnemies = 10;
			
		}
		
		world.mainStage.spawnList.Slimes = Slimes;
		
		world.mainStage.spawnList.IceSlimes = IceSlimes;
		
		world.mainStage.spawnList.FireSlimes = FireSlimes;
		
		world.mainStage.spawnList.BossSlimes = BossSlimes;
		
		world.mainStage.spawnList.IceBossSlimes = IceBossSlimes;
		
		world.mainStage.spawnList.FireBossSlimes = FireBossSlimes;
		
		world.mainStage.Score = 0;
		
		world.mainStage.TopScore = MaxScore;
		world.mainStage.wall.Health = 4;
		
	}
	
}
