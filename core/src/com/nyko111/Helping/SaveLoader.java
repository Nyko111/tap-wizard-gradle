package com.nyko111.Helping;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.nyko111.GameWorld.GameWorld;
import com.nyko111.GameWorld.PlayerInfo;
import com.nyko111.Menus.ShopMenuWiz;


public class SaveLoader {

	GameWorld world;
	static Preferences prefs;
	
	public SaveLoader(GameWorld world) {
		prefs = Gdx.app.getPreferences("Tap Wizard");
		this.world = world;
		
	}
	
	public void SavePlayerInfo(PlayerInfo playerInfo) {

		prefs.putInteger("CompletedTutorial", playerInfo.CompletedTutorial);
	
		prefs.putInteger("Money", playerInfo.Money);
		
		prefs.putInteger("IceLevel", playerInfo.IceLevel);
		
		prefs.putInteger("FireLevel", playerInfo.FireLevel);
		
		prefs.putInteger("EarthLevel", playerInfo.EarthLevel);
		
		prefs.putInteger("WindLevel", playerInfo.WindLevel);
		
		prefs.putInteger("StarBallDropLevel", playerInfo.StarBallDropLevel);
		
		prefs.putInteger("UnlockedLevels", world.UnlockedLevels);

		prefs.putBoolean("ShowAds", world.bShowAds);

		prefs.flush();
	}
	
	public void LoadPlayerInfo(PlayerInfo playerInfo) {

		world.bShowAds = prefs.getBoolean("ShowAds", true);
		
		playerInfo.CompletedTutorial = prefs.getInteger("CompletedTutorial", 0);
		
		playerInfo.Money = prefs.getInteger("Money", 20);
		
		playerInfo.IceLevel = prefs.getInteger("IceLevel", 1);
		
		playerInfo.FireLevel = prefs.getInteger("FireLevel", 1);
		
		playerInfo.EarthLevel = prefs.getInteger("EarthLevel", 1);
		
		playerInfo.WindLevel = prefs.getInteger("WindLevel", 1);
		
		playerInfo.StarBallDropLevel = prefs.getInteger("StarBallDropLevel", 1);
		
		world.UnlockedLevels = prefs.getInteger("UnlockedLevels", 1);
	
	}
	
	//public void Save(ShopMenuWiz shopmenu) {
		
		//DEBUG
		//prefs.putInteger("Money", world.shopmenu.Money);
		//DEBUG
		
		//prefs.putString("penColor", world.mainStage.hookahPen.penColor.toString());
		
		//for (int i = 0; i < shopmenu.shopCategories.length; i++) {
			//if ( shopmenu.shopCategories[i] != null) {
				//for (int y = 0; y < shopmenu.shopCategories[i].length; y++) {
					//if ( shopmenu.shopCategories[i][y] != null)
					//prefs.putBoolean("shopButtonColor" + y, shopmenu.shopCategories[i][y].Purchased);
				//}
			//}
		//}
		
		//prefs.flush();
	//}
	
	public void Load(ShopMenuWiz shopmenu) {
		
		//HighScore lodaing
		/*world.shopmenu.Money = prefs.getInteger("Money", 0);
		
		if (prefs.getString("penColor") != null)
			world.mainStage.hookahPen.loadColor(prefs.getString("penColor", Color.BLACK.toString()));
		else 
			world.mainStage.hookahPen.loadColor(Color.BLACK.toString());
		
		
		for (int i = 0; i < shopmenu.shopCategories.length; i++ )
			if (shopmenu.shopCategories[i] != null) {
				for (int y = 0; y < shopmenu.shopCategories[i].length; y++) {
			
					if (shopmenu.shopCategories[i][y] != null)
						shopmenu.shopCategories[i][y].Purchased = prefs.getBoolean("shopButtonColor" + y, false);
					
				}
		}*/
		//world.TutPart = prefs.getInteger("Tutorial", 1);
	}
	

}
