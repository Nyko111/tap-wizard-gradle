package com.nyko111.Spells;

import java.util.List;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Vector2;
import com.nyko111.Actors.Enemy;
import com.nyko111.GameWorld.GameWorld;
import com.nyko111.GameWorld.MainStage;
import com.nyko111.Spells.SpellBase.SpellType;
import com.nyko111.StatusEffects.StatusEffect;
import com.nykoengine.Audio.AudioManager;
import com.nykoengine.Helpers.Assetloader;
import com.nykoengine.ParticleEngine.Emitter;

public class EarthSpell extends SpellBase {
	
	Vector2 particleMovement = new Vector2();
	
	Vector2 shadowPos = new Vector2();
	
	public Vector2 MiddlePos = new Vector2();
	
	public Sprite ShadowSprite;
	
	float ShadowScale = 0.5f, SpellScale = 1.2f;
	
	public EarthSpell(float Damage, Sprite SpellSprite, Sprite ShadowSprite, Vector2 TargetPos, SpellType spellType, StatusEffect statusEffect, Color damagedColor, GameWorld world) {
		
		super(Damage, Assetloader.allSpritesMap.get("EarthSpell"), TargetPos, spellType, statusEffect, damagedColor, world);
		
		particleMovement.x = Assetloader.screenWidth / 2;
		
		this.ShadowSprite = ShadowSprite;
		
		particleMovement.y = Assetloader.screenWidth / 4;
		
		Position.x = TargetPos.x - ((SpellSprite.getWidth() * Assetloader.gameScaleX) / 2);  
		
		Position.y = TargetPos.y + Assetloader.screenWidth / 2.66f;
		
		shadowPos.x = TargetPos.x - ((ShadowSprite.getWidth() * Assetloader.gameScaleX) / 2);
		
		shadowPos.y = Position.y - Assetloader.screenHeight / 3.6f;
		
		this.hitBox.setBounds(hitBox.getBounds().width * 4, hitBox.getBounds().height * 4);
		
		velocity.set(0, TargetPos.y - Position.y).nor().scl(Math.min(Position.dst(0, TargetPos.y), Assetloader.screenWidth / 2));
		
		//Assetloader.sounds.get("EarthFalling").play(GameWorld.audioManager.SoundVolume);
	}
	
	@Override 
	public void act(float delta) {
		
		//update the hitbox position
		hitBox.Update(MiddlePos.x - (hitBox.getBounds().width / 2), MiddlePos.y - (hitBox.getBounds().height / 2));
		
		ShadowScale += 0.5 * delta;
		
		SpellScale -= 0.5 * delta;
		
		this.MiddlePos.x = (((SpellSprite.getWidth() * SpellScale) / 2f) * Assetloader.gameScaleX) + Position.x;//this.getCenterX();
		this.MiddlePos.y = (((SpellSprite.getHeight() * SpellScale) / 2f) * Assetloader.gameScaleY) + Position.y;
	
		if (Position.y > TargetPos.y)
			Position.add(0, velocity.y * delta);
		else
			Hit(world.mainStage.enemyArray);
		
	}
	
	@Override
	public void draw(Batch batch, float alpha) {
		
		batch.draw(ShadowSprite, shadowPos.x, shadowPos.y, ShadowSprite.getWidth() / 2 * Assetloader.gameScaleX, ShadowSprite.getHeight() / 2 * Assetloader.gameScaleY, ShadowSprite.getWidth() * Assetloader.gameScaleX, ShadowSprite.getHeight() * Assetloader.gameScaleY, ShadowScale, ShadowScale, spriteRotation);
		
		batch.draw(SpellSprite, Position.x, Position.y, SpellSprite.getWidth() / 2 * Assetloader.gameScaleX, SpellSprite.getHeight() / 2 * Assetloader.gameScaleY, SpellSprite.getWidth() * Assetloader.gameScaleX, SpellSprite.getHeight() * Assetloader.gameScaleY, SpellScale, SpellScale, spriteRotation);

	}
	
	@Override
	public void Hit(List<Enemy> enemyArray) {
		
		for (int i = 0; i < enemyArray.size(); i++) {
			if (enemyArray.get(i) != null) {
				if (Intersector.overlaps(this.hitBox.getBounds(), enemyArray.get(i).hitbox.getBounds())) {
					
					enemyArray.get(i).Hit(damagedColor, Damage, spellType, statusEffect);
				
				}
			}	
		}
		
		//Creates emitter
		Emitter emitter = new Emitter(Assetloader.allSpritesMap.get("EarthParticle"), null,	MiddlePos, particleMovement.cpy(), 25, .3f, .25f, 5, 1);
		
		emitter.setRandDirections(true, false);
	
		GameWorld.particleEmitterArray.add(emitter);
		
		Assetloader.sounds.get("EarthSpell").play(AudioManager.SoundVolume);
		
		spellUsed = true;
		
		this.remove();
		
	}
	

}
