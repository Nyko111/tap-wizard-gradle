package com.nyko111.Spells;

import java.util.List;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.nyko111.Actors.Enemy;
import com.nyko111.GameWorld.GameWorld;
import com.nyko111.StatusEffects.StatusEffect;
import com.nykoengine.Detection.HitBox;
import com.nykoengine.Helpers.Assetloader;

public class SpellBase extends Actor {
	
	public float Damage = 0;
	
	public float spriteRotation = 0;
	
	public Vector2 Position = new Vector2();
	
	Vector2 velocity = new Vector2();
	
	public boolean spellUsed = false;
	
	public Color damagedColor = Color.WHITE;
	
	public Sprite SpellSprite;
	
	public Vector2 TargetPos;
	
	public HitBox hitBox;
	
	public enum SpellType {Fire, Water, Earth, Air, Default};
	
	public SpellType spellType;
	
	public StatusEffect statusEffect;
	
	public Vector2 MiddlePos = new Vector2();
	
	Sprite particleSprite, additiveAlpha;

	public GameWorld world;
	
	public SpellBase(float Damage, Sprite SpellSprite, Vector2 TargetPos, SpellType spellType, StatusEffect statusEffect, Color damagedColor, GameWorld world) {

		this.world = world;

		this.Damage = Damage;
		
		this.SpellSprite = SpellSprite;
		
		this.TargetPos = TargetPos;
		
		this.spellType = spellType;
		
		this.statusEffect = statusEffect;
		
		this.damagedColor = damagedColor;
		
		this.MiddlePos.x = (((SpellSprite.getWidth()) / 2f) * Assetloader.gameScaleX) + Position.x;//this.getCenterX();
		this.MiddlePos.y = (((SpellSprite.getHeight()) / 2f) * Assetloader.gameScaleY) + Position.y;
		
		hitBox = new HitBox(Position.x - ((SpellSprite.getWidth() / 2) * Assetloader.gameScaleX), Position.y - ((SpellSprite.getHeight() / 2) * Assetloader.gameScaleY), SpellSprite.getWidth() * Assetloader.gameScaleX, SpellSprite.getHeight() * Assetloader.gameScaleY);
		
		this.hitBox.setBounds(hitBox.getBounds().width / 2, hitBox.getBounds().height / 2);
	}

	public void Hit(List<Enemy> enemyArray) {
		
		
	}
	
	@Override
	public void draw(Batch batch, float Alpha) {
		
		
		
	}
	
	public void setParticleSprites(Sprite particleSprite, Sprite additiveAlpha) {
		
		this.particleSprite = particleSprite;
		
		this.additiveAlpha = additiveAlpha;
		
	}
	
}
