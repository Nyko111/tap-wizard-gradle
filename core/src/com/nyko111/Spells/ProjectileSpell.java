package com.nyko111.Spells;

import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Vector2;
import com.nyko111.Actors.Enemy;
import com.nyko111.GameWorld.GameWorld;
import com.nyko111.GameWorld.MainStage;
import com.nyko111.Spells.SpellBase.SpellType;
import com.nyko111.StatusEffects.StatusEffect;
import com.nykoengine.Helpers.Assetloader;
import com.nykoengine.ParticleEngine.Emitter;

public class ProjectileSpell extends SpellBase {
	
	List<Enemy> enemyArray;
	
	public Sprite spellAlpha;
	
	float AlphaPulse = 1f;
	
	boolean PulseIncrease = false;
	
	public ProjectileSpell(float Damage, Sprite SpellSprite, Sprite spellAlpha, Vector2 TargetPos, SpellType spellType, StatusEffect statusEffect, Color damagedColor, List<Enemy> enemyArray, GameWorld world) {
		
		super(Damage, SpellSprite, TargetPos, spellType, statusEffect, damagedColor, world);
		
		this.enemyArray = enemyArray;
		
		this.spellAlpha = spellAlpha;
		
		Position.x = (Assetloader.screenWidth / 2) - ((SpellSprite.getWidth() / 2) * Assetloader.gameScaleX);  
		
		Position.y = Assetloader.screenHeight / 8f;
		
		this.TargetPos = TargetPos;
																																//Max velocity
		//velocity.set(TargetPos.x - Position.x, TargetPos.y - Position.y).nor().scl(Math.min(Position.dst(TargetPos.x, TargetPos.y), 800f));
		if (TargetPos.x == Gdx.graphics.getWidth() / 2) {

			velocity.set(0, TargetPos.y - Position.y).nor().scl(Math.min(Assetloader.screenWidth, Assetloader.screenWidth));


		} else {

			velocity.set(TargetPos.x - Position.x, TargetPos.y - Position.y).nor().scl(Math.min(Assetloader.screenWidth, Assetloader.screenWidth));

		}
	}
	
	@Override
	public void act(float deltaTime) {

		if (AlphaPulse > 1f || AlphaPulse < .5f) {
			
			PulseIncrease = !PulseIncrease;
			
		}
		
		if (!PulseIncrease) {
			
			AlphaPulse -= deltaTime;
			
		} else {
			
			AlphaPulse += deltaTime;
			
		} 
		
		
		//update the hitbox position
		hitBox.Update(Position.x + (hitBox.getBounds().width / 2), Position.y + (hitBox.getBounds().height / 2));
		
		for (int i = 0; i < enemyArray.size(); i++) {
			
			if (enemyArray.get(i) != null && !enemyArray.get(i).isDead && Intersector.overlaps(this.hitBox.getBounds(), enemyArray.get(i).hitbox.getBounds())) {
				
					
				//Creates emitter
				
				if (particleSprite != null) {
					
				Emitter emitter = new Emitter(particleSprite, additiveAlpha, enemyArray.get(i).MiddlePos, enemyArray.get(i).particleMovement.cpy(), 0, .3f, .25f, 3, 1);
					
				emitter.setRandDirections(true, true);
				
				GameWorld.particleEmitterArray.add(emitter);
				
				}
					
				//Hit enemy
				enemyArray.get(i).Hit(damagedColor, Damage, spellType, statusEffect);	
					
				Hit(world.mainStage.enemyArray);
			
		}
			
			
				
					
	}	
			
	
       
            
		Position.add(velocity.x * deltaTime, velocity.y * deltaTime);
        spriteRotation = velocity.angle() - 90;
        
    	if (Position.y <= 0 || Position.x >= Assetloader.screenWidth || Position.x < 0) {
		
			spellUsed = true;
		
			this.remove();
    	}
		
	}
	
	@Override
	public void draw(Batch batch, float alpha) {
		
		if (spellAlpha != null) {
			
			batch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE);
			
			batch.setColor(Color.WHITE.r,Color.WHITE.g, Color.WHITE.b, AlphaPulse);
			
			batch.draw(spellAlpha, Position.x, Position.y, spellAlpha.getWidth() / 2 * Assetloader.gameScaleX, spellAlpha.getHeight() / 2 * Assetloader.gameScaleY, spellAlpha.getWidth() * Assetloader.gameScaleX, spellAlpha.getHeight() * Assetloader.gameScaleY, 1, 1, spriteRotation);
		
			batch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
			
			//reset color
			batch.setColor(Color.WHITE.r,Color.WHITE.g, Color.WHITE.b, 1);
		}
		
		batch.draw(SpellSprite, Position.x, Position.y, SpellSprite.getWidth() / 2 * Assetloader.gameScaleX, SpellSprite.getHeight() / 2 * Assetloader.gameScaleY, SpellSprite.getWidth() * Assetloader.gameScaleX, SpellSprite.getHeight() * Assetloader.gameScaleY, 1, 1, spriteRotation);
	
	}
	
	public void Hit(List<Enemy> enemyArray) {
		
		spellUsed = true;
		
		this.remove();
		
	}

	
}
