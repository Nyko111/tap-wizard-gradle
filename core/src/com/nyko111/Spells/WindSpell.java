package com.nyko111.Spells;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.nyko111.Actors.Enemy;
import com.nyko111.GameWorld.GameWorld;
import com.nyko111.Spells.SpellBase.SpellType;
import com.nyko111.StatusEffects.StatusEffect;
import com.nykoengine.Helpers.Assetloader;

public class WindSpell extends ProjectileSpell {
	
	Vector2 position2, position3;
	
	float TimeLeft;

	GameWorld world;
	
	public ArrayList<Sprite> spriteArray = new ArrayList<Sprite>();

	public WindSpell(int Damage, Sprite SpellSprite, Sprite spellAlpha,
			Vector2 TargetPos, SpellType spellType, StatusEffect statusEffect,
			Color damagedColor, List<Enemy> enemyArray, GameWorld world) {
		super(Damage, SpellSprite, spellAlpha, TargetPos, spellType, statusEffect,
				damagedColor, enemyArray, world);
		// TODO Auto-generated constructor stub
		
		//TargetPos.x = Position.x;

		this.world = world;
		
		Position.x = TargetPos.x;
		
		position2 = Position.cpy();
		
		spriteArray.add(new Sprite(SpellSprite));
		
		//spriteArray.get(0).setScale(Assetloader.gameScaleX, Assetloader.gameScaleY);
		
		if (spriteArray.get(0).getX() + Assetloader.screenWidth / 5f >= Assetloader.screenWidth) {
			
			spriteArray.get(0).setX(Assetloader.screenWidth - (SpellSprite.getWidth() * Assetloader.gameScaleX));
			
		} else if (spriteArray.get(0).getX() - Assetloader.screenWidth / 5f <= 0) {
		
			spriteArray.get(0).setX(spriteArray.get(0).getX() + (SpellSprite.getWidth() * Assetloader.gameScaleX));
			
		}/* else {
			
			spriteArray.get(0).setX(Position.x + Assetloader.screenWidth / 5f);
			
		} */
		
		

		velocity.set(0, TargetPos.y).nor().scl(Math.min(Assetloader.screenWidth * 2f, Assetloader.screenWidth * 2f));
		
		TimeLeft = .5f + (0.01f * world.playerInfo.WindLevel);
	}

	@Override 
	public void act(float deltaTime) {
		
		for (int i = 0; i < spriteArray.size(); i++) {
			
			spriteArray.get(i).setX(spriteArray.get(i).getX() + (velocity.x * deltaTime));
			spriteArray.get(i).setY(spriteArray.get(i).getY() + (velocity.y * deltaTime));
		
		}
		
		if (TimeLeft > 0) {
		
			if (spriteArray.get(spriteArray.size() - 1).getY() > (spriteArray.get(spriteArray.size() - 1).getHeight() * Assetloader.gameScaleY) / 2) {
			
				spriteArray.add(new Sprite(SpellSprite));
			
				//spriteArray.get(spriteArray.size() - 1).setScale(Assetloader.gameScaleX, Assetloader.gameScaleY);
			
				spriteArray.get(spriteArray.size() - 1).setX(Assetloader.rand.nextInt((int) (Assetloader.screenWidth - (SpellSprite.getWidth() * Assetloader.gameScaleX))));
		
			}
	
			for (int i = 0; i < enemyArray.size(); i++) {
			
				if (enemyArray.get(i).WalkSpeed > 0)
					enemyArray.get(i).WalkSpeed = -(enemyArray.get(i).WalkSpeed / (2 - (0.01f * world.playerInfo.WindLevel)));
			
			}
    	
			TimeLeft -= deltaTime;
		}
		
		if (TimeLeft <= 0 && spriteArray.get(spriteArray.size() - 1).getY() > Assetloader.screenHeight + SpellSprite.getHeight()) {
    		
			spellUsed = true;
		
			this.remove();
    		
    	}
    	
    	
		
	}
	
	@Override
	public void draw(Batch batch, float alpha) {
		
		for (int i = 0; i < spriteArray.size(); i++) {
			
			batch.draw(spriteArray.get(i), spriteArray.get(i).getX(), spriteArray.get(i).getY(), spriteArray.get(i).getWidth() * Assetloader.gameScaleX, spriteArray.get(i).getHeight() * Assetloader.gameScaleY);
			
			//spriteArray.get(i).draw(batch);
			
		}
		
		
	
	/*	if (spellAlpha != null) {
			
			batch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE);
			
			batch.setColor(Color.WHITE.r,Color.WHITE.g, Color.WHITE.b, AlphaPulse);
			
			batch.draw(spellAlpha, Position.x, Position.y, spellAlpha.getWidth() / 2 * Assetloader.gameScaleX, spellAlpha.getHeight() / 2 * Assetloader.gameScaleY, spellAlpha.getWidth() * Assetloader.gameScaleX, spellAlpha.getHeight() * Assetloader.gameScaleY, 1, 1, spriteRotation);
		
			batch.draw(spellAlpha, position2.x, position2.y, spellAlpha.getWidth() / 2 * Assetloader.gameScaleX, spellAlpha.getHeight() / 2 * Assetloader.gameScaleY, spellAlpha.getWidth() * Assetloader.gameScaleX, spellAlpha.getHeight() * Assetloader.gameScaleY, 1, 1, spriteRotation);
			
			batch.draw(spellAlpha, position3.x, position3.y, spellAlpha.getWidth() / 2 * Assetloader.gameScaleX, spellAlpha.getHeight() / 2 * Assetloader.gameScaleY, spellAlpha.getWidth() * Assetloader.gameScaleX, spellAlpha.getHeight() * Assetloader.gameScaleY, 1, 1, spriteRotation);
			
			batch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
			
			//reset color
			batch.setColor(Color.WHITE.r,Color.WHITE.g, Color.WHITE.b, 1);
		}
		
		batch.draw(SpellSprite, Position.x, Position.y, SpellSprite.getWidth() / 2 * Assetloader.gameScaleX, SpellSprite.getHeight() / 2 * Assetloader.gameScaleY, SpellSprite.getWidth() * Assetloader.gameScaleX, SpellSprite.getHeight() * Assetloader.gameScaleY, 1, 1, spriteRotation);
		
		batch.draw(SpellSprite, position2.x, position2.y, SpellSprite.getWidth() / 2 * Assetloader.gameScaleX, SpellSprite.getHeight() / 2 * Assetloader.gameScaleY, SpellSprite.getWidth() * Assetloader.gameScaleX, SpellSprite.getHeight() * Assetloader.gameScaleY, 1, 1, spriteRotation);
		
		batch.draw(SpellSprite, position3.x, position3.y, SpellSprite.getWidth() / 2 * Assetloader.gameScaleX, SpellSprite.getHeight() / 2 * Assetloader.gameScaleY, SpellSprite.getWidth() * Assetloader.gameScaleX, SpellSprite.getHeight() * Assetloader.gameScaleY, 1, 1, spriteRotation);
	}*/
	
		
		
	}
	
	
}
