package com.nyko111.Interfaces;

public interface IabInterface {
	
	// (arbitrary) request code for the purchase flow     
    static final int RC_REQUEST = 10001;
	
	//Starts the transaction based on which item is selected
	public void buyStuff(int i);
	
	//Consumes the purchase
	public void consumeItem();

	//Restore old purchases
	public void restorePurchase();
	
}
