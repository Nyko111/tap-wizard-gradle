package com.nyko111.Interfaces;

public interface GoogleInterface {
	
	public void Login();
	public void LogOut();

	//get if client is signed in to Google+
	public boolean getSignedIn();

	//submit a score to a leaderboard
	public void submitScore(int score);

	//gets the scores and displays them threw googles default widget
	public void getScores();

	//Turns ads on and off
	public void adActive(Boolean bActive);
	
	//Opens an interstitial (full screen) ad
	public void Interstitial();
		
	//Opens the ads that pay users to view
	public void PaidAd();
	
	public void PayAd();
}
