package com.nyko111.Actors;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Vector2;
import com.nyko111.GameWorld.GameWorld;
import com.nyko111.GameWorld.MainStage;
import com.nykoengine.Audio.AudioManager;
import com.nykoengine.Detection.HitBox;
import com.nykoengine.Helpers.Assetloader;

public class StarBall {
	
	Sprite StarBall;
	
	float Distance;
	
	Vector2 Position, StartingPos, Target;
	
	Vector2 Direction;
	
	float SpeedMult = 1;
	
	float xMovement;
	
	public HitBox hitbox;
	
	public boolean Moving = true;

	GameWorld world;

	public StarBall(Sprite StarBall, Vector2 Position, Vector2 Target, GameWorld world) {

		this.world = world;

		this.StarBall = StarBall;
		
		this.Position = Position;
		
		this.StartingPos = Position.cpy();
		
		this.Target = Target;
		
		Direction = new Vector2();
		
		//Distance = StartingPos.dst(Target);
		
		Direction.set(Target.x - StartingPos.x, Target.y - StartingPos.y).nor();
		
		xMovement = -Direction.cpy().x;
		
		hitbox = new HitBox(Position.x, Position.y, StarBall.getWidth() * Assetloader.gameScaleX, StarBall.getHeight() * Assetloader.gameScaleY);
		

		if (Target.dst(Position) <= Assetloader.screenHeight / 2) {
			
			SpeedMult = 2;
			
		}
	}
	
	public void update(float deltaTime) {
		
		if (Moving) {
			
		//Distance = Position.dst(Target);
			
		if (xMovement < Direction.x) {
				
			xMovement += deltaTime * SpeedMult;
				
		} else if (xMovement > Direction.x){
				
			xMovement -= deltaTime * SpeedMult;
				
		} else {
			
			xMovement = Direction.x;
			
		}
			
		Position.add((Direction.x * Assetloader.screenWidth) * deltaTime, (Direction.y * Assetloader.screenWidth) * deltaTime);
		
		hitbox.Update(Position.x, Position.y);
		
		if (Intersector.overlaps(hitbox.getBounds(), world.mainStage.wizard.hitbox.getBounds())) {
			
			Position = Target;
			
			world.playerInfo.Money += Assetloader.rand.nextInt(2) + 1;
			
			Assetloader.sounds.get("StarBall").play(AudioManager.SoundVolume);
			
			Moving = false;
			
		}
		
		}
		
		
	}
	
	public void draw(Batch batch, float Alpha) {
		
		batch.draw(StarBall, Position.x, Position.y, StarBall.getWidth() * Assetloader.gameScaleX, StarBall.getHeight() * Assetloader.gameScaleY);
		
		
	}

}
