package com.nyko111.Actors;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.nyko111.Actors.Enemy.Weakness;
import com.nyko111.GameWorld.GameWorld;
import com.nyko111.GameWorld.MainStage;
import com.nyko111.Helping.TapWizSpawnTypes;
import com.nykoengine.Animation.AnimNyko;
import com.nykoengine.Helpers.Assetloader;
import com.nykoengine.Helpers.SpawnInfo;

public class SpawnList extends SpawnInfo {
	
	MainStage mainStage;

	GameWorld world;
	
	public Boolean bStopSpawning = false;
	
	public int Slimes = 0;
	
	public int IceSlimes = 0;
	
	public int FireSlimes = 0;
	
	public int BossSlimes = 0;
	
	public int IceBossSlimes = 0;
	
	public int FireBossSlimes = 0;
	
	public AnimNyko slimeWalkAnim = new AnimNyko();
	

	public SpawnList(MainStage mainStage, GameWorld world) {
		
		this.mainStage = mainStage;

		this.world = world;
		//slimeWalkAnim.addFrame(Assetloader.allSpritesMap.get("SlimeWalk1"), 0.05f);
		//slimeWalkAnim.addFrame(Assetloader.allSpritesMap.get("SlimeWalk2"), 0.05f);
		//slimeWalkAnim.addFrame(Assetloader.allSpritesMap.get("SlimeWalk3"), 0.05f);
		//slimeWalkAnim.addFrame(Assetloader.allSpritesMap.get("SlimeWalk4"), 0.05f);
		//slimeWalkAnim.addFrame(Assetloader.allSpritesMap.get("SlimeWalk5"), 0.05f);
		//slimeWalkAnim.addFrame(Assetloader.allSpritesMap.get("SlimeWalk6"), 0.05f);
		//slimeWalkAnim.addFrame(Assetloader.allSpritesMap.get("SlimeWalk7"), 0.05f);
		//slimeWalkAnim.addFrame(Assetloader.allSpritesMap.get("SlimeWalk8"), 0.05f);
		//slimeWalkAnim.repeat = true;
		
	}
	
	
	@Override 
	public void Spawn(int spawnType, int Health, float X, float Y) {
		
	
		if (!world.bPaused) {
		
		if (spawnType == TapWizSpawnTypes.Slime && Slimes > 0){
			
			AnimNyko[] animArray = new AnimNyko[4];
			
			AnimNyko walkAnim = new AnimNyko();
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk01"), 0.05f);
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk02"), 0.05f);
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk03"), 0.05f);
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk04"), 0.05f);
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk05"), 0.05f);
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk06"), 0.05f);
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk07"), 0.05f);
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk08"), 0.05f);
			walkAnim.repeat = true;
			
			AnimNyko hurtAnim = new AnimNyko();
			
			hurtAnim.addFrame(Assetloader.allSpritesMap.get("slime_hit"), 1f);
			hurtAnim.repeat = true;
			
			animArray[0] = walkAnim;
			animArray[1] = hurtAnim;
			
		
			Slime newSlime = new Slime(10 + (world.CurrentLevel / 2f), X, Y, Assetloader.screenHeight / 5, animArray, Assetloader.allSpritesMap.get("BurstRing"), Weakness.Fire, world);
			
			newSlime.setScale(.8f);
			
			newSlime.addListener(new InputListener(){
				 public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
		            	
		                return true;
		            }
		            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
		            	
		            	//Spell type
		            	//mainStage.addSpell(newSlime, mainStage.spellSelector.selectedSpellSprite);
		            	
		            	//Tap type
		            	//newSlime.Hit(mainStage.spellSelector.damagedColor, 1, mainStage.spellSelector.selectedSpell, mainStage.spellSelector.selectedStatusEffect);
		            	
		            	//SpawnEnemy();
		            }
		        });
			
			Slimes -= 1;
			
			mainStage.enemyArray.add(newSlime);
			
			mainStage.addActor(mainStage.enemyArray.get(mainStage.enemyArray.size() - 1));
		
		} else if (spawnType == TapWizSpawnTypes.IceSlime && IceSlimes > 0){
			
			AnimNyko[] animArray = new AnimNyko[4];
			
			AnimNyko walkAnim = new AnimNyko();
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk01"), 0.05f);
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk02"), 0.05f);
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk03"), 0.05f);
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk04"), 0.05f);
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk05"), 0.05f);
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk06"), 0.05f);
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk07"), 0.05f);
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk08"), 0.05f);
			walkAnim.repeat = true;
			
			AnimNyko hurtAnim = new AnimNyko();
			
			hurtAnim.addFrame(Assetloader.allSpritesMap.get("slime_hit"), 1f);
			hurtAnim.repeat = true;
			
			animArray[0] = walkAnim;
			animArray[1] = hurtAnim;
			
		
			Slime newSlime = new Slime(Health + (world.CurrentLevel / 2f), X, Y, Assetloader.screenHeight / 5, animArray, Assetloader.allSpritesMap.get("BurstRing"), Weakness.Earth, world);
			
			newSlime.setScale(.8f);
			
			newSlime.addListener(new InputListener(){
				 public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
		            	
		                return true;
		            }
		            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
		            	
		            	//Spell type
		            	//mainStage.addSpell(newSlime, mainStage.spellSelector.selectedSpellSprite);
		            	
		            	//Tap type
		            	//newSlime.Hit(mainStage.spellSelector.damagedColor, 1, mainStage.spellSelector.selectedSpell, mainStage.spellSelector.selectedStatusEffect);
		            	
		            	//SpawnEnemy();
		            }
		        });
			
			IceSlimes -= 1;
			
			mainStage.enemyArray.add(newSlime);
			
			mainStage.addActor(mainStage.enemyArray.get(mainStage.enemyArray.size() - 1));
		
		} else if (spawnType == TapWizSpawnTypes.FireSlime && FireSlimes > 0){
			
			AnimNyko[] animArray = new AnimNyko[4];
			
			AnimNyko walkAnim = new AnimNyko();
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk01"), 0.05f);
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk02"), 0.05f);
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk03"), 0.05f);
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk04"), 0.05f);
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk05"), 0.05f);
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk06"), 0.05f);
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk07"), 0.05f);
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk08"), 0.05f);
			walkAnim.repeat = true;
			
			AnimNyko hurtAnim = new AnimNyko();
			
			hurtAnim.addFrame(Assetloader.allSpritesMap.get("slime_hit"), 1f);
			hurtAnim.repeat = true;
			
			animArray[0] = walkAnim;
			animArray[1] = hurtAnim;
			
		
			Slime newSlime = new Slime(Health + (world.CurrentLevel / 2f), X, Y, Assetloader.screenHeight / 5, animArray, Assetloader.allSpritesMap.get("BurstRing"), Weakness.Ice, world);
			
			newSlime.setScale(.8f);
			
			newSlime.addListener(new InputListener(){
				 public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
		            	
		                return true;
		            }
		            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
		            	
		            	//Spell type
		            	//mainStage.addSpell(newSlime, mainStage.spellSelector.selectedSpellSprite);
		            	
		            	//Tap type
		            	//newSlime.Hit(mainStage.spellSelector.damagedColor, 1, mainStage.spellSelector.selectedSpell, mainStage.spellSelector.selectedStatusEffect);
		            	
		            	//SpawnEnemy();
		            }
		        });
			
			FireSlimes -= 1;
			
			mainStage.enemyArray.add(newSlime);
			
			mainStage.addActor(mainStage.enemyArray.get(mainStage.enemyArray.size() - 1));
		
		} else if (spawnType == TapWizSpawnTypes.BossSlime && BossSlimes > 0){
			
			AnimNyko[] animArray = new AnimNyko[4];
			
			AnimNyko walkAnim = new AnimNyko();
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk01"), 0.05f);
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk02"), 0.05f);
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk03"), 0.05f);
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk04"), 0.05f);
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk05"), 0.05f);
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk06"), 0.05f);
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk07"), 0.05f);
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk08"), 0.05f);
			walkAnim.repeat = true;
			
			AnimNyko hurtAnim = new AnimNyko();
			
			hurtAnim.addFrame(Assetloader.allSpritesMap.get("slime_hit"), 1f);
			hurtAnim.repeat = true;
			
			animArray[0] = walkAnim;
			animArray[1] = hurtAnim;
			
		
			Slime newSlime = new Slime(25 + (world.CurrentLevel / 2f),(Assetloader.screenWidth / 2) - ((Assetloader.allSpritesMap.get("slime_walk01").getWidth() / 2f) * 1.25f), Y, Assetloader.screenHeight / 5, animArray, Assetloader.allSpritesMap.get("BurstRing"), Weakness.Fire, world);
			
			newSlime.setScale(2.5f);
			
			newSlime.addListener(new InputListener(){
				 public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
		            	
		                return true;
		            }
		            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
		            	
		            	//Spell type
		            	//mainStage.addSpell(newSlime, mainStage.spellSelector.selectedSpellSprite);
		            	
		            	//Tap type
		            	//newSlime.Hit(mainStage.spellSelector.damagedColor, 1, mainStage.spellSelector.selectedSpell, mainStage.spellSelector.selectedStatusEffect);
		            	
		            	//SpawnEnemy();
		            }
		        });
			
			BossSlimes -= 1;
			
			mainStage.enemyArray.add(newSlime);
			
			mainStage.addActor(mainStage.enemyArray.get(mainStage.enemyArray.size() - 1));
		
		} else if (spawnType == TapWizSpawnTypes.IceBossSlime && IceBossSlimes > 0){
			
			AnimNyko[] animArray = new AnimNyko[4];
			
			AnimNyko walkAnim = new AnimNyko();
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk01"), 0.05f);
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk02"), 0.05f);
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk03"), 0.05f);
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk04"), 0.05f);
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk05"), 0.05f);
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk06"), 0.05f);
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk07"), 0.05f);
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk08"), 0.05f);
			walkAnim.repeat = true;
			
			AnimNyko hurtAnim = new AnimNyko();
			
			hurtAnim.addFrame(Assetloader.allSpritesMap.get("slime_hit"), 1f);
			hurtAnim.repeat = true;
			
			animArray[0] = walkAnim;
			animArray[1] = hurtAnim;
			
		
			Slime newSlime = new Slime(25 + (world.CurrentLevel / 2f),(Assetloader.screenWidth / 2) - ((Assetloader.allSpritesMap.get("slime_walk01").getWidth() / 2f) * 1.25f), Y, Assetloader.screenHeight / 5, animArray, Assetloader.allSpritesMap.get("BurstRing"), Weakness.Earth, world);
			
			newSlime.setScale(2.5f);
			
			newSlime.addListener(new InputListener(){
				 public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
		            	
		                return true;
		            }
		            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
		            	
		            	//Spell type
		            	//mainStage.addSpell(newSlime, mainStage.spellSelector.selectedSpellSprite);
		            	
		            	//Tap type
		            	//newSlime.Hit(mainStage.spellSelector.damagedColor, 1, mainStage.spellSelector.selectedSpell, mainStage.spellSelector.selectedStatusEffect);
		            	
		            	//SpawnEnemy();
		            }
		        });
			
			IceBossSlimes -= 1;
			
			mainStage.enemyArray.add(newSlime);
			
			mainStage.addActor(mainStage.enemyArray.get(mainStage.enemyArray.size() - 1));
		} else if (spawnType == TapWizSpawnTypes.FireBossSlime && FireBossSlimes > 0){
			
			AnimNyko[] animArray = new AnimNyko[4];
			
			AnimNyko walkAnim = new AnimNyko();
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk01"), 0.05f);
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk02"), 0.05f);
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk03"), 0.05f);
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk04"), 0.05f);
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk05"), 0.05f);
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk06"), 0.05f);
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk07"), 0.05f);
			walkAnim.addFrame(Assetloader.allSpritesMap.get("slime_walk08"), 0.05f);
			walkAnim.repeat = true;
			
			AnimNyko hurtAnim = new AnimNyko();
			
			hurtAnim.addFrame(Assetloader.allSpritesMap.get("slime_hit"), 1f);
			hurtAnim.repeat = true;
			
			animArray[0] = walkAnim;
			animArray[1] = hurtAnim;
			
		
			Slime newSlime = new Slime(25 + (world.CurrentLevel / 2f),(Assetloader.screenWidth / 2) - ((Assetloader.allSpritesMap.get("slime_walk01").getWidth() / 2f) * 1.25f), Y, Assetloader.screenHeight / 5, animArray, Assetloader.allSpritesMap.get("BurstRing"), Weakness.Ice, world);
			
			newSlime.setScale(2.5f);
			
			newSlime.addListener(new InputListener(){
				 public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
		            	
		                return true;
		            }
		            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
		            	
		            	//Spell type
		            	//mainStage.addSpell(newSlime, mainStage.spellSelector.selectedSpellSprite);
		            	
		            	//Tap type
		            	//newSlime.Hit(mainStage.spellSelector.damagedColor, 1, mainStage.spellSelector.selectedSpell, mainStage.spellSelector.selectedStatusEffect);
		            	
		            	//SpawnEnemy();
		            }
		        });
			
			FireBossSlimes -= 1;
			
			mainStage.enemyArray.add(newSlime);
			
			mainStage.addActor(mainStage.enemyArray.get(mainStage.enemyArray.size() - 1));
		}
	
		}
		
	}
	
	@Override
	public Boolean MapEmpty() {
		
		return mainStage.enemyArray.isEmpty();
		
	}
	
	@Override
	public Boolean DontSpawn() {
		
		return (!GameWorld.state.getCurrentState().equals(GameWorld.state.StateMap.get("Playing")));
			
	}
	
	@Override
	public Boolean DoneSpawning() {
		
		
		return (Slimes <= 0 && IceSlimes <= 0 && FireSlimes <= 0  && BossSlimes <= 0 && IceBossSlimes <= 0 && FireBossSlimes <= 0);
		
	}
		
}

