package com.nyko111.Actors;

import java.util.Timer;
import java.util.TimerTask;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.nyko111.GameWorld.GameWorld;
import com.nykoengine.Audio.AudioManager;
import com.nykoengine.Helpers.Assetloader;

public class Wall {

	Sprite currentSprite;
	
	boolean bShake = false;
	
	public Vector2 Position = new Vector2();
	
	//float delta;
	
	Timer invincableTimer;
	
	public float Health = 3;
	
	public Boolean bInvincable = false;
	
	public Integer ReinforcedLevel = 1;
	
	public Wall(float X, float Y, Sprite currentSprite) {
		
		Health = Health + ReinforcedLevel;
		
		this.currentSprite = currentSprite;
		
		Position.x = X;
		
		Position.y = Y;
		
		invincableTimer = new Timer();
		
	}
 
	public void draw(Batch batch, float alpha) {
		
		batch.draw(currentSprite, Position.x, Position.y, currentSprite.getWidth() * Assetloader.gameScaleX, currentSprite.getHeight() * Assetloader.gameScaleY);
		
		if (Health == 3) {
			
			batch.draw(Assetloader.allSpritesMap.get("WallDamage1"), Position.x + Assetloader.screenWidth / 1.37f, Position.y + Assetloader.screenHeight / 5.75f, Assetloader.allSpritesMap.get("WallDamage1").getWidth() * Assetloader.gameScaleX, Assetloader.allSpritesMap.get("WallDamage1").getHeight() * Assetloader.gameScaleY);
			
			
			//lower floor cracks
			
			batch.draw(Assetloader.allSpritesMap.get("WallDamageFloor"), Position.x + Assetloader.screenWidth / 3.8f, Position.y + Assetloader.screenHeight / 70f, Assetloader.allSpritesMap.get("WallDamageFloor").getWidth() * Assetloader.gameScaleX, Assetloader.allSpritesMap.get("WallDamageFloor").getHeight() * Assetloader.gameScaleY);
			
		} else if (Health == 2) {
			
			batch.draw(Assetloader.allSpritesMap.get("WallDamage1"), Position.x + Assetloader.screenWidth / 6.5f, Position.y + Assetloader.screenHeight / 5.75f, Assetloader.allSpritesMap.get("WallDamage1").getWidth() * Assetloader.gameScaleX, Assetloader.allSpritesMap.get("WallDamage1").getHeight() * Assetloader.gameScaleY);
			
			batch.draw(Assetloader.allSpritesMap.get("WallDamage1"), Position.x + Assetloader.screenWidth / 1.37f, Position.y + Assetloader.screenHeight / 5.75f, Assetloader.allSpritesMap.get("WallDamage1").getWidth() * Assetloader.gameScaleX, Assetloader.allSpritesMap.get("WallDamage1").getHeight() * Assetloader.gameScaleY);
			
			//upper floor cracks
			
			batch.draw(Assetloader.allSpritesMap.get("WallDamageFloorReversed"), Position.x + Assetloader.screenWidth / 1.15f, Position.y + Assetloader.screenHeight / 9.8f, Assetloader.allSpritesMap.get("WallDamageFloorReversed").getWidth() * Assetloader.gameScaleX, Assetloader.allSpritesMap.get("WallDamageFloorReversed").getHeight() * Assetloader.gameScaleY);
			
			//lower floor cracks
			
			batch.draw(Assetloader.allSpritesMap.get("WallDamageFloor"), Position.x + Assetloader.screenWidth / 3.8f, Position.y + Assetloader.screenHeight / 70f, Assetloader.allSpritesMap.get("WallDamageFloor").getWidth() * Assetloader.gameScaleX, Assetloader.allSpritesMap.get("WallDamageFloor").getHeight() * Assetloader.gameScaleY);
			
			
		} else if (Health <= 1) {
			
			batch.draw(Assetloader.allSpritesMap.get("WallDamage1"), Position.x + Assetloader.screenWidth / 6.5f, Position.y + Assetloader.screenHeight / 5.75f, Assetloader.allSpritesMap.get("WallDamage1").getWidth() * Assetloader.gameScaleX, Assetloader.allSpritesMap.get("WallDamage1").getHeight() * Assetloader.gameScaleY);
			
			batch.draw(Assetloader.allSpritesMap.get("WallDamage1"), Position.x + Assetloader.screenWidth / 1.37f, Position.y + Assetloader.screenHeight / 5.75f, Assetloader.allSpritesMap.get("WallDamage1").getWidth() * Assetloader.gameScaleX, Assetloader.allSpritesMap.get("WallDamage1").getHeight() * Assetloader.gameScaleY);
			
			batch.draw(Assetloader.allSpritesMap.get("WallDamage1"), Position.x + Assetloader.screenWidth / 1.03f, Position.y + Assetloader.screenHeight / 6.4f, Assetloader.allSpritesMap.get("WallDamage1").getWidth() * Assetloader.gameScaleX, Assetloader.allSpritesMap.get("WallDamage1").getHeight() * Assetloader.gameScaleY);
			
			//upper floor cracks
			
			batch.draw(Assetloader.allSpritesMap.get("WallDamageFloorReversed"), Position.x + Assetloader.screenWidth / 12f, Position.y + Assetloader.screenHeight / 9.8f, Assetloader.allSpritesMap.get("WallDamageFloorReversed").getWidth() * Assetloader.gameScaleX, Assetloader.allSpritesMap.get("WallDamageFloorReversed").getHeight() * Assetloader.gameScaleY);
			
			batch.draw(Assetloader.allSpritesMap.get("WallDamageFloorReversed"), Position.x + Assetloader.screenWidth / 1.15f, Position.y + Assetloader.screenHeight / 9.8f, Assetloader.allSpritesMap.get("WallDamageFloorReversed").getWidth() * Assetloader.gameScaleX, Assetloader.allSpritesMap.get("WallDamageFloorReversed").getHeight() * Assetloader.gameScaleY);
			
			//lower floor cracks
			batch.draw(Assetloader.allSpritesMap.get("WallDamageFloor"), Position.x + Assetloader.screenWidth / 1.4f, Position.y + Assetloader.screenHeight / 70f, Assetloader.allSpritesMap.get("WallDamageFloor").getWidth() * Assetloader.gameScaleX, Assetloader.allSpritesMap.get("WallDamageFloor").getHeight() * Assetloader.gameScaleY);
			
			batch.draw(Assetloader.allSpritesMap.get("WallDamageFloor"), Position.x + Assetloader.screenWidth / 3.8f, Position.y + Assetloader.screenHeight / 70f, Assetloader.allSpritesMap.get("WallDamageFloor").getWidth() * Assetloader.gameScaleX, Assetloader.allSpritesMap.get("WallDamageFloor").getHeight() * Assetloader.gameScaleY);

		}
		
		//setBounds(Position.x, Position.y, currentSprite.getWidth() * Assetloader.gameScaleX, currentSprite.getHeight() * Assetloader.gameScaleY);
	
	}
	
	public void Hit(int Damage) {
		
		bShake = true;
		
		if (!bInvincable) 
			Health -= Damage;
		
		Assetloader.sounds.get("WallCrumble").play(AudioManager.SoundVolume);
		
	}
	
	public void Invincable() {
		
		bInvincable = true;
		
		invincableTimer.schedule( new TimerTask() {
			@Override
			public void run() {
				
				bInvincable = false;
				
			}
		}, 3000);
		
	}
	
}
