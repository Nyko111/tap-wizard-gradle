package com.nyko111.Actors;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Vector2;
import com.nyko111.GameWorld.GameWorld;
import com.nyko111.GameWorld.MainStage;
import com.nykoengine.Audio.AudioManager;
import com.nykoengine.Helpers.Assetloader;

public class ManaBall extends StarBall {

	public ManaBall(Sprite StarBall, Vector2 Position, Vector2 Target, GameWorld world) {
		super(StarBall, Position, Target, world);
		// TODO Auto-generated constructor stub




	}
	
	
	@Override
	public void update(float deltaTime) {
		
		if (Moving) {
			
		//Distance = Position.dst(Target);
			
		if (xMovement < Direction.x) {
				
			xMovement += deltaTime * SpeedMult;
				
		} else if (xMovement > Direction.x){
				
			xMovement -= deltaTime * SpeedMult;
				
		} else {
			
			xMovement = Direction.x;
			
		}
			
		Position.add((Direction.x * 500) * deltaTime, (Direction.y * 500) * deltaTime);
		
		hitbox.Update(Position.x, Position.y);
		
		if (Intersector.overlaps(hitbox.getBounds(), world.mainStage.wizard.hitbox.getBounds())) {
			
			Position = Target;
			
			if (world.mainStage.manaBar.Value <= 90)
				world.mainStage.manaBar.Value += 10;
			else
				world.mainStage.manaBar.Value = 100;
			
			Assetloader.sounds.get("StarBall").play(AudioManager.SoundVolume);
			
			Moving = false;
			
		}
		
		}
		
		
	}

	
	
}
