package com.nyko111.Actors;

import com.badlogic.gdx.ai.fsm.DefaultStateMachine;
import com.badlogic.gdx.ai.fsm.StateMachine;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.nyko111.GameWorld.GameWorld;
import com.nyko111.Spells.SpellBase.SpellType;
import com.nyko111.StatusEffects.StatusEffect;
import com.nykoengine.Animation.AnimNyko;
import com.nykoengine.Audio.AudioManager;
import com.nykoengine.Helpers.Assetloader;
import com.nykoengine.ParticleEngine.Emitter;

public class Slime extends Enemy {
	
	public StateMachine<Slime> state;
	
	public Sound WalkSound;
	
	public long walkSoundID, hurtSoundID;
	
	public float walkSoundTimer = 0.395f;

	public GameWorld world;

	public Slime(float Health, float X, float Y, float WalkSpeed,
			AnimNyko[] animation, Sprite DamageSprite, Weakness weakness, GameWorld world) {
		super(Health, X, Y, WalkSpeed, animation, DamageSprite, weakness, Color.GREEN);
		// TODO Auto-generated constructor stub

		this.world = world;

		particleMovement.x = 450f;
		
		particleMovement.y = 200f;
		
		WalkSound = Assetloader.sounds.get("SlimeWalk");
		
		state = new DefaultStateMachine<Slime>(this, SlimeState.WALKING);
		
		state.changeState(SlimeState.WALKING);
		
	}
	
	@Override
	public void act(float Delta) {
		
		super.act(Delta);
		
		state.update();
		
	}
	
	@Override
	public void Hit(Color displayDamageColor, float displayDamage, SpellType spellType, StatusEffect statusEffect) {
		
		super.Hit(displayDamageColor, displayDamage, spellType, statusEffect);
		
		WalkSpeed -= (WalkSpeed / 1.25f);
		
		hurtSoundID = Assetloader.sounds.get("SlimeHit").play(AudioManager.SoundVolume);
		
		Emitter emitter = new Emitter(Assetloader.allSpritesMap.get("slime particle"), null, MiddlePos, particleMovement.cpy(), 50, .3f, .25f, 3, 1);
		
		emitter.setColor(EnemyColor);
		
		GameWorld.particleEmitterArray.add(emitter);
		
	}
	
	public void dispose() {
		
		WalkSound.dispose();
		
		super.dispose();
		
	}

}
