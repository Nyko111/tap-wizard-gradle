package com.nyko111.Actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ai.fsm.State;
import com.badlogic.gdx.ai.msg.Telegram;
import com.badlogic.gdx.math.Vector2;
import com.nyko111.GameWorld.GameWorld;
import com.nyko111.GameWorld.MainStage;
import com.nykoengine.Audio.AudioManager;
import com.nykoengine.Helpers.Assetloader;

public enum SlimeState implements State<Slime>{
	
	WALKING() {
		@Override
		public void enter(Slime slime) {
			// TODO Auto-generated method stub
			
			//slime.currentAnimation = WALKINGANIMATION;
			
			slime.walkSoundID = slime.WalkSound.play(AudioManager.SoundVolume);
			
		}
		
		@Override
		public void update(Slime slime) {
			// TODO Auto-generated method stub
	
			//walk downwards
			
			if (slime.walkSoundTimer <= 0) {
				
				slime.walkSoundID = slime.WalkSound.play(AudioManager.SoundVolume);
				slime.walkSoundTimer = 0.395f;
				
			} else {
				
				slime.walkSoundTimer -= Gdx.graphics.getDeltaTime();
				
			}
			
			slime.Position.y -= (slime.WalkSpeed * slime.delta);
			
			//slime.hitbox.Update(slime.Position.x, slime.Position.y);
			
			//slime.slimeY -= SPEED;
			if (slime.Health <= 0) {
				slime.state.changeState(DEAD);
			} else if (slime.Position.y < Assetloader.screenHeight / 5.5 /* - (slime.currentSprite.getHeight() * Assetloader.gameScaleY / 2)*/) {
				
				//damage wall or whatever
				
				slime.state.changeState(DESPAWN);
			}
			
			
		}
	}, 
	
	DESPAWN() {
		
		@Override
		public void enter(Slime slime) {
			// TODO Auto-generated method stub
			
			//SCORE REMOVE
			
			Assetloader.sounds.get("SlimeDeath").play(AudioManager.SoundVolume);
			
			slime.hitbox.setBounds(0, 0);
			
			slime.bShowDamageSprite = true;
			
			slime.world.mainStage.screenShake.Reset(1);
			
			slime.world.mainStage.screenShake.setIntensity(.5f);
			
			//screenShake = new ScreenShake(GameRenderer.InitCamX, GameRenderer.InitCamY, 1, 4);
			
			slime.world.mainStage.ResetScoreMultiplier();
			
			slime.world.mainStage.wall.Hit(1);
			
			slime.WalkSound.stop(slime.walkSoundID);
			
			slime.WalkSound = null;
			
			//slime.currentAnimation = DEATHANIM;
			
		}
		
		@Override
		public void update(Slime slime) {
			// TODO Auto-generated method stub
			
			if (slime.DamageSpriteScale < 2.5f) {
				slime.DamageSpriteScale += Assetloader.screenWidth / 2500;
			}
			
			if (slime.Alpha > 0)
				slime.Alpha -= slime.delta * 2;
			
			//MainStage.screenShake.shakeScreen(GameRenderer.cam, GameRenderer.batch, 4);
	
			if (slime.Alpha <= 0 && slime.world.mainStage.screenShake.bIsDone) {
				
				slime.isDead = true;
				
				slime.clearListeners();
				
				slime.remove();
				
			}
			
		}
	
	}, 
	
	DEAD() {
		
		@Override
		public void enter(Slime slime) {
			// TODO Auto-generated method stub
			
			//screenShake = new ScreenShake(GameRenderer.InitCamX, GameRenderer.InitCamY, .5f);
			
			Assetloader.sounds.get("SlimeDeath").play(AudioManager.SoundVolume);
			
			slime.hitbox.setBounds(0, 0);
			
			slime.bShowDamageSprite = true;
			
			MainStage.screenShake.Reset(.5f);
			
			MainStage.screenShake.setIntensity(.25f);
			
			slime.world.mainStage.ScoreMultiplier();
			
			slime.WalkSound.stop(slime.walkSoundID);
			
			slime.WalkSound = null;
			
			//GameWorld.playerInfo.XP += 2;
			
			
		}
		
		@Override
		public void update(Slime slime) {
			// TODO Auto-generated method stub
			
			if (slime.DamageSpriteScale < 2.5f) {
				slime.DamageSpriteScale += Assetloader.screenWidth / 2500;
			}
	
			if (slime.Alpha > 0)
				slime.Alpha -= slime.delta * 2;
			
			//MainStage.screenShake.shakeScreen(GameRenderer.cam, GameRenderer.batch, 1);
			
			if (slime.Alpha <= 0 && MainStage.screenShake.bIsDone) {
				
				slime.isDead = true;
				
				slime.clearListeners();
				
				slime.world.mainStage.starBallArray.add(new StarBall(Assetloader.allSpritesMap.get("StarBall"), slime.MiddlePos, new Vector2(slime.world.mainStage.wizard.getCenterX(), slime.world.mainStage.wizard.getCenterY()), slime.world));
				
				//Mana ball spawn
				/*if (Assetloader.rand.nextInt(10) == 5) {
					
					MainStage.manaBallArray.add(new ManaBall(Assetloader.allSpritesMap.get("manaball"), slime.Position, new Vector2(MainStage.wizard.getCenterX(), MainStage.wizard.getCenterY())));
					
				}*/
				
				//GameRenderer.resetCamera();
				
				slime.remove();
				
				
			}
			
		}
	
	};
	

	

	@Override
	public void exit(Slime slime) {
		// TODO Auto-generated method stub
		
		
	}

	@Override
	public boolean onMessage(Slime arg0, Telegram arg1) {
		// TODO Auto-generated method stub
		return false;
	}

}
