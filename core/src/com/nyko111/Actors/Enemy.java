package com.nyko111.Actors;

import java.util.Timer;
import java.util.TimerTask;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.nyko111.GameWorld.GameWorld;
import com.nyko111.Spells.SpellBase.SpellType;
import com.nyko111.StatusEffects.StatusEffect;
import com.nyko111.StatusEffects.StatusEffect.StatusType;
import com.nykoengine.Animation.AnimNyko;
import com.nykoengine.Detection.HitBox;
import com.nykoengine.Helpers.Assetloader;

public class Enemy extends Actor {

	Sprite currentSprite;
	
	Sprite DamageSprite;
	
	AnimNyko currentAnimation;
	
	AnimNyko[] allAnimations;
	
	public Boolean bIsColliding = false;
	
	public Boolean bShowdisplayDamage = false;
	
	float Alpha = 1f;
	
	//Shows how much displayDamage was done to self
	public Float displayDamage = 0f;
	
	//What color enemy is tinted White = untinted
	public Color EnemyColor = new Color(Color.WHITE);
	
	//What color enemy is due to effect (white = uncoloured)
	public Color effectColor = new Color(Color.WHITE);
	
	//What color enemy flashes when displayDamaged 
	public Color displayDamagedColor = new Color(Color.WHITE);
	
	public HitBox hitbox;
	
	public boolean bShowDamageSprite = false;
	
	public boolean isDead = false;
	
	public float FullWalkSpeed = 0;
	
	public float WalkSpeed = 0;
	
	public Vector2 Position = new Vector2();
	
	public Vector2 MiddlePos = new Vector2();
	
	public Vector2 particleMovement = new Vector2();
	
	float delta;
	
	float DamageSpriteScale = 0;
	
	public float Health;
	
	Timer HitTimer;
	
	public float EnemyScale = 1f;
	
	//public enum StatusEffect {Fire, Water, Earth, Air};
	
	public enum Weakness {Fire, Ice, Earth, Air, Default};
	
	//StatusEffect statusEffect;
	
	public Weakness weakness;
	
	public Enemy(float Health, float X, float Y, float WalkSpeed, AnimNyko[] allAnimations, Sprite DamageSprite, Weakness weakness, Color EnemyColor) {
		// TODO Auto-generated constructor stub
		
		Position.x = X;
		
		Position.y = Y;
		
		particleMovement.x = Assetloader.screenWidth / 8f;
		
		particleMovement.y = Assetloader.screenWidth / 8f;
		
		this.FullWalkSpeed = WalkSpeed;
		
		this.WalkSpeed = FullWalkSpeed;
			
		this.Health = Health;
		
		HitTimer = new Timer();
		
		this.weakness = weakness;
		
		//WalkingAnimation = animation;
		
		this.allAnimations = allAnimations;
		
		currentAnimation = allAnimations[0];
		
		this.currentSprite = currentAnimation.currentSprite();
		
		hitbox = new HitBox(Position.x, Position.y, (currentSprite.getWidth() * Assetloader.gameScaleX) * EnemyScale, (currentSprite.getHeight() * Assetloader.gameScaleY) * EnemyScale);
		
		switch (weakness) {
			
		case Fire:
			this.EnemyColor = Color.GREEN;
			break;
		
		case Ice:
			this.EnemyColor = Color.RED;
			break;
			
		case Earth:
			this.EnemyColor = Color.CYAN;
			break;
		
		}
		
		//this.EnemyColor = EnemyColor;
		
		this.DamageSprite = DamageSprite;
		
	}
	
	@Override
	public void act(float delta) {
		
		this.delta = delta;
		
		//this.MiddlePos.x = (((currentSprite.getWidth() / 2f) * EnemyScale) * Assetloader.gameScaleX) + Position.x;
		
		//this.MiddlePos.y = (((currentSprite.getHeight() / 2f) * EnemyScale) * Assetloader.gameScaleY) + Position.y;
		
		if (!bIsColliding && WalkSpeed < FullWalkSpeed) {
			
			WalkSpeed += 1f;
			
		}
		
		hitbox.Update(Position.x, Position.y);
		
		hitbox.getBounds().getCenter(MiddlePos);
		
		currentAnimation.update(delta);
		
		currentSprite = currentAnimation.currentSprite();
		
	}
	
	@Override 
	public void draw(Batch batch, float alpha) {
		
		if (Alpha < 0) {
			Alpha = 0;
		}
		
		batch.setColor(EnemyColor.r, EnemyColor.g, EnemyColor.b, this.Alpha);
		
		if (!effectColor.equals(Color.WHITE))
			batch.setColor(effectColor.r, effectColor.g, effectColor.b, this.Alpha);
		
		//Change color of sprite if color does not equal white
		if (!displayDamagedColor.equals(Color.WHITE))
			batch.setColor(displayDamagedColor.r, displayDamagedColor.g, displayDamagedColor.b, this.Alpha);
		
		batch.draw(currentSprite, Position.x, Position.y, (currentSprite.getWidth() * Assetloader.gameScaleX) * EnemyScale, (currentSprite.getHeight() * Assetloader.gameScaleY) * EnemyScale);
		
		batch.setColor(EnemyColor.r, EnemyColor.g, EnemyColor.b, 1);
		
		if (bShowDamageSprite && DamageSprite != null && DamageSpriteScale <= 2.5f) {
			
			batch.draw(DamageSprite, Position.x + ((((currentSprite.getWidth() * EnemyScale) / 2f) * Assetloader.gameScaleX) - ((((DamageSprite.getWidth() * EnemyScale) * DamageSpriteScale) / 2f) * Assetloader.gameScaleX)), Position.y + ((((currentSprite.getHeight() * EnemyScale) / 2f) * Assetloader.gameScaleY) - ((((DamageSprite.getHeight() * EnemyScale) * DamageSpriteScale) / 2f) * Assetloader.gameScaleY)), (((DamageSprite.getWidth() * Assetloader.gameScaleX)) * EnemyScale) * DamageSpriteScale, (((DamageSprite.getHeight() * Assetloader.gameScaleY)) * EnemyScale) * DamageSpriteScale);
			
		}
		
		batch.setColor(Color.WHITE);
		
		if (bShowdisplayDamage)
			Assetloader.bitmapfonts.get("MainWiz").draw(batch, "-" + Float.toString(displayDamage), Position.x + (((currentSprite.getWidth() / 2) * Assetloader.gameScaleX) * EnemyScale), Position.y + ((currentSprite.getHeight() * Assetloader.gameScaleY) * EnemyScale));
		
		setBounds(Position.x, Position.y, (currentSprite.getWidth() * Assetloader.gameScaleX) * EnemyScale, (currentSprite.getHeight() * Assetloader.gameScaleY) * EnemyScale);
	
		
		
	}
	
	public void Hit(Color displayDamageColor, float displayDamage, SpellType spellType, StatusEffect statusEffect) {
		
		displayDamagedColor = displayDamageColor;
		bShowdisplayDamage = true;
		//bShowDamageSprite = true;
		
		if (statusEffect != null)
			statusEffect.ApplyEffect(this);
		
		if (weakness.ordinal() == spellType.ordinal()) {
			
			Health -= (displayDamage * 2);
			this.displayDamage = displayDamage * 2;
		
		} else {
		
			Health -= displayDamage;
			this.displayDamage = displayDamage;
		
		}
		
		if (allAnimations[1] != null) {
			
			currentAnimation = allAnimations[1];
				
		}
		
		//effect.getEmitters().first().setPosition(MiddlePos.x, MiddlePos.y);
		
		 HitTimer.schedule( new TimerTask() {
			@Override
			public void run() {
				
				
				displayDamagedColor = Color.WHITE;
				bShowdisplayDamage = false;
				//bShowDamageSprite = false;
				if (Health > 0)
					currentAnimation = allAnimations[0];
				
				//this.cancel();
				
			}
		}, 500); 
		
	}
	
	public void StatusEffect(StatusType status) {
		
		
		
	}
	
	public void Collide(float WalkSpeed) {
		
		//bIsColliding = true;
		
		this.WalkSpeed = WalkSpeed;
		
	//	if (CollisionSpeed >= WalkSpeed) {
	//		WalkSpeed += 8;
	//	} else if (CollisionSpeed < WalkSpeed) {
	//		WalkSpeed -= 8;
	//	}
	}
	
	public void setScale(float EnemyScale) {
		
		this.EnemyScale = EnemyScale;
		
		hitbox.setBounds(hitbox.getBounds().width * EnemyScale, hitbox.getBounds().height * EnemyScale);
	}
	
	public void dispose() {
		
		for (int i = 0; i < allAnimations.length; i++) {
			
			allAnimations[i].dispose();
			
		}
		
	
	}

}
