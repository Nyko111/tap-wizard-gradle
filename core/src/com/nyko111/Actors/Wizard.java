package com.nyko111.Actors;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.nykoengine.Animation.AnimNyko;
import com.nykoengine.Detection.HitBox;
import com.nykoengine.Helpers.Assetloader;

public class Wizard {
	
	public static AnimNyko SpellThrowAnim;
	public static AnimNyko IdleAnim;
	
	public static AnimNyko currentAnim;
	
	public static HitBox hitbox;
	
	float X, Y, ShadowX;

	public Wizard() {
		
		SpellThrowAnim = new AnimNyko();
		SpellThrowAnim.addFrame(Assetloader.allSpritesMap.get("WizardAtk1"), 0.03f);
		SpellThrowAnim.addFrame(Assetloader.allSpritesMap.get("WizardAtk2"), 0.03f);
		SpellThrowAnim.addFrame(Assetloader.allSpritesMap.get("WizardAtk3"), 0.03f);
		SpellThrowAnim.addFrame(Assetloader.allSpritesMap.get("WizardAtk4"), 0.03f);
		SpellThrowAnim.addFrame(Assetloader.allSpritesMap.get("WizardAtk5"), 0.03f);
		SpellThrowAnim.addFrame(Assetloader.allSpritesMap.get("WizardAtk6"), 0.03f);
		SpellThrowAnim.addFrame(Assetloader.allSpritesMap.get("WizardAtk5"), 0.03f);
		SpellThrowAnim.addFrame(Assetloader.allSpritesMap.get("WizardAtk4"), 0.03f);
		SpellThrowAnim.addFrame(Assetloader.allSpritesMap.get("WizardAtk3"), 0.03f);
		SpellThrowAnim.addFrame(Assetloader.allSpritesMap.get("WizardAtk2"), 0.03f);
		SpellThrowAnim.addFrame(Assetloader.allSpritesMap.get("WizardAtk1"), 0.03f);
		
		IdleAnim = new AnimNyko();
		IdleAnim.addFrame(Assetloader.allSpritesMap.get("WizardAtk1"), 0.05f);
		
		currentAnim = IdleAnim;
		
		X = (Assetloader.screenWidth - currentAnim.currentSprite().getWidth() * Assetloader.gameScaleX) / 2.15f;
		
		Y = Assetloader.screenHeight / 50f;
		
		ShadowX = ((Assetloader.screenWidth / 2) - ((Assetloader.allSpritesMap.get("WizardShadow").getWidth() * Assetloader.gameScaleX) / 2)) + Assetloader.screenWidth / 100;
		
		hitbox = new HitBox(X + (Assetloader.screenWidth / 21.62f), Y, (currentAnim.currentSprite().getWidth() * Assetloader.gameScaleX) * .6f, (currentAnim.currentSprite().getHeight() * Assetloader.gameScaleY) * 0.6f);
		
	}
	
	public void update(float deltaTime) {
		
		currentAnim.update(deltaTime);
		
		if (SpellThrowAnim.isDone()) {
			
			currentAnim = IdleAnim;
			
			SpellThrowAnim.ResetAnim();
			
		}
		
	}
	
	public void draw(Batch batch) {
		
		batch.draw(Assetloader.allSpritesMap.get("WizardShadow"), ShadowX, Y, Assetloader.allSpritesMap.get("WizardShadow").getWidth() * Assetloader.gameScaleX, Assetloader.allSpritesMap.get("WizardShadow").getHeight() * Assetloader.gameScaleY);
		
		batch.draw(currentAnim.currentSprite(), X, Y, currentAnim.currentSprite().getWidth() * Assetloader.gameScaleX, currentAnim.currentSprite().getHeight() * Assetloader.gameScaleY);
	
	}

	public float getCenterX() {
		
		return ((currentAnim.currentSprite().getWidth() / 2) * Assetloader.gameScaleX) + X;
		
	}
	
	public float getCenterY() {
		
		return ((currentAnim.currentSprite().getHeight() / 2) * Assetloader.gameScaleY) + Y;
		
	}
	
	public void dispose() {
		
		SpellThrowAnim.dispose();
		IdleAnim.dispose();
		
		
		
	}
	
}
