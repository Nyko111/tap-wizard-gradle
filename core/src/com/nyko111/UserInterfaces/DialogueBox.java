package com.nyko111.UserInterfaces;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.nykoengine.Helpers.Assetloader;

public class DialogueBox {
	
	public Sprite DialogueBG;
	
	Rectangle textfield;
	
	float charWidth, charHeight;
	
	public int Page = 0;
	
	int maxCharPerLine, maxLines;

	public float X, Y, Width, Height;
	
	public float strX = 0, strY = 0;
	
	public Array<Float> strXArray = new Array<Float>(), strYArray = new Array<Float>();
	
	public GlyphLayout glyphLayout = new GlyphLayout();
	
	public BitmapFont bitmapFont;
	
	public Array<String> strArray = new Array<String>(); 
	
	public String initialString;
	
	public int amountOfLines = 1;
	
	String[] strsplit;
	
	int amountOfPages;
	
	int strSplit;
	
	char letter;
	
	int strXIter = 0, strYIter = 0;
	
	public DialogueBox(Sprite DialogueBG, float X, float Y, float Width, float Height, Vector2 textfield, BitmapFont bitmapFont, String str) {
		
		this.DialogueBG = DialogueBG;//DialogueBG;
		
		this.X = X;
		
		this.Y = Y;
		
		this.Width = Width;
		
		this.Height = Height;
		
		this.textfield = new Rectangle(Width, Height, textfield.x, textfield.y);
		
		this.bitmapFont = bitmapFont;
		
		glyphLayout.setText(bitmapFont, "E");
		
		charWidth = glyphLayout.width;
		
		glyphLayout.setText(bitmapFont, "H");
		
		charHeight = glyphLayout.height;
		
		strX = X + charWidth;
		
		strY = Y + (this.textfield.height * Assetloader.gameScaleY) - (charHeight * 1.25f);
		
		maxCharPerLine = (int) (((this.textfield.width + Width) * Assetloader.gameScaleX) / charWidth);
		
		maxLines = (int) (((this.textfield.height + Height) * Assetloader.gameScaleY) / charHeight);
		
		amountOfPages = (int) str.length() / maxCharPerLine;
		
		//strsplit = new String[str.length() / maxCharPerLine];
		
		for(int i = 0; i < (maxLines - 1); i++) {
			
			strXArray.add(strX);
			strYArray.add(strY);
			
			strY -= charHeight * 1.25f;
			
		}
		
		splitString(str);
		
		//for (int i = 0; strsplit.length > i; i++) {
			
			
			
		}
		
	
	
	public void draw(Batch batch) {
		
		batch.begin();
		
		batch.draw(DialogueBG, X, Y, (DialogueBG.getWidth() + Width) * Assetloader.gameScaleX, (DialogueBG.getHeight() + Height) * Assetloader.gameScaleY);
	
		for (int i = Page; Page + maxLines - 1 > i; i++) {
			
			bitmapFont.draw(batch, strArray.get(i), strXArray.get(strXIter), strYArray.get(strYIter) + charHeight);
			
			if (strXIter < maxLines - 2) {
				strXIter += 1;
				strYIter += 1;
			} else {
				strXIter = 0;
				strYIter = 0;
				
			}
		}
	
		batch.end();
		
	}
	
	public void update(float deltaTime) {
		
		
		
	}
	
	public void splitString(String str) {
			
		//String[] returnString = new String[(int) str.length() / maxCharPerLine];
		
		for (boolean done = false; done == false;) {
			
			if (str.length() < maxCharPerLine) {
				
				newLine(str.substring(0, str.length()), strX, strY);
				done = true;
				
			} else {
				
				strSplit = maxCharPerLine;
				
				letter = str.charAt(strSplit);
				
				for (; letter != ' ';) {
					
					letter = str.charAt(strSplit);
					
					if (letter == ' ') {
						
						newLine(str.substring(0, strSplit), strX, strY);
						
						str = str.substring(strSplit, str.length());
					
					}
					
					strSplit -= 1;
					
				}
				
			}		
	
	}
		
}
	
	public void newLine(String str, float X, float Y) {
		
		strArray.add(str);
		
		//strXArray.add(X);
		
		//strYArray.add(Y);
		
		if (amountOfLines >= maxLines) {
			strY = Y + (DialogueBG.getHeight() * Assetloader.gameScaleY) - (charHeight * 1.25f);
			amountOfLines = 1;
		} else {
			strY -= charHeight * 1.25f;
			amountOfLines += 1;
		}
		
	}
	
	public Boolean nextPage() {
		if (Page + maxLines - 1 < strArray.size - 1) {
			if (Page + maxLines > (strArray.size - 1)) {
				Page = strArray.size;
			} else 
				Page += maxLines - 1;
			return true;
		} else {
			return false;
		}
			
	}
	

}
