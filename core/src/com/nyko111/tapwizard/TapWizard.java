package com.nyko111.tapwizard;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.assets.AssetManager;
import com.nyko111.Interfaces.GoogleInterface;
import com.nyko111.Interfaces.IabInterface;
import com.nyko111.Screens.LoadingScreen;
import com.nykoengine.Helpers.Assetloader;

public class TapWizard extends Game {
	
	//In app billing interface
	public IabInterface platformInterface;
	
	//Google play interface
	public GoogleInterface googleInterface;
	
	//Loading screen
	public LoadingScreen load;
	
	//This loads all of the assets while displaying loadingscreen
	AssetManager manager;
	
	//This contains all the asset file locations
	Assetloader asset;
	
	//GameScreen game;
	
	public TapWizard(IabInterface aInt, GoogleInterface gInterface) {
		platformInterface = aInt;
		
		googleInterface = gInterface;
		
	}
	//Desktop
	public TapWizard() {
	
	}
	
	@Override
	public void create() {
		
		manager = new AssetManager();
		
		
		
		System.out.println("HAAAAI!");
		
		asset = new Assetloader(manager);
		
		load = new LoadingScreen(platformInterface, googleInterface, this);
		
		
		
		//game = new GameScreen(platformInterface);
		setScreen(load);
		
	}
	
	@Override
	public void dispose() {
		super.dispose();
		manager.dispose();
		Assetloader.dispose();
		load.dispose();
		
	}
	
}
