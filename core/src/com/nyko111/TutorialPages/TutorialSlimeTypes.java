package com.nyko111.TutorialPages;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.nyko111.GameWorld.GameRenderer;
import com.nykoengine.Helpers.Assetloader;
import com.nykoengine.TutorialEngine.TutorialHandler;
import com.nykoengine.TutorialEngine.TutorialPageBase;

public class TutorialSlimeTypes extends TutorialPageBase {
	
	float PopupX, PopupY;
	
	GlyphLayout glyphLayout, glyphLayout2, glyphLayout3;
	
	public TutorialSlimeTypes() {
		
		PopupX = ((Assetloader.screenWidth / 2f) - ((Assetloader.allBGMap.get("LoginBG").getWidth()) / 2) * Assetloader.gameScaleX);
		
		PopupY = Assetloader.screenHeight / 3.8f;
		
		glyphLayout = new GlyphLayout(Assetloader.bitmapfonts.get("MainFont"), "Slimes are weak");
		
		glyphLayout2 = new GlyphLayout(Assetloader.bitmapfonts.get("MainFont"), "to certain spells!");
		
		glyphLayout3 = new GlyphLayout(Assetloader.bitmapfonts.get("MainFont"), "right slime!");
		
	}
	
	@Override
	public void draw() {
		
		GameRenderer.batch.begin();
		
		GameRenderer.batch.draw(Assetloader.allBGMap.get("LoginBG"), PopupX, PopupY, Assetloader.allBGMap.get("LoginBG").getWidth() * Assetloader.gameScaleX, Assetloader.allBGMap.get("LoginBG").getHeight() * Assetloader.gameScaleY);

		Assetloader.bitmapfonts.get("MainFont").draw(GameRenderer.batch, glyphLayout, (Assetloader.screenWidth / 2) - (glyphLayout.width / 2), Assetloader.screenHeight / 1.3f);

		Assetloader.bitmapfonts.get("MainFont").draw(GameRenderer.batch, glyphLayout2, (Assetloader.screenWidth / 2) - (glyphLayout2.width / 2), Assetloader.screenHeight / 1.4f);
		
		//Assetloader.MainFont.draw(GameRenderer.batch, glyphLayout3, (Assetloader.screenWidth / 2) - (glyphLayout3.width / 2), Assetloader.screenHeight / 1.51f);
		
		//Slime images
		GameRenderer.batch.setColor(Color.GREEN);
		
		GameRenderer.batch.draw(Assetloader.allSpritesMap.get("slime_walk03"), Assetloader.screenWidth / 1.5f, Assetloader.screenHeight / 2f, (Assetloader.allSpritesMap.get("slime_walk03").getWidth() * Assetloader.gameScaleX) * 0.6f, (Assetloader.allSpritesMap.get("slime_walk03").getHeight() * Assetloader.gameScaleY) * 0.6f);
		
		GameRenderer.batch.setColor(Color.CYAN);
		
		GameRenderer.batch.draw(Assetloader.allSpritesMap.get("slime_walk03"), Assetloader.screenWidth / 1.5f, Assetloader.screenHeight / 2.6f, (Assetloader.allSpritesMap.get("slime_walk03").getWidth() * Assetloader.gameScaleX) * 0.6f, (Assetloader.allSpritesMap.get("slime_walk03").getHeight() * Assetloader.gameScaleY) * 0.6f);
		
		GameRenderer.batch.setColor(Color.RED);
		
		GameRenderer.batch.draw(Assetloader.allSpritesMap.get("slime_walk03"), Assetloader.screenWidth / 1.5f, Assetloader.screenHeight / 3.7f, (Assetloader.allSpritesMap.get("slime_walk03").getWidth() * Assetloader.gameScaleX) * 0.6f, (Assetloader.allSpritesMap.get("slime_walk03").getHeight() * Assetloader.gameScaleY) * 0.6f);
		
		GameRenderer.batch.setColor(Color.WHITE);
		
		//Arrow images
		
		//First arrow
		GameRenderer.batch.draw(Assetloader.allSpritesMap.get("tutorial arrow"), Assetloader.screenWidth / 2.6f, Assetloader.screenHeight / 1.95f, Assetloader.allSpritesMap.get("tutorial arrow").getWidth() / 2 * Assetloader.gameScaleX, Assetloader.allSpritesMap.get("tutorial arrow").getHeight() / 2 * Assetloader.gameScaleY, Assetloader.allSpritesMap.get("tutorial arrow").getWidth() * Assetloader.gameScaleX, Assetloader.allSpritesMap.get("tutorial arrow").getHeight() * Assetloader.gameScaleY, 1, 1, 0);
		
		//Second arrow
		GameRenderer.batch.draw(Assetloader.allSpritesMap.get("tutorial arrow"), Assetloader.screenWidth / 2.6f, Assetloader.screenHeight / 2.5f, Assetloader.allSpritesMap.get("tutorial arrow").getWidth() / 2 * Assetloader.gameScaleX, Assetloader.allSpritesMap.get("tutorial arrow").getHeight() / 2 * Assetloader.gameScaleY, Assetloader.allSpritesMap.get("tutorial arrow").getWidth() * Assetloader.gameScaleX, Assetloader.allSpritesMap.get("tutorial arrow").getHeight() * Assetloader.gameScaleY, 1, 1, 0);
				
		//Third arrow
		GameRenderer.batch.draw(Assetloader.allSpritesMap.get("tutorial arrow"), Assetloader.screenWidth / 2.6f, Assetloader.screenHeight / 3.475f, Assetloader.allSpritesMap.get("tutorial arrow").getWidth() / 2 * Assetloader.gameScaleX, Assetloader.allSpritesMap.get("tutorial arrow").getHeight() / 2 * Assetloader.gameScaleY, Assetloader.allSpritesMap.get("tutorial arrow").getWidth() * Assetloader.gameScaleX, Assetloader.allSpritesMap.get("tutorial arrow").getHeight() * Assetloader.gameScaleY, 1, 1, 0);
				
		//Spells draw
		
		GameRenderer.batch.draw(Assetloader.allSpritesMap.get("FireButton"), Assetloader.screenWidth / 7f, Assetloader.screenHeight / 2f, (Assetloader.allSpritesMap.get("FireButton").getWidth() * Assetloader.gameScaleX) * 0.8f, (Assetloader.allSpritesMap.get("FireButton").getHeight() * Assetloader.gameScaleY) * 0.8f);
		
		GameRenderer.batch.draw(Assetloader.allSpritesMap.get("EarthButton"), Assetloader.screenWidth / 7f, Assetloader.screenHeight / 2.6f, (Assetloader.allSpritesMap.get("EarthButton").getWidth() * Assetloader.gameScaleX) * 0.8f, (Assetloader.allSpritesMap.get("EarthButton").getHeight() * Assetloader.gameScaleY) * 0.8f);

		GameRenderer.batch.draw(Assetloader.allSpritesMap.get("IceButton"), Assetloader.screenWidth / 7f, Assetloader.screenHeight / 3.7f, (Assetloader.allSpritesMap.get("IceButton").getWidth() * Assetloader.gameScaleX) * 0.8f, (Assetloader.allSpritesMap.get("IceButton").getHeight() * Assetloader.gameScaleY) * 0.8f);
		
		
		GameRenderer.batch.end();
		
	}
	
	@Override
	public void ActionComplete() {
		
		TutorialHandler.bCompleted = true;
		
	}

}
