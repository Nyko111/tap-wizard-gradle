package com.nyko111.TutorialPages;

import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.nyko111.GameWorld.GameRenderer;
import com.nykoengine.Helpers.Assetloader;
import com.nykoengine.TutorialEngine.TutorialPageBase;

/**
 * Created by MacUser on 5/20/17.
 */
public class TutorialWindSpell extends TutorialPageBase {

    float PopupX, PopupY;

    float SpriteX, SpriteY;

    GlyphLayout glyphLayout, glyphLayout2, glyphLayout3;

    public TutorialWindSpell() {

        PopupX = ((Assetloader.screenWidth / 2f) - ((Assetloader.allBGMap.get("LoginBG").getWidth()) / 2) * Assetloader.gameScaleX);

        PopupY = Assetloader.screenHeight / 3.8f;

        SpriteX = ((Assetloader.screenWidth / 2f) - (((Assetloader.allSpritesMap.get("AirButtonPressed").getWidth()) / 2) * Assetloader.gameScaleX) * 1.25f);

        SpriteY = Assetloader.screenHeight / 1.6f;

        glyphLayout = new GlyphLayout(Assetloader.bitmapfonts.get("MainFont"), "Wind spell");

        glyphLayout2 = new GlyphLayout(Assetloader.bitmapfonts.get("MainFont"), "This pushes your");

        glyphLayout3 = new GlyphLayout(Assetloader.bitmapfonts.get("MainFont"), "enemies back!");



    }

    @Override
    public void draw() {
        GameRenderer.batch.begin();
        //GameRenderer.batch.draw(Assetloader.allBGMap.get("tutorial background 1"), x, y, width, height);

        GameRenderer.batch.draw(Assetloader.allBGMap.get("LoginBG"), PopupX, PopupY, Assetloader.allBGMap.get("LoginBG").getWidth() * Assetloader.gameScaleX, Assetloader.allBGMap.get("LoginBG").getHeight() * Assetloader.gameScaleY);

        GameRenderer.batch.draw(Assetloader.allSpritesMap.get("AirButtonPressed"), SpriteX, SpriteY, (Assetloader.allSpritesMap.get("AirButtonPressed").getWidth() * Assetloader.gameScaleX) * 1.25f, (Assetloader.allSpritesMap.get("AirButtonPressed").getHeight() * Assetloader.gameScaleY) * 1.25f);



        Assetloader.bitmapfonts.get("MainFont").draw(GameRenderer.batch, glyphLayout, (Assetloader.screenWidth / 2) - (glyphLayout.width / 2), Assetloader.screenHeight / 1.6f);

        Assetloader.bitmapfonts.get("MainFont").draw(GameRenderer.batch, glyphLayout2, (Assetloader.screenWidth / 2) - (glyphLayout2.width / 2), Assetloader.screenHeight / 2.0f);

        Assetloader.bitmapfonts.get("MainFont").draw(GameRenderer.batch, glyphLayout3, (Assetloader.screenWidth / 2) - (glyphLayout3.width / 2), Assetloader.screenHeight / 2.3f);


        GameRenderer.batch.end();
    }


}
