package com.nyko111.TutorialPages;

import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.nyko111.GameWorld.GameRenderer;
import com.nykoengine.Helpers.Assetloader;
import com.nykoengine.TutorialEngine.TutorialHandler;
import com.nykoengine.TutorialEngine.TutorialPageBase;

public class TutorialTouchAnywhere extends TutorialPageBase {
	
	float PopupX, PopupY;
	
	GlyphLayout glyphLayout, glyphLayout2;
	
	public TutorialTouchAnywhere()  {
		
		PopupX = ((Assetloader.screenWidth / 2f) - ((Assetloader.allBGMap.get("tutorial background 1").getWidth()) / 2) * Assetloader.gameScaleX);
		
		PopupY = Assetloader.screenHeight / 3f;
		
		glyphLayout = new GlyphLayout(Assetloader.bitmapfonts.get("MainFont"), "Touch anywhere");
		
		glyphLayout2 = new GlyphLayout(Assetloader.bitmapfonts.get("MainFont"), "to shoot!");
		
	}
	
	@Override
	public void draw() {
		
		GameRenderer.batch.begin();
		//GameRenderer.batch.draw(Assetloader.allBGMap.get("tutorial background 1"), x, y, width, height);
		
		GameRenderer.batch.draw(Assetloader.allBGMap.get("tutorial background 1"), PopupX, PopupY, Assetloader.allBGMap.get("tutorial background 1").getWidth() * Assetloader.gameScaleX, Assetloader.allBGMap.get("tutorial background 1").getHeight() * Assetloader.gameScaleY);

		Assetloader.bitmapfonts.get("MainFont").draw(GameRenderer.batch, glyphLayout, (Assetloader.screenWidth / 2) - (glyphLayout.width / 2), Assetloader.screenHeight / 1.9f);

		Assetloader.bitmapfonts.get("MainFont").draw(GameRenderer.batch, glyphLayout2, (Assetloader.screenWidth / 2) - (glyphLayout2.width / 2), Assetloader.screenHeight / 2.2f);
		
		GameRenderer.batch.end();
	}
	
	@Override
	public void ActionComplete() {
		
		TutorialHandler.bCompleted = true;
		
	}

}
