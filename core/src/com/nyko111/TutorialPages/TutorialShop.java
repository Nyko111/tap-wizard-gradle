package com.nyko111.TutorialPages;

import com.nyko111.GameWorld.GameRenderer;
import com.nykoengine.Helpers.Assetloader;
import com.nykoengine.TutorialEngine.TutorialHandler;
import com.nykoengine.TutorialEngine.TutorialPageBase;

public class TutorialShop extends TutorialPageBase {
	
	float PopupX, PopupY;
	
	public TutorialShop() {
		
		PopupX = ((Assetloader.screenWidth / 2f) - ((Assetloader.allSpritesMap.get("shoptutorialpopup").getWidth()) / 2) * Assetloader.gameScaleX);
		
		PopupY = Assetloader.screenHeight / 1.35f;
	}
	
	@Override
	public void draw() {
		GameRenderer.batch.begin();
		
		GameRenderer.batch.draw(Assetloader.allSpritesMap.get("shoptutorialpopup"), PopupX, PopupY, Assetloader.allSpritesMap.get("shoptutorialpopup").getWidth() * Assetloader.gameScaleX, Assetloader.allSpritesMap.get("shoptutorialpopup").getHeight() * Assetloader.gameScaleY);
		
		GameRenderer.batch.draw(Assetloader.allSpritesMap.get("shoptutorialbuttonhighlight"), Assetloader.screenWidth / 3.23f, Assetloader.screenHeight / 3.68f, Assetloader.allSpritesMap.get("shoptutorialbuttonhighlight").getWidth() * Assetloader.gameScaleX, Assetloader.allSpritesMap.get("shoptutorialbuttonhighlight").getHeight() * Assetloader.gameScaleY);
		
		
		
		GameRenderer.batch.end();
	}
	
	@Override
	public void ActionComplete() {
		
		TutorialHandler.bCompleted = true;
		
	}

}
