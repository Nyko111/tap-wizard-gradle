package com.nyko111.tapwizard.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.nyko111.tapwizard.TapWizard;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "Tap Wizard";
		cfg.width = 400;
		cfg.height = 640;

		new LwjglApplication(new TapWizard(), cfg);
	}
}
